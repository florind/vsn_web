<?php
namespace App\Api;

use App\Exception\Auth\AccessDeniedException;
use App\Exception\Auth\AccountLockedException;
use App\Exception\Auth\InvalidStructureException;
use App\Exception\Auth\PasswordResetException;
use App\Exception\Auth\ServerException;
use App\Exception\Auth\UserExistsException;
use App\Exception\Auth\ValidationException;
use App\Form\Model\RegisterModel;
use App\Security\AuthUser;
use App\Security\IdentityTokenPayload;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthEndpoint
{
    private $client;
    private $resources;
    private $locale;

    const ERROR_VALIDATION = 'UMN_RESPONSE_VALIDATION_ERROR';
    const ERROR_USER_EXISTS = 'UMN_USER_EXISTS';
    const ERROR_INVALID_TOKEN = 'UMN_RESPONSE_INVALID_TOKEN';
    const ERROR_ACCOUNT_LOCKED = 'UMN_RESPONSE_ACCOUNT_LOCKED';
    const ERROR_TOKEN_LOCKED = 'UMN_TOKEN_LOCKED';
    const ERROR_RESET_PASSWORD = 'UMN_PASSWORD_RESET_ERROR';

    public function __construct(array $authEndpointConf, RequestStack $requestStack)
    {
        $this->locale = $requestStack->getCurrentRequest()->getLocale();
        $this->resources = $authEndpointConf['resource'];
        $this->client = new Client([
            'base_uri' => $authEndpointConf['base_uri'],
            'verify' => false,
            'headers' => ['X-AUTH-TOKEN' => $authEndpointConf['api_key']]
        ]);
    }

    public function createUser(RegisterModel $model)
    {
        try {
            $this->client->post($this->resources['register'], [
                'form_params' => [
                    'email' => $model->getEmail(),
                    'pass' => $model->getPassword(),
                    'first_name' => $model->getFirstName(),
                    'last_name' => $model->getLastName(),
                    'gender' => $model->getGender(),
                    'birthday' => $model->getBirthday(),
                    'locale' => $this->locale
                ]
            ]);
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_BAD_REQUEST) {
                $data = json_decode(
                    $ex->getResponse()->getBody()->getContents(),
                    true
                );

                switch ($data['response']) {
                    case self::ERROR_VALIDATION :
                        throw new ValidationException($data['data']);
                        break;
                    case self::ERROR_USER_EXISTS :
                        throw new UserExistsException();
                }
            } elseif ($ex->getCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
                throw new ServerException();
            }
        } catch (\Exception $exception) {
            throw new ServerException();
        }
    }

    public function activateAccount(string $email, string $token)
    {
        try {
            $this->client->post($this->resources['activate'], [
                'form_params' => [
                    'email' => $email,
                    'token' => $token,
                    'locale' => $this->locale
                ]
            ]);
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_BAD_REQUEST) {
                $data = json_decode($ex->getResponse()->getBody()->getContents(), true);

                if ($data['response'] === self::ERROR_INVALID_TOKEN) {
                    throw new ValidationException(['token' => 'invalid_authorization_code']);
                }
            }
        } catch (\Exception $ex) {
            throw new ServerException();
        }
    }

    public function resendActivationCode(string $email)
    {
        try {
            $this->client->post($this->resources['generate_activation_code'], [
                'form_params' => [
                    'email' => $email,
                    'locale' => $this->locale
                ]
            ]);
        } catch (ClientException $ex) {
            //keep it quiet
        } catch (\Exception $ex) {
            throw new ServerException();
        }
    }

    public function login(string $user, string $pass): AuthUser
    {
        try {
            $response = $this->client->post($this->resources['authenticate'], [
                'form_params' => [
                    'email' => $user,
                    'pass' => $pass
                ]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            $data = $response['data'];
            $refreshToken = $data['refresh_token'];
            $email = $data['email'];
            $tokenPayload = new IdentityTokenPayload($data['token']);

            return (new AuthUser())
                ->setRefreshToken($refreshToken)
                ->setToken($data['token'])
                ->setUsername($email)
                ->setProfileId($tokenPayload->getProfileId())
                ->setRoles($tokenPayload->getRoles());
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_FORBIDDEN) {
                $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                if (null !== $response && array_key_exists('error', $response)) {
                    if ($response['response'] === self::ERROR_ACCOUNT_LOCKED) {
                        throw new AccountLockedException();
                    } else {
                        throw new AccessDeniedException();
                    }
                }
            }

            throw new AuthenticationException();
        } catch (\Exception $exception) {
            throw new ServerException();
        }
    }

    public function refreshIdentityToken(string $user, string $refreshToken)
    {
        try {
            $response = $this->client->post($this->resources['identity_token'], [
                'form_params' => [
                    'email' => $user,
                    'refresh_token' => $refreshToken
                ]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);

            return $response['data']['token'];
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_FORBIDDEN) {
                throw new AccessDeniedException();
            } else {
                throw new ServerException();
            }
        } catch (\Exception $exception) {
            throw new ServerException();
        }
    }

    public function initiatePasswordResetProcess(string $email)
    {
        try {
            $this->client->post($this->resources['forgot_password'], [
                'form_params' => [
                    'email' => $email,
                    'locale' => $this->locale
                ]
            ]);
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
                throw new ServerException();
            }
        } catch (\Exception $exception) {
            throw new ServerException();
        }
    }

    public function resetPassword(string $email, string $token, string $password)
    {
        try {
            $this->client->post($this->resources['reset_password'], [
                'form_params' => [
                    'email' => $email,
                    'token' => $token,
                    'pass' => $password,
                    'locale' => $this->locale
                ]
            ]);
        } catch (ClientException $ex) {
            if ($ex->getCode() === Response::HTTP_BAD_REQUEST) {
                $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                if (null !== $response && array_key_exists('response', $response)) {
                    switch ($response['response']) {
                        case self::ERROR_RESET_PASSWORD:
                            throw new ValidationException(['token' => 'invalid_authorization_code']);
                        case self::ERROR_VALIDATION:
                            throw new ValidationException($response['data']);
                        case self::ERROR_INVALID_TOKEN:
                            throw new ValidationException(['token' => 'invalid_authorization_code']);
                        case self::ERROR_TOKEN_LOCKED:
                            throw new ValidationException(['token' => 'authorization_code_locked']);
                        default:
                            throw new ServerException();
                    }
                }
            }

            if ($ex->getCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
                throw new ServerException();
            }
        } catch (\Exception $exception) {
            throw new ServerException();
        }
    }
}