<?php
namespace App\Api;

use App\Form\Model\CommentTypeModel;
use GuzzleHttp\Client;

class ReplyEndpoint
{
    private $res;
    private $apiKey;
    private $client;

    public function __construct(array $asnConfig)
    {
        $this->res = $asnConfig['resource']['reply'];
        $this->apiKey = $asnConfig['key'];
        $this->client = new Client([
            'base_uri' => $asnConfig['url'],
            'verify' => false
        ]);
    }

    public function list(string $jwt, string $club, string $topic, string $comment, $page, $limit, $refDate)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['collection'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'query' => [
                'page' => $page,
                'limit' => $limit,
                'refDate' => $refDate
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function get(string $jwt, string $club, string $topic, string $comment, string $reply)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment,
            '{reply}' => $reply
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function create(string $jwt, string $club, string $topic, string $comment, CommentTypeModel $model)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['collection'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function update(string $jwt, string $club, string $topic, string $comment, CommentTypeModel $model)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment,
            '{reply}' => $model->getHash()
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function delete(string $jwt, string $club, string $topic, string $comment, string $reply)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment,
            '{reply}' => $reply
        ];

        $this->client->delete(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);
    }

    private function buildUri(string $uri, array $params = [])
    {
        return str_replace(array_keys($params), array_values($params), $uri);
    }
}