<?php
namespace App\Api;

use App\Form\Model\TopicTypeModel;
use GuzzleHttp\Client;

class TopicEndpoint
{
    private $res;
    private $apiKey;
    private $client;

    public function __construct(array $asnConfig)
    {
        $this->res = $asnConfig['resource']['topic'];
        $this->apiKey = $asnConfig['key'];
        $this->client = new Client([
            'base_uri' => $asnConfig['url'],
            'verify' => false
        ]);
    }

    public function list(string $jwt, string $club, $page, $limit)
    {
        $uriParams = ['{club}' => $club];
        $queryParams = [
            'page' => $page,
            'limit' => $limit
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['collection'], $uriParams), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'query' => $queryParams
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function get(string $jwt, string $club, string $topic)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }
    
    public function create(string $jwt, string $club, TopicTypeModel $model)
    {
        $params = ['{club}' => $club];

        $response = $this->client->post(
            $this->buildUri($this->res['collection'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function update(string $jwt, string $club, TopicTypeModel $model)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $model->getHash()
        ];

        $this->client->patch(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'body' => \json_encode($model)
        ]);
    }

    public function lock(string $jwt, string $club, string $topic)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['lock'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function unlock(string $jwt, string $club, string $topic)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['unlock'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function follow(string $jwt, string $club, string $topic)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['follow'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function unfollow(string $jwt, string $club, string $topic)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['unfollow'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    private function buildUri(string $uri, array $params = [])
    {
        return str_replace(array_keys($params), array_values($params), $uri);
    }
}