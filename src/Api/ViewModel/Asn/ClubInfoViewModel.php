<?php
namespace App\Api\ViewModel\Asn;

class ClubInfoViewModel implements \JsonSerializable
{
    private $data = [];

    private $template = [
        'hash' => null,
        'name' => null,
        'description' => null,
        'logo' => null,
        'created' => null,
        'member_count' => 0,
        'topic_count' => 0,
        'founder' => [],
        'permissions' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->template, $response);
        $this->validate($response);

        $this->data = [
            'hash' => $response['hash'],
            'name' => $response['name'],
            'description' => $response['description'],
            'logo' => $response['logo'],
            'created' => $response['created'],
            'memberCount' => $response['member_count'],
            'topicCount' => $response['topic_count'],
            'founder' => $response['founder'],
            'permissions' => $response['permissions']
        ];
    }

    function jsonSerialize()
    {
        return $this->data;
    }

    private function validate(array $response)
    {
        if (
            null === $response['hash'] ||
            null === $response['name'] ||
            null === $response['founder'] ||
            null === $response['created'] ||
            null === $response['member_count'] ||
            null === $response['topic_count'] ||
            null === $response['permissions']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}