<?php
namespace App\Api\ViewModel\Asn;

class PostViewModel
{
    private $hash;
    private $text;
    private $media;
    private $likes;
    private $comments;
    private $context;
    private $poster;
    private $timestamp;

    private $responseTemplate = [
        'post' => [
            'hash' => null,
            'text' => null,
            'media' => [],
            'poster' => null,
            'timestamp' => null
        ],
        'likes' => 0,
        'comments' => 0,
        'context' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->hash = $response['post']['hash'];
        $this->text = $response['post']['text'];
        $this->media = $response['post']['media'];
        $this->timestamp = $response['post']['timestamp'];
        $this->likes = $response['likes'];
        $this->comments = $response['comments'];
        $this->context = $response['context'];
        $this->poster = new MinimalProfileViewModel($response['post']['poster']);
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function getPoster()
    {
        return $this->poster;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function isLiked()
    {
        return in_array('LIKED', $this->context);
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }

    private function validate(array $response)
    {
        if (
            null === $response['post']['hash'] ||
            null === $response['post']['poster']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}