<?php
namespace App\Api\ViewModel\Asn;

class RepliesListViewModel implements \JsonSerializable
{
    private $model;
    private $responseTemplate = [
        'replies' => [],
        'count' => 0
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->model = [
            'replies' => array_map(function ($reply) {return new CommentViewModel($reply);}, $response['replies']),
            'count' => $response['count']
        ];
    }

    function jsonSerialize()
    {
        return $this->model;
    }

    private function validate(array $response)
    {
        if (
            null === $response['replies'] ||
            null === $response['count']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}