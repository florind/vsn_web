<?php
namespace App\Api\ViewModel\Asn;

class ClubMemberViewModel implements \JsonSerializable
{
    private $data = [];

    private $responseTemplate = [
        'hash' => null,
        'first_name' => null,
        'last_name' => null,
        'avatar' => null,
        'locality' => null,
        'timestamp' => null,
        'permissions' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->data = [
            'hash' => $response['hash'],
            'firstName' => $response['first_name'],
            'lastName' => $response['last_name'],
            'avatar' => $response['avatar'],
            'locality' => $response['locality'],
            'timestamp' => $response['timestamp'],
            'permissions' => $response['permissions'],
        ];
    }

    function jsonSerialize()
    {
        return $this->data;
    }

    private function validate(array $response)
    {
        if (
            null === $response['hash'] ||
            null === $response['first_name'] ||
            null === $response['last_name'] ||
            null === $response['permissions']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}