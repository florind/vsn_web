<?php
namespace App\Api\ViewModel\Asn;

class FullProfileViewModel
{
    private $hash;
    private $firstName;
    private $lastName;
    private $gender;
    private $birthday;
    private $avatar;
    private $carCount;
    private $followersCount;
    private $followsCount;
    private $membershipsCount;
    private $context;
    private $locality;

    private $responseTemplate = [
        'profile' => [
            'hash' => null,
            'first_name' => null,
            'last_name' => null,
            'gender' => null,
            'birthday' => null,
            'avatar' => null,
            'locality' => null
        ],
        'cars' => 0,
        'followers' => 0,
        'follows' => 0,
        'memberships' => 0,
        'context' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->hash = $response['profile']['hash'];
        $this->firstName = $response['profile']['first_name'];
        $this->lastName = $response['profile']['last_name'];
        $this->gender = $response['profile']['gender'];
        $this->birthday = $response['profile']['birthday'];
        $this->avatar = $response['profile']['avatar'];
        $this->locality = $response['profile']['locality'];
        $this->carCount = $response['cars'];
        $this->followersCount = $response['followers'];
        $this->followsCount = $response['follows'];
        $this->followsCount = $response['follows'];
        $this->context = $response['context'];
        $this->membershipsCount = $response['memberships'];
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function getCarCount()
    {
        return $this->carCount;
    }

    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    public function getFollowsCount()
    {
        return $this->followsCount;
    }

    public function getMembershipsCount()
    {
        return $this->membershipsCount;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    public function isFollowedByViewer()
    {
        return in_array('followed', $this->context);
    }

    public function isFollowingViewer()
    {
        return in_array('followed_by', $this->context);
    }

    private function validate(array $response)
    {
        if (in_array(null, [
            $response['profile']['hash'],
            $response['profile']['first_name'],
            $response['profile']['last_name']
        ])) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }
}