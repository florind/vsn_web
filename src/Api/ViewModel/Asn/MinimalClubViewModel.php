<?php
namespace App\Api\ViewModel\Asn;

class MinimalClubViewModel implements \JsonSerializable
{
    private $data;

    private $responseTemplate = [
        'hash' => null,
        'name' => null,
        'logo' => null,
        'member_count' => 0,
        'permissions' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->data = [
            'hash' => $response['hash'],
            'name' => $response['name'],
            'logo' => $response['logo'],
            'memberCount' => $response['member_count'],
            'permissions' => $response['permissions']
        ];
    }

    function jsonSerialize()
    {
        return $this->data;
    }

    private function validate(array $response)
    {
        if (
            null === $response['hash'] ||
            null === $response['name'] ||
            null === $response['member_count'] ||
            null === $response['permissions']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}