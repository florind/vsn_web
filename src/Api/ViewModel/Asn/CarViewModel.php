<?php
namespace App\Api\ViewModel\Asn;

class CarViewModel
{
    private $hash;
    private $model;
    private $brand;
    private $year;
    private $fuel;
    private $cc;
    private $hp;
    private $transmission;
    private $body;
    private $mileage;
    private $vin;
    private $media;
    private $likes;
    private $comments;
    private $context;

    private $responseTemplate = [
        'car' => [
            'hash' => null,
            'model' => null,
            'brand' => null,
            'year' => null,
            'fuel' => null,
            'cc' => null,
            'hp' => null,
            'transmission' => null,
            'body' => null,
            'mileage' => null,
            'vin' => null,
        ],
        'media' => null,
        'likes' => 0,
        'comments' => 0,
        'context' => false
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->hash = $response['car']['hash'];
        $this->model = $response['car']['model'];
        $this->brand = $response['car']['brand'];
        $this->year = $response['car']['year'];
        $this->fuel = $response['car']['fuel'];
        $this->cc = $response['car']['cc'];
        $this->hp = $response['car']['hp'];
        $this->transmission = $response['car']['transmission'];
        $this->body = $response['car']['body'];
        $this->mileage = $response['car']['mileage'];
        $this->vin = $response['car']['vin'];
        $this->media = $response['media'];
        $this->likes = $response['likes'];
        $this->comments = $response['comments'];
        $this->context = $response['context'];
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getFuel()
    {
        return $this->fuel;
    }

    public function getCc()
    {
        return $this->cc;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function getTransmission()
    {
        return $this->transmission;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getMileage()
    {
        return $this->mileage;
    }

    public function getVin()
    {
        return $this->vin;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function getComments()
    {
        return $this->comments;
    }

    /** is liked by viewer */
    public function isLiked()
    {
        return in_array('liked', $this->context);
    }

    private function validate(array $response)
    {
        if (in_array(null, [
            $response['car']['hash'],
            $response['car']['model'],
            $response['car']['brand'],
            $response['car']['year'],
        ])) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}