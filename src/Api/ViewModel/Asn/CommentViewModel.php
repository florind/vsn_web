<?php
namespace App\Api\ViewModel\Asn;

class CommentViewModel implements \JsonSerializable
{
    private $data;

    private $responseTemplate = [
        'hash' => null,
        'text' => null,
        'media' => null,
        'author' => null,
        'timestamp' => null,
        'likes' => 0,
        'replies' => 0,
        'permissions' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->data = [
            'hash' => $response['hash'],
            'text' => $response['text'],
            'media' => $response['media'],
            'author' => $response['author'],
            'timestamp' => $response['timestamp'],
            'likes' => $response['likes'],
            'replies' => $response['replies'],
            'permissions' => $response['permissions']
        ];
    }

    function jsonSerialize()
    {
        return $this->data;
    }

    private function validate(array $response)
    {
        if (
            null === $response['hash'] ||
            null === $response['text'] ||
            null === $response['author'] ||
            null === $response['timestamp'] ||
            null === $response['permissions']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}