<?php
namespace App\Api\ViewModel\Asn;

class CommentListViewModel implements \JsonSerializable
{
    private $model;
    private $responseTemplate = [
        'comments' => [],
        'count' => 0
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->model = [
            'comments' => array_map(function ($comment) {return new CommentViewModel($comment);}, $response['comments']),
            'count' => $response['count']
        ];
    }

    function jsonSerialize()
    {
        return $this->model;
    }

    private function validate(array $response)
    {
        if (
            null === $response['comments'] ||
            null === $response['count']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}