<?php
namespace App\Api\ViewModel\Asn;

class MemberListViewModel implements \JsonSerializable
{
    private $model;
    private $responseTemplate = [
        'members' => [],
        'count' => 0
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->model = [
            'members' => array_map(function ($member) {return new ClubMemberViewModel($member);}, $response['members']),
            'count' => $response['count']
        ];
    }

    function jsonSerialize()
    {
        return $this->model;
    }

    private function validate(array $response)
    {
        if (
            null === $response['members'] ||
            null === $response['count']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}