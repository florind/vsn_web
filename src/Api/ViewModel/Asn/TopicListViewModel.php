<?php
namespace App\Api\ViewModel\Asn;

class TopicListViewModel implements \JsonSerializable
{
    private $model;
    private $responseTemplate = [
        'topics' => [],
        'count' => 0
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->model = [
            'topics' => array_map(function ($topic) {return new TopicViewModel($topic);}, $response['topics']),
            'count' => $response['count']
        ];
    }

    function jsonSerialize()
    {
        return $this->model;
    }

    private function validate(array $response)
    {
        if (
            null === $response['topics'] ||
            null === $response['count']
        ) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}