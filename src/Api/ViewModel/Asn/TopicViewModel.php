<?php
namespace App\Api\ViewModel\Asn;

class TopicViewModel implements \JsonSerializable
{
    private $model;
    private $responseTemplate = [
        'hash' => null,
        'title' => null,
        'text' => null,
        'media' => [],
        'author' => null,
        'timestamp' => null,
        'locked' => false,
        'likes' => 0,
        'comments' => 0,
        'permissions' => []
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->validate($response);

        $this->model = [
            'hash' => $response['hash'],
            'title' => $response['title'],
            'text' => $response['text'],
            'media' => $response['media'],
            'likes' => $response['likes'],
            'comments' => $response['comments'],
            'author' => $response['author'],
            'timestamp' => $response['timestamp'],
            'locked' => $response['locked'],
            'permissions' => $response['permissions']
        ];
    }

    function jsonSerialize()
    {
        return $this->model;
    }

    private function validate(array $response)
    {
        if (in_array(null, [
            $response['hash'],
            $response['title'],
            $response['text'],
            $response['author'],
            $response['timestamp'],
            $response['permissions']
        ])) {
            throw new \InvalidArgumentException('Invalid response structure!');
        }
    }

}