<?php
namespace App\Api\ViewModel\Asn;

class LikeCountViewModel implements \JsonSerializable
{
    private $likes;

    private $responseTemplate = [
        'likes' => 0
    ];

    public function __construct(array $response)
    {
        $response = array_replace_recursive($this->responseTemplate, $response);
        $this->likes = $response['likes'];
    }

    public function getLikes()
    {
        return $this->likes;
    }

    function jsonSerialize()
    {
        return ['likes' => $this->likes];
    }
}