<?php
namespace App\Api;

use App\Form\Model\ClubModel;
use GuzzleHttp\Client;

class ClubEndpoint
{
    private $res;
    private $apiKey;
    private $client;

    public function __construct(array $asnConfig)
    {
        $this->res = $asnConfig['resource']['club'];
        $this->apiKey = $asnConfig['key'];
        $this->client = new Client([
            'base_uri' => $asnConfig['url'],
            'verify' => false
        ]);
    }

    public function list(string $jwt)
    {
        $response = $this->client->get(
            $this->res['collection'], [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function get(string $jwt, string $club)
    {
        $params = ['{club}' => $club];

        $response = $this->client->get(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function create(string $jwt, ClubModel $model)
    {
        $response = $this->client->post($this->res['collection'], [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function members(string $jwt, string $club, $page, $limit)
    {
        $params = ['{club}' => $club];

        $response = $this->client->get(
            $this->buildUri($this->res['members'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'query' => [
                'page' => $page,
                'limit' => $limit
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function join(string $jwt, string $club)
    {
        $params = ['{club}' => $club];

        $this->client->post(
            $this->buildUri($this->res['join'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);
    }

    public function leave(string $jwt, string $club)
    {
        $params = ['{club}' => $club];

        $this->client->post(
            $this->buildUri($this->res['leave'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);
    }

    public function ban(string $jwt, string $club, string $member)
    {
        $params = [
            '{club}' => $club,
            '{member}' => $member
        ];

        $this->client->post(
            $this->buildUri($this->res['ban'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);
    }

    private function buildUri(string $uri, array $params = [])
    {
        return str_replace(array_keys($params), array_values($params), $uri);
    }
}