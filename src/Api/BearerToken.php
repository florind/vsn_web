<?php
namespace App\Api;

class BearerToken
{
    public static function generate($apiKey, $jwt)
    {
        return sprintf(
            "Bearer %s",
            base64_encode(sprintf('%s %s', $apiKey, $jwt))
        );
    }
}