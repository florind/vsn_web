<?php
namespace App\Api;

use App\Api\Request\Asn\ListProfileFollowsRequest;
use App\Api\Request\Asn\ViewProfileRequest;
use App\Api\ViewModel\Asn\CarViewModel;
use App\Api\ViewModel\Asn\FullProfileViewModel;
use App\Api\ViewModel\Asn\LikeCountViewModel;
use App\Api\ViewModel\Asn\MinimalClubViewModel;
use App\Api\ViewModel\Asn\MinimalProfileViewModel;
use App\Form\Model\CarTypeModel;
use App\Form\Model\PostTypeModel;
use App\Model\Profile;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AsnEndpoint
{
    private $client;
    private $resources;
    private $apiKey;

    public function __construct(array $asnEndpointConf)
    {
        $this->resources = $asnEndpointConf['resource'];
        $this->apiKey = $asnEndpointConf['api_key'];
        $this->client = new Client([
            'base_uri' => $asnEndpointConf['base_uri'],
            'verify' => false
        ]);
    }

    public function viewProfile(ViewProfileRequest $request)
    {
        try {
            $response = $this->client->get($this->resources['profile'] . '/' . $request->getProfileHash(), [
                'headers' => ['Authorization' => $this->generateBearerToken($request->getJwt())],
            ]);

            return new FullProfileViewModel(json_decode($response->getBody()->getContents(), true));
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return null;
            }
        }
    }

    public function updateProfile(string $jwtToken, Profile $profile)
    {
        try {
            $this->client->patch($this->resources['profile'], [
                'headers' => ['Authorization' => $this->generateBearerToken($jwtToken)],
                'body' => json_encode($profile)
            ]);
        } catch (ClientException $ex) {
            throw new \RuntimeException('Profile update failed!');
        }
    }

    public function deletePhoto(string $jwt, string $nodeHash, string $mediaHash)
    {
        $url = str_replace('{nodeHash}', $nodeHash, $this->resources['delete_photo']);
        $url = str_replace('{mediaHash}', $mediaHash, $url);

        $this->client->delete($url, ['headers' => ['Authorization' => $this->generateBearerToken($jwt)]]);
    }

    public function changeAvatar(string $jwtToken, string $filename)
    {
        try {
            $this->client->post($this->resources['avatar'], [
                'headers' => ['Authorization' => $this->generateBearerToken($jwtToken)],
                'form_params' => ['avatar' => $filename]
            ]);
        } catch (ClientException $ex) {
            throw new \RuntimeException('Profile avatar update failed!');
        }
    }

    public function listFollows(ListProfileFollowsRequest $request)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $request->getProfileHash(), $this->resources['list_following']), [
                'headers' => ['Authorization' => $this->generateBearerToken($request->getJwt())]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            $results = [];

            foreach ($response as $item) {
                $results[] = new MinimalProfileViewModel($item);
            }

            return $results;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return null;
            }
        }
    }

    public function listFollowers(ListProfileFollowsRequest $request)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $request->getProfileHash(), $this->resources['list_followers']), [
                'headers' => ['Authorization' => $this->generateBearerToken($request->getJwt())]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            $results = [];

            foreach ($response as $item) {
                $results[] = new MinimalProfileViewModel($item);
            }

            return $results;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return null;
            }
        }
    }

    public function listClubMemberships(string $jwt, string $profileHash)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $profileHash, $this->resources['list_memberships']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            $results = [];

            foreach ($response as $item) {
                $results[] = new MinimalClubViewModel($item);
            }

            return $results;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return null;
            }
        }
    }

    public function follow(string $jwt, string $profileHash): bool
    {
        try {
            $this->client->post(
                str_replace('{hash}', $profileHash, $this->resources['follow']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);
            return true;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function unfollow(string $jwt, string $profileHash)
    {
        try {
            $this->client->post(
                str_replace('{hash}', $profileHash, $this->resources['unfollow']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return true;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function like(string $jwt, string $nodeHash)
    {
        try {
            $response = $this->client->post(
                str_replace('{hash}', $nodeHash, $this->resources['like']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return new LikeCountViewModel(json_decode($response->getBody()->getContents(), true));
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function unlike(string $jwt, string $nodeHash)
    {
        try {
            $response = $this->client->post(
                str_replace('{hash}', $nodeHash, $this->resources['unlike']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return new LikeCountViewModel(json_decode($response->getBody()->getContents(), true));
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function block(string $jwt, string $nodeHash)
    {
        try {
            $this->client->post(
                str_replace('{hash}', $nodeHash, $this->resources['block']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function unblock(string $jwt, string $nodeHash)
    {
        try {
            $this->client->post(
                str_replace('{hash}', $nodeHash, $this->resources['unblock']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function listBlocked(string $jwt)
    {
        try {
            $response = $this->client->get($this->resources['list_blocked'], [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);

            $profiles = [];

            foreach ($response as $item) {
                $profiles[] = new MinimalProfileViewModel($item);
            }

            return $profiles;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function listLikes(string $jwt, string $nodeHash)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $nodeHash, $this->resources['list_likes']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);

            $likes = [];

            foreach ($response as $item) {
                $likes[] = new MinimalProfileViewModel($item);
            }

            return $likes;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                throw new BadRequestHttpException();
            }
        }
    }

    public function listCars(string $jwt, string $profileHash)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $profileHash, $this->resources['profile_cars']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            $results = [];

            foreach ($response as $item) {
                $results[] = new CarViewModel($item);
            }

            return $results;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function vehicleMedia(string $jwt, string $vehicleHash)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $vehicleHash, $this->resources['get_photos']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function getCar(string $jwt, string $hash)
    {
        try {
            $response = $this->client->get(
                str_replace('{hash}', $hash, $this->resources['get_car']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            $result = json_decode($response->getBody()->getContents(), true);

            return new CarViewModel($result);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function deleteCar(string $jwt, string $hash)
    {
        try {
            $this->client->delete(
                str_replace('{hash}', $hash, $this->resources['delete_car']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return true;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function deleteCarMedia(string $jwt, string $carHash, string $mediaHash)
    {
        try {
            $uri = str_replace('{carHash}', $carHash, $this->resources['delete_photo']);
            $uri = str_replace('{mediaHash}', $mediaHash, $uri);

            $this->client->delete(
                $uri, [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)]
            ]);

            return true;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function createCar(string $jwt, CarTypeModel $model)
    {
        try {
            $response = $this->client->post($this->resources['create_car'], [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)],
                'form_params' => $model->getArrayCopy()
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function updateCar(string $jwt, CarTypeModel $model)
    {
        try {
            $response = $this->client->patch(
                str_replace('{hash}', $model->getHash(), $this->resources['update_car']), [
                'headers' => ['Authorization' => $this->generateBearerToken($jwt)],
                'body' => json_encode($model->getArrayCopy())
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    public function createPost(string $jwt, PostTypeModel $model)
    {}

    public function updatePost(string $jwt, PostTypeModel $model)
    {}

    public function deletePost(string $jwt, string $hash)
    {}

    public function listPosts(string $jwt, string $profileId)
    {}

    public function getPost(string $jwt, string $postId)
    {}

    public function search(string $jwt, string $query, string $type = null)
    {
        try {
            $response = $this->client->get(
                $this->resources['search'], [
                    'query' => ['phrase' => $query, 'type' => $type],
                    'headers' => ['Authorization' => $this->generateBearerToken($jwt)],
            ]);

            $searchResults = json_decode($response->getBody()->getContents(), true);
            $results = [];

            foreach ($searchResults as $type => $entries) {
                foreach ($entries as $entry) {
                    if ($type === 1) {
                        $results[] = new MinimalProfileViewModel($entry);
                    }
                }
            }
            return $results;
        } catch (ClientException $ex) {
            if (in_array($ex->getCode(), [Response::HTTP_FORBIDDEN, Response::HTTP_NOT_FOUND])) {
                return false;
            }
        }
    }

    private function generateBearerToken(string $jwt)
    {
        return sprintf(
            "Bearer %s",
            base64_encode(sprintf('%s %s', $this->apiKey, $jwt))
        );
    }
}