<?php
namespace App\Api;

use App\Form\Model\CommentTypeModel;
use GuzzleHttp\Client;

class CommentEndpoint
{
    private $res;
    private $apiKey;
    private $client;

    public function __construct(array $asnConfig)
    {
        $this->res = $asnConfig['resource']['comment'];
        $this->apiKey = $asnConfig['key'];
        $this->client = new Client([
            'base_uri' => $asnConfig['url'],
            'verify' => false
        ]);
    }

    public function get(string $jwt, string $club, string $topic, string $comment)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function create(string $jwt, string $club, string $topic, CommentTypeModel $model)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['collection'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function update(string $jwt, string $club, string $topic, CommentTypeModel $model)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $model->getHash()
        ];

        $response = $this->client->put(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'form_params' => $model->getArrayCopy()
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function delete(string $jwt, string $club, string $topic, string $comment)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment
        ];

        $this->client->delete(
            $this->buildUri($this->res['single'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);
    }

    public function list(string $jwt, string $club, string $topic, $page, $limit)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic
        ];

        $response = $this->client->get(
            $this->buildUri($this->res['collection'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)],
            'query' => [
                'page' => $page,
                'limit' => $limit
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function follow(string $jwt, string $club, string $topic, string $comment)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment,
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['follow'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function unfollow(string $jwt, string $club, string $topic, string $comment)
    {
        $params = [
            '{club}' => $club,
            '{topic}' => $topic,
            '{comment}' => $comment,
        ];

        $response = $this->client->post(
            $this->buildUri($this->res['unfollow'], $params), [
            'headers' => ['Authorization' => BearerToken::generate($this->apiKey, $jwt)]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    private function buildUri(string $uri, array $params = [])
    {
        return str_replace(array_keys($params), array_values($params), $uri);
    }
}