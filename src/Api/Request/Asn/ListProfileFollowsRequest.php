<?php
namespace App\Api\Request\Asn;

class ListProfileFollowsRequest
{
    private $jwt;
    private $profileHash;

    public function __construct(string $jwt, string $profileHash)
    {
        $this->jwt = $jwt;
        $this->profileHash = $profileHash;
    }

    public function getJwt(): string
    {
        return $this->jwt;
    }

    public function getProfileHash(): string
    {
        return $this->profileHash;
    }
}