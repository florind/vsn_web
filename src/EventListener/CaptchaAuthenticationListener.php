<?php
namespace App\EventListener;

use App\Service\CaptchaValidationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener;

class CaptchaAuthenticationListener extends UsernamePasswordFormAuthenticationListener
{
    private $captchaValidationService;

    public function setCaptchaValidationService(CaptchaValidationService $service)
    {
        $this->captchaValidationService = $service;
    }

    protected function attemptAuthentication(Request $request)
    {
        $captcha = $request->request->get('g-recaptcha-response');
        $this->captchaValidationService->validate($captcha);
        return parent::attemptAuthentication($request);
    }

}