<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EngineDisplacementConverter extends AbstractExtension
{
    const CC_TO_L_RATIO = 0.001;

    public function getFunctions()
    {
        return [
            new TwigFunction('cc_to_liters', [$this, 'convertDisplacementToLiters'])
        ];
    }

    public function convertDisplacementToLiters(int $displacement): string
    {
        $cc = substr($displacement * self::CC_TO_L_RATIO, 0, 3);

        if (strlen($cc) === 1) {
            return sprintf('%d.0', $cc);
        }

        return $cc;
    }
}