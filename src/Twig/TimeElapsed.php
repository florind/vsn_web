<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TimeElapsed extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('time_elapsed', [$this, 'getTimeElapsed'])
        ];
    }

    public function getTimeElapsed(string $dateTime)
    {
        $today = new \DateTime();
        $timestamp = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);

        if (null === $timestamp) {
            return '';
        }

        $diff = $today->diff($timestamp);

        if ($diff->y > 0) {
            return $diff->y . 'y';
        }

        if ($diff->m > 0) {
            return $diff->m . 'mo';
        }

        if ($diff->d > 0) {
            return $diff->d . 'd';
        }

        if ($diff->h > 0) {
            return $diff->h . 'h';
        }

        if ($diff->i > 0) {
            return $diff->i . 'm';
        }

        if ($diff->s > 0) {
            return 'moments ago';
        }

    }
}