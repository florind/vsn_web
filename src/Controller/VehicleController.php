<?php
namespace App\Controller;

use App\Api\ViewModel\Asn\CarViewModel;
use App\Form\SaveCarType;
use App\Form\Model\CarTypeModel;
use App\Repository\NomenclatureRepository;
use App\Service\CarService;
use App\Service\MediaUploadManager;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VehicleController extends Controller
{
    public function saveCar(Request $request, CarService $carService, MediaUploadManager $imageUploader)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $hash = $request->get("hash", "new");

        $carModel = new CarTypeModel();

        if ($hash !== "new") {
            /** @var CarViewModel $car */
            $car = $carService->get($this->getUser()->getToken(), $hash);

            if (false === $car) {
                throw new NoResultException();
            }

            $carModel->setHash($car->getHash())
                ->setYear($car->getYear())
                ->setVin($car->getVin())
                ->setMileage($car->getMileage())
                ->setTransmission($car->getTransmission())
                ->setHorsepower($car->getHp())
                ->setEngineCapacity($car->getCc())
                ->setFuel($car->getFuel())
                ->setBrand($car->getBrand())
                ->setModel($car->getModel())
                ->setBody($car->getBody())
                ->setMedia($car->getMedia());
        }

        $form = $this->createForm(SaveCarType::class, $carModel);
        $form->handleRequest($request);
        $saved = false;

        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $carModel->getUploadedMedia()) {
//                $fileNames = $imageUploader->persistFromSlot(
//                    explode(',', $carModel->getUploadedMedia())
//                );
//                $carModel->setMedia($fileNames);
            }

            $saved = $carService->save($this->getUser()->getToken(), $carModel);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $formErrors = [];

            /** @var Form $child */
            foreach ($form as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $error) {
                        $formErrors[$child->getName()][] = $error->getMessage();
                    }
                }
            }

            return new JsonResponse([
                'saved' => false,
                'errors' => $formErrors
            ]);
        }

        return new JsonResponse([
            'saved' => $saved,
            'html' => $this->renderView(
                'profile/fragment/save-car-form.html.twig',
                ['form' => $form->createView()]
            )
        ]);
    }

    public function deleteCar(Request $request, CarService $carService)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $hash = $request->get('hash');

        if (null === $hash) {
            throw new NotFoundHttpException();
        }

        $result = $carService->delete($this->getUser()->getToken(), $hash);

        return $result ? new JsonResponse() : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function searchCarBrand(Request $request, NomenclatureRepository $repository)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $searchTerm = $request->get('q');

        if (empty(trim($searchTerm))) {
            return new JsonResponse([]);
        }

        $results = $repository->searchCarBrands($searchTerm);
        $brands = [];

        foreach ($results as $entry) {
            $brands[] = [
                'id' => $entry,
                'name' => $entry
            ];
        }

        return new JsonResponse($brands);
    }

    public function searchCarModel(Request $request, NomenclatureRepository $repository)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $searchTerm = $request->get('q');
        $brand = $request->get('brand');

        if (
            empty(trim($searchTerm)) ||
            empty(trim($brand))
        ) {
            return new JsonResponse([]);
        }

        $results = $repository->searchCarModels($brand, $searchTerm);
        $models = [];

        foreach ($results as $entry) {
            $models[] = [
                'id' => $entry,
                'name' => $entry
            ];
        }

        return new JsonResponse($models);
    }
}