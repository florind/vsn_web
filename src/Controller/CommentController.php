<?php

namespace App\Controller;

use App\Form\Model\CommentTypeModel;
use App\Service\CommentService;
use App\Service\MediaUploadManager;
use App\Service\TopicService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommentController extends Controller
{
    public function create(
        Request $request,
        MediaUploadManager $imageUploader,
        CommentService $commentService
    ) {
        $club = $request->get('club');
        $topic = $request->get('topic');

        $csrfKey = 'COMMENT';
        $csrfToken = $slot = $request->get('csrf');

        if (null !== $csrfToken && $this->isCsrfTokenValid($csrfKey, $csrfToken)) {
            $model = (new CommentTypeModel())->setText($request->get('text'));

            if ($imageUploader->slotIsEmpty($slot) && empty($model->getText())) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);

                $success = $commentService->create($this->getUser()->getToken(), $club, $topic, $model);

                return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function update(
        Request $request,
        CommentService $commentService,
        MediaUploadManager $imageUploader
    ) {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $csrfKey = 'COMMENT';
        $csrfToken = $slot = $request->get('csrf');

        if (null !== $csrfToken && $this->isCsrfTokenValid($csrfKey, $csrfToken)) {
            $model = (new CommentTypeModel())
                ->setText($request->get('text'))
                ->setHash($comment);

            if ($imageUploader->slotIsEmpty($slot) && empty($model->getText())) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);

                $success = $commentService->update($this->getUser()->getToken(), $club, $topic, $model);

                return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function list(Request $request, CommentService $commentService)
    {
        $topic = $request->get('topic');
        $club = $request->get('club');
        $page = $request->get('page');
        $limit = $request->get('limit');
        $refDate = $request->get('refDate');

        if (empty($topic)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $comments = $commentService->list($this->getUser()->getToken(), $club, $topic, $page, $limit, $refDate);

        return new JsonResponse($comments);
    }

    public function delete(Request $request, CommentService $commentService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $success = $commentService->delete($this->getUser()->getToken(), $club, $topic, $comment);

        return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function load(Request $request, CommentService $commentService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $response = $commentService->get($this->getUser()->getToken(), $club, $topic, $comment);

        if (empty($response)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($response);
    }

    public function follow(Request $request, CommentService $commentService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $permissions = $commentService->follow($this->getUser()->getToken(), $club, $topic, $comment);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function unfollow(Request $request, CommentService $commentService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $permissions = $commentService->unfollow($this->getUser()->getToken(), $club, $topic, $comment);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }
}