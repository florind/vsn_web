<?php
namespace App\Controller;

use App\Form\ClubType;
use App\Form\Model\ClubModel;
use App\Service\ClubService;
use App\Service\MediaUploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClubController extends Controller
{
    public function view(Request $request, ClubService $clubService, CsrfTokenManagerInterface $csrfManager)
    {
        $club = $request->get('club');

        if (empty($club)) {
            throw new NotFoundHttpException();
        }

        $topicCsrf = $csrfManager->refreshToken('TOPIC');

        $club = $clubService->get($this->getUser()->getToken(), $club);
        return $this->render('club/view.html.twig', [
            'club' => $club,
            'topic_csrf' => $topicCsrf->getValue()
        ]);
    }

    public function info(Request $request, ClubService $clubService)
    {
        $club = $request->get('club');
        $clubInfo = $clubService->get($this->getUser()->getToken(), $club);
        return new JsonResponse($clubInfo);
    }

    public function list(Request $request, ClubService $clubService)
    {
        return $this->render('club/list.html.twig');
    }

    public function members(Request $request, ClubService $clubService)
    {
        $club = $request->get('club');
        $page = $request->get('page');
        $limit = $request->get('limit');

        if (empty($club)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $members = $clubService->members(
            $this->getUser()->getToken(),
            $club, $page, $limit
        );

        return new JsonResponse($members);
    }

    public function create(Request $request, ClubService $clubService, MediaUploadManager $imageUploader, ValidatorInterface $validator)
    {
        $model = new ClubModel(
            $request->get('name'),
            $request->get('description'),
            $request->files->get('logo')
        );

        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        try {
            $filename = $imageUploader->uploadSingle($request->files->get('logo'));
            $model->setLogo($filename);
            $clubService->create($this->getUser()->getToken(), $model);

            return new JsonResponse();
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(['logo' => ['error.logo_format']], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function join(Request $request, ClubService $clubService)
    {
        $club = $request->get('club');

        if (empty($club)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $status = $clubService->join($this->getUser()->getToken(), $club);

        return $status ? new JsonResponse() : new JsonResponse(null, Response::HTTP_SERVICE_UNAVAILABLE);
    }

    public function leave(Request $request, ClubService $clubService)
    {
        $club = $request->get('club');

        if (empty($club)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $status = $clubService->leave($this->getUser()->getToken(), $club);

        return $status ? new JsonResponse() : new JsonResponse(null, Response::HTTP_SERVICE_UNAVAILABLE);
    }

    public function ban(Request $request, ClubService $clubService)
    {
        $club = $request->get('club');
        $member = $request->get('member');

        if (empty($club) || empty($member)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $status = $clubService->ban($this->getUser()->getToken(), $club, $member);

        return $status ? new JsonResponse() : new JsonResponse(null, Response::HTTP_SERVICE_UNAVAILABLE);
    }
}