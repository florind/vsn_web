<?php
namespace App\Controller;

use App\Form\EditProfileType;
use App\Form\Model\EditProfileTypeModel;
use App\Model\Profile;
use App\Service\PostService;
use App\Service\ProfileService;
use App\Form\DataUrlPhotoUploadType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\Model\DataUrlPhotoUploadTypeModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProfileController extends Controller
{
    public function index(Request $request, ProfileService $profileService, PostService $postService)
    {
        $isProfileOwner = $request->get('id') === $this->getUser()->getProfileId();

        $profile = $profileService->view(
            $this->getUser()->getToken(),
            $request->get('id', null)
        );

        if (null === $profile) {
            throw new NotFoundHttpException();
        }

        $recentPosts = $postService->listRecent($this->getUser()->getToken(), $profile->getHash());

        $props = [
            'profile' => $profile,
            'is_profile_owner' => $isProfileOwner,
            'posts' => $recentPosts
        ];

        if ($isProfileOwner) {
            $avatarUploadModel = new DataUrlPhotoUploadTypeModel();
            $avatarUploadForm = $this->createForm(DataUrlPhotoUploadType::class, $avatarUploadModel);

            $birthday = \DateTime::createFromFormat('Y-m-d', $profile->getBirthday());

            $editProfileModel = new EditProfileTypeModel();
            $editProfileModel->setYear($birthday->format("Y"));
            $editProfileModel->setMonth($birthday->format("m"));
            $editProfileModel->setDay($birthday->format("d"));

            $editProfileModel->setFirstName($profile->getFirstName());
            $editProfileModel->setLastName($profile->getLastName());
            $editProfileModel->setGender($profile->getGender());
            $editProfileModel->setLocality($profile->getLocality());

            $editProfileFrom = $this->createForm(EditProfileType::class, $editProfileModel);

            $editProfileFrom->handleRequest($request);

            if ($editProfileFrom->isSubmitted() && $editProfileFrom->isValid()) {
                $updatedProfile = (new Profile())->setHash($this->getUser()->getToken())
                    ->setGender($editProfileModel->getGender())
                    ->setFirstName($editProfileModel->getFirstName())
                    ->setLastName($editProfileModel->getLastName())
                    ->setBirthday($editProfileModel->getBirthday())
                    ->setLocality($editProfileModel->getLocality());

                $profileService->update($this->getUser()->getToken(), $updatedProfile);
                return $this->redirectToRoute('profile', ['id' => $request->get('id')]);
            }

            $props['photo_upload_form'] = $avatarUploadForm->createView();
            $props['edit_profile_form'] = $editProfileFrom->createView();
        }

        return $this->render('profile/view.html.twig', $props);
    }

    public function uploadAvatar(Request $request, ProfileService $profileService)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $model = new DataUrlPhotoUploadTypeModel();
        $form = $this->createForm(DataUrlPhotoUploadType::class, $model);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $model = $form->getData();

            try {
                $profileService->uploadAvatar(
                    $model,
                    $this->getUser()->getProfileId(),
                    $this->getUser()->getToken()
                );
            } catch (\Exception $ex) {
                return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

        }

        return new JsonResponse(null, Response::HTTP_OK);
    }

    public function deleteImage(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('nodeHash');
        $mediaHash = $request->get('mediaHash');

        $deleted = $profileService->deletePhoto($this->getUser()->getToken(), $nodeHash, $mediaHash);

        return $deleted ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function listFollows(Request $request, ProfileService $profileService)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $profileHash = $request->get('hash');

        if (empty($profileHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $results = $profileService->getFollowsList($this->getUser()->getToken(), $profileHash);

        $html = '';

        foreach ($results as $item) {
            $html .= $this->renderView('view-models/minimal-profile-view.html.twig', ['profile' => $item]);
        }

        return new JsonResponse($html);
    }

    public function listFollowers(Request $request, ProfileService $profileService)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $profileHash = $request->get('hash');

        if (empty($profileHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $results = $profileService->getFollowersList($this->getUser()->getToken(), $profileHash);

        $html = '';

        foreach ($results as $item) {
            $html .= $this->renderView('view-models/minimal-profile-view.html.twig', ['profile' => $item]);
        }

        return new JsonResponse($html);
    }

    public function listClubMemberships(Request $request, ProfileService $profileService)
    {
        $clubList = $profileService->getMemberships(
            $this->getUser()->getToken(),
            $this->getUser()->getProfileId()
        );

        return new JsonResponse($clubList);
    }

    public function follow(Request $request, ProfileService $profileService)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $profileHash = $request->get('hash');

        if (empty($profileHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $success = $profileService->follow($this->getUser()->getToken(), $profileHash);

        if ($success) {
            return new JsonResponse();
        } else {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function unfollow(Request $request, ProfileService $profileService)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $profileHash = $request->get('hash');

        if (empty($profileHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $success = $profileService->unfollow($this->getUser()->getToken(), $profileHash);

        if ($success) {
            return new JsonResponse();
        } else {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function like(Request $request, ProfileService $profileService)
    {
        try {
            $nodeHash = $request->get('hash');
            return new JsonResponse($profileService->like($this->getUser()->getToken(), $nodeHash));
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function unlike(Request $request, ProfileService $profileService)
    {
        try {
            $nodeHash = $request->get('hash');
            return new JsonResponse($profileService->unlike($this->getUser()->getToken(), $nodeHash));
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function block(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('hash');

        if (empty($nodeHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $profileService->block($this->getUser()->getToken(), $nodeHash);
            return new JsonResponse();
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function unblock(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('hash');

        if (empty($nodeHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $profileService->unblock($this->getUser()->getToken(), $nodeHash);
            return new JsonResponse();
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function listBlocked(Request $request, ProfileService $profileService)
    {
        try {
            $list = $profileService->listBlocked($this->getUser()->getToken());

            $html = '';

            foreach ($list as $item) {
                $html .= $this->renderView('view-models/minimal-profile-view.html.twig', [
                    'profile' => $item
                ]);
            }

            return new JsonResponse($html);
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function listLikes(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('hash');

        if (empty($nodeHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $likes = $profileService->listLikes($this->getUser()->getToken(), $nodeHash);
            return new JsonResponse($likes);
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function listComments(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('hash');

        if (empty($nodeHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $comments = $profileService->listComments($this->getUser()->getToken(), $nodeHash);

            $html = $this->renderView('view-models/comments-list-view.html.twig', [
                'comments' => $comments,
                'nodeHash' => $nodeHash
            ]);

            return new JsonResponse($html);

        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addComment(Request $request, ProfileService $profileService)
    {
        $nodeHash = $request->get('hash');
        $comment = $request->get('comment');
        $replyTo = $request->get('reply_to');

        if (empty($nodeHash) || empty($comment)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $profileService->addComment($this->getUser()->getToken(), $nodeHash, $comment, $replyTo);
            return new JsonResponse();
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }

    public function listCars(Request $request, ProfileService $profileService)
    {
        $profileHash = $request->get('hash');
        $isProfileOwner = $profileHash === $this->getUser()->getProfileId();

        if (empty($profileHash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $cars = $profileService->listCars($this->getUser()->getToken(), $profileHash);
        $html = $this->renderView(
            'profile/fragment/car-list.html.twig',
            ['cars' => $cars, 'is_profile_owner' => $isProfileOwner]
        );

        return new JsonResponse($html);
    }
}