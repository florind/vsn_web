<?php
namespace App\Controller;

use App\Form\Model\PostTypeModel;
use App\Form\PostType;
use App\Service\MediaUploadManager;
use App\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{
    public function create(Request $request, PostService $postService, MediaUploadManager $imageUploader)
    {
        $postModel = new PostTypeModel();
        $form = $this->createForm(PostType::class, $postModel);
        $form->handleRequest($request);
        $saved = false;

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (null !== $postModel->getUploadedMedia()) {
//                    $fileNames = $imageUploader->persistFromSlot(
//                        explode(',', $postModel->getUploadedMedia())
//                    );
//                    $postModel->setMedia($fileNames);
                }

                $saved = $postService->create($this->getUser()->getToken(), $postModel);
            } else {
                $formErrors = [];

                /** @var Form $child */
                foreach ($form as $child) {
                    if (!$child->isValid()) {
                        foreach ($child->getErrors() as $error) {
                            $formErrors[$child->getName()][] = $error->getMessage();
                        }
                    }
                }

                return new JsonResponse([
                    'saved' => false,
                    'errors' => $formErrors
                ]);
            }
        }

        return new JsonResponse([
            'saved' => $saved,
            'html' => $this->renderView(
                'profile/fragment/save-post-form.html.twig',
                ['form' => $form->createView()]
            )
        ]);
    }

    public function edit(Request $request, PostService $postService, MediaUploadManager $imageUploader)
    {
        $hash = $request->get('hash');
        $post = $postService->get($this->getUser()->getToken(), $hash);

        if (false === $post) {
            throw new NotFoundHttpException();
        }

        $postModel = new PostTypeModel();
        $postModel->setHash($hash)
            ->setMedia($post->getMedia())
            ->setText($post->getText());

        $saved = false;

        $form = $this->createForm(PostType::class, $postModel);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (null !== $postModel->getUploadedMedia()) {
//                    $fileNames = $imageUploader->persistFromSlot(
//                        explode(',', $postModel->getUploadedMedia())
//                    );
//                    $postModel->setMedia($fileNames);
                }

                $saved = (bool)$postService->update($this->getUser()->getToken(), $postModel);
            } else {
                $formErrors = [];

                /** @var Form $child */
                foreach ($form as $child) {
                    if (!$child->isValid()) {
                        foreach ($child->getErrors() as $error) {
                            $formErrors[$child->getName()][] = $error->getMessage();
                        }
                    }
                }

                return new JsonResponse([
                    'saved' => false,
                    'errors' => $formErrors
                ]);
            }
        }

        return new JsonResponse([
            'saved' => $saved,
            'html' => $this->renderView(
                'profile/fragment/save-post-form.html.twig',
                ['form' => $form->createView()]
            )
        ]);
    }

    public function delete(Request $request, PostService $postService)
    {
        $hash = $request->get('hash');

        if (empty($hash)) {
            throw new NotFoundHttpException();
        }

        $success = $postService->delete($this->getUser()->getToken(), $hash);

        return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function view(Request $request, PostService $postService)
    {
        $hash = $request->get('hash');

        if (empty($hash)) {
            throw new NotFoundHttpException();
        }

        $post = $postService->get($this->getUser()->getToken(), $hash);

        return new JsonResponse([
            'html' => $this->renderView('view-models/post-view.html.twig', ['post' => $post])
        ]);
    }
}