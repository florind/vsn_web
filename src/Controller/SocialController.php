<?php
namespace App\Controller;

use App\Repository\NomenclatureRepository;
use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SocialController extends Controller
{
    public function newsfeed(Request $request)
    {
        return $this->render('social/newsfeed.html.twig', [
            'hash' => $this->getUser()->getProfileId()
        ]);
    }

    public function search(Request $request, SearchService $searchService)
    {
        $phrase = $request->get('q');
        $type = $request->get('t');

        $results = [];

        if (!empty($phrase)) {
            $results = $searchService->search($this->getUser()->getToken(), $phrase, $type);
        }

        return $this->render('search/results.html.twig', [
            'results' => $results,
            'query' => $phrase
        ]);
    }

    public function searchLocality(Request $request, NomenclatureRepository $repository)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $searchTerm = $request->get('q');

        if (empty(trim($searchTerm))) {
            return [];
        }

        $results = $repository->searchLocalities($searchTerm);
        $localities = [];

        foreach ($results as $entry) {
            $localities[] = [
                'id' => $entry,
                'name' => $entry
            ];
        }

        return new JsonResponse($localities);
    }
}