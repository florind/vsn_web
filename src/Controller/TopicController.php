<?php

namespace App\Controller;

use App\Form\Model\TopicTypeModel;
use App\Service\MediaUploadManager;
use App\Service\TopicService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TopicController extends Controller
{
    public function create(
        Request $request,
        TopicService $topicService,
        MediaUploadManager $imageUploader,
        ValidatorInterface $validator
    ) {
        $clubHash = $request->get('club');
        $csrfValue = $slot = $request->get('csrf');
        $csrfKey = 'TOPIC';

        if (null !== $csrfValue && $this->isCsrfTokenValid($csrfKey, $csrfValue)) {
            $model = (new TopicTypeModel())
                ->setText($request->get('text'))
                ->setTitle($request->get('title'));

            $violations = $validator->validate($model);

            if ($violations->count()) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);
                $hash = $topicService->create($this->getUser()->getToken(), $clubHash, $model);

                return $hash ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function edit(
        Request $request,
        TopicService $topicService,
        MediaUploadManager $imageUploader,
        ValidatorInterface $validator
    ) {
        $club = $request->get('club');
        $csrfValue = $slot = $request->get('csrf');
        $csrfKey = 'TOPIC';

        if (null !== $csrfValue && $this->isCsrfTokenValid($csrfKey, $csrfValue)) {
            $model = (new TopicTypeModel())
                ->setHash($request->get('topic'))
                ->setText($request->get('text'))
                ->setTitle($request->get('title'));

            $violations = $validator->validate($model);

            if ($violations->count()) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);
                $updated = $topicService->update($this->getUser()->getToken(), $club, $model);

                return $updated ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function list(Request $request, TopicService $topicService)
    {
        $hash = $request->get('hash');
        $page = $request->get('page');
        $limit = $request->get('limit');

        if (empty($hash)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $topics = $topicService->list($this->getUser()->getToken(), $hash, $page, $limit);

        return new JsonResponse($topics);
    }

    public function view(
        Request $request,
        TopicService $topicService,
        CsrfTokenManagerInterface $csrfManager
    ) {
        $clubHash = $request->get('clubHash');
        $topicHash = $request->get('topicHash');
        $topic = $topicService->get($this->getUser()->getToken(), $clubHash, $topicHash);

        $commentCsrf = $csrfManager->refreshToken('COMMENT');
        $replyCsrf = $csrfManager->refreshToken('REPLY');
        $topicCsrf = $csrfManager->refreshToken('TOPIC');

        if (empty($topic)) {
            throw new NotFoundHttpException();
        }

        return $this->render('topic/view.html.twig', [
            'topic' => $topic,
            'clubHash' => $clubHash,
            'comment_csrf' => $commentCsrf->getValue(),
            'reply_csrf' => $replyCsrf->getValue(),
            'topic_csrf' => $topicCsrf->getValue(),
        ]);
    }

    public function load(Request $request, TopicService $topicService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $result = $topicService->get($this->getUser()->getToken(), $club, $topic);

        if (empty($result)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($result);
    }

    public function lock(Request $request, TopicService $topicService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $permissions = $topicService->lock($this->getUser()->getToken(), $club, $topic);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function unlock(Request $request, TopicService $topicService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $permissions = $topicService->unlock($this->getUser()->getToken(), $club, $topic);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function follow(Request $request, TopicService $topicService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $permissions = $topicService->follow($this->getUser()->getToken(), $club, $topic);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function unfollow(Request $request, TopicService $topicService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $permissions = $topicService->unfollow($this->getUser()->getToken(), $club, $topic);

        return null !== $permissions ? new JsonResponse($permissions) : new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }
}