<?php
namespace App\Controller;

use App\Form\Model\CommentTypeModel;
use App\Service\MediaUploadManager;
use App\Service\ReplyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReplyController extends Controller
{
    public function create(
        Request $request,
        MediaUploadManager $imageUploader,
        ReplyService $replyService
    ) {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');

        $csrfKey = 'REPLY';
        $csrfToken = $slot = $request->get('csrf');

        if (null !== $csrfToken && $this->isCsrfTokenValid($csrfKey, $csrfToken)) {
            $model = (new CommentTypeModel())->setText($request->get('text'));

            if ($imageUploader->slotIsEmpty($slot) && empty($model->getText())) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);

                $success = $replyService->create($this->getUser()->getToken(), $club, $topic, $comment, $model);

                return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function update(
        Request $request,
        ReplyService $replyService,
        MediaUploadManager $imageUploader
    ) {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');
        $reply = $request->get('reply');

        $csrfKey = 'REPLY';
        $csrfToken = $slot = $request->get('csrf');

        if (null !== $csrfToken && $this->isCsrfTokenValid($csrfKey, $csrfToken)) {
            $model = (new CommentTypeModel())
                ->setHash($reply)
                ->setText($request->get('text'));

            if ($imageUploader->slotIsEmpty($slot) && empty($model->getText())) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            } else {
                $media = $imageUploader->persistFromSlot($slot);
                $model->setMedia($media);
                $imageUploader->dropSlot($slot);

                $success = $replyService->update($this->getUser()->getToken(), $club, $topic, $comment, $model);

                return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
    }

    public function list(Request $request, ReplyService $replyService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');
        $page = $request->get('page');
        $limit = $request->get('limit');
        $refDate = $request->get('refDate');

        if (empty($comment)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $comments = $replyService->list($this->getUser()->getToken(), $club, $topic, $comment, $page, $limit, $refDate);

        return new JsonResponse($comments);
    }

    public function delete(Request $request, ReplyService $replyService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');
        $reply = $request->get('reply');

        $success = $replyService->delete($this->getUser()->getToken(), $club, $topic, $comment, $reply);

        return $success ? new JsonResponse() : new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function view(Request $request, ReplyService $replyService)
    {
        $club = $request->get('club');
        $topic = $request->get('topic');
        $comment = $request->get('comment');
        $reply = $request->get('reply');

        $topic = $replyService->get($this->getUser()->getToken(), $club, $topic, $comment, $reply);

        if (empty($topic)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($topic);
    }
}