<?php
namespace App\Controller;

use App\Api\AuthEndpoint;
use App\Exception\Auth\ServerException;
use App\Exception\Auth\UserExistsException;
use App\Exception\Auth\ValidationException;
use App\Form\ActivationCodeType;
use App\Form\Model\ActivateAccountModel;
use App\Form\Model\ActivationCodeModel;
use App\Form\Model\ForgotPasswordTypeModel;
use App\Form\Model\PasswordResetTypeModel;
use App\Form\Model\RegisterModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends Controller
{
    public function register(Request $request, AuthEndpoint $authEndpoint, ValidatorInterface $validator)
    {
        $model = new RegisterModel(
            $request->get('first_name'),
            $request->get('last_name'),
            $request->get('email'),
            $request->get('password'),
            $request->get('repeat_password'),
            $request->get('birthday'),
            $request->get('gender')
        );

        /** @var ConstraintViolationList $violations */
        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        try {
            $authEndpoint->createUser($model);
        } catch (UserExistsException $exception) {
            return new JsonResponse(['email' => 'error.email_exists'], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse();
    }

    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $username = $authUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['error' => $error, 'username' => $username]);
    }

    public function activateAccount(Request $request, AuthEndpoint $authEndpoint, ValidatorInterface $validator)
    {
        $model = new ActivateAccountModel(
            $request->get('email'),
            $request->get('token')
        );

        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];

            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        } else {
            try {
                $authEndpoint->activateAccount($model->getEmail(), $model->getToken());
                return new JsonResponse();
            } catch (ValidationException $exception) {
                return new JsonResponse(['token' => ['error.invalid_token']], Response::HTTP_BAD_REQUEST);
            } catch (ServerException $exception) {
                return new JsonResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    public function sendActivationCode(Request $request, AuthEndpoint $authEndpoint, ValidatorInterface $validator)
    {
        $model = new ActivationCodeModel($request->get('email'));
        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];

            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        } else {
            try {
                $authEndpoint->resendActivationCode($model->getEmail());
                return new JsonResponse();
            } catch (ServerException $exception) {
                return new JsonResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    public function passwordReset(Request $request)
    {
        return $this->render('security/password_reset.html.twig');
    }

    public function initPasswordReset(Request $request, AuthEndpoint $authEndpoint, ValidatorInterface $validator)
    {
        $model = new ForgotPasswordTypeModel();
        $model->setEmail($request->get('email'));

        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        try {
            $authEndpoint->initiatePasswordResetProcess($model->getEmail());
            return new JsonResponse();
        } catch (ServerException $ex) {
            return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function finishPasswordReset(Request $request, AuthEndpoint $authEndpoint, ValidatorInterface $validator)
    {
        $model = new PasswordResetTypeModel(
            $request->get('email'),
            $request->get('password'),
            $request->get('repeat_password'),
            $request->get('token')
        );

        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        try {
            $authEndpoint->resetPassword($model->getEmail(), $model->getToken(), $model->getPassword());
            return new JsonResponse();
        } catch (ValidationException $ex) {
            return new JsonResponse(['token' => ['error.invalid_token']], Response::HTTP_BAD_REQUEST);
        } catch (ServerException $ex) {
            return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}