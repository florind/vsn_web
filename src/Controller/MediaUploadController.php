<?php
namespace App\Controller;

use App\Service\MediaUploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class MediaUploadController extends Controller
{
    public function upload(
        Request $request,
        MediaUploadManager $uploader,
        CsrfTokenManagerInterface $csrfManager
    ) {
        $image = $request->files->get('image');
        $csrfKey = $request->get('csrf_key');
        $csrfValue = $request->get('csrf_value');

        if (
            in_array(null, [$image, $csrfKey, $csrfValue]) ||
            !$csrfManager->isTokenValid(new CsrfToken($csrfKey, $csrfValue))
        ) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $temporaryMedia = $uploader->putInSlot($image, $csrfValue);

            return new JsonResponse($temporaryMedia);
        } catch (\InvalidArgumentException $ex) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }
    }

    public function remove(
        Request $request,
        MediaUploadManager $uploader,
        CsrfTokenManagerInterface $csrfManager
    ) {
        $image = $request->get('image');
        $csrfKey = $request->get('csrf_key');
        $csrfValue = $slot = $request->get('csrf_value');

        if (
            in_array(null, [$image, $csrfKey, $csrfValue]) ||
            !$csrfManager->isTokenValid(new CsrfToken($csrfKey, $csrfValue))
        ) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $uploader->dropFromSlot($slot, $image);
            return new JsonResponse();
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }
    }

    public function clear(
        Request $request,
        MediaUploadManager $uploader,
        CsrfTokenManagerInterface $csrfManager
    ) {
        $csrfKey = $request->get('csrf_key');
        $csrfValue = $slot = $request->get('csrf_value');

        if (
            in_array(null, [$csrfKey, $csrfValue]) ||
            !$csrfManager->isTokenValid(new CsrfToken($csrfKey, $csrfValue))
        ) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $uploader->dropSlot($slot);
            return new JsonResponse();
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }
    }
}