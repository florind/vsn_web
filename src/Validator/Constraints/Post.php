<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class Post extends Constraint
{
    public $message = 'post.empty';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}