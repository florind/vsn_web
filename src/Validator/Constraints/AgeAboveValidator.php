<?php
namespace App\Validator\Constraints;

use App\Form\Model\RegisterModel;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AgeAboveValidator extends ConstraintValidator
{
    public function validate($model, Constraint $constraint)
    {
        if (!$model instanceof RegisterModel) {
            return;
        }

        if (!$constraint instanceof AgeAbove) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'AgeAbove');
        }

        $birthday = \DateTime::createFromFormat('Y-m-d', $model->getBirthday());

        if (!$birthday) {
            $this->context->buildViolation($constraint->invalid)
                ->setInvalidValue($birthday)
                ->atPath('birthday')
                ->addViolation();
        }

        $today = new \DateTime();
        $diff = $today->diff($birthday)->y;

        if ($diff < $constraint->age) {
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($diff)
                ->atPath('birthday')
                ->addViolation();
        }
    }
}