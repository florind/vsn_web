<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * Class AgeAbove
 * @package App\Validator\Constraints
 * @Annotation
 */
class AgeAbove extends Constraint
{
    public $age;
    public $message = "error.underage";
    public $invalid = "error.date_format";

    public function __construct($options = null)
    {
        if (null !== $options && !is_array($options)) {
            $options = [ 'age' => $options ];
        }

        parent::__construct($options);

        if (null === $this->age) {
            throw new MissingOptionsException(sprintf('Option "age" must be given for constraint %s', __CLASS__), array('age'));
        }
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}