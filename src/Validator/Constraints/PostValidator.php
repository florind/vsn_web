<?php
namespace App\Validator\Constraints;

use App\Form\Model\CarTypeModel;
use App\Form\Model\PostTypeModel;
use App\Repository\NomenclatureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class PostValidator extends ConstraintValidator
{
    public function validate($topicModel, Constraint $constraint)
    {
        /** @var PostTypeModel $topicModel */
        if (
            empty($topicModel->getText()) &&
            empty($topicModel->getMedia())
        ) {
            $this->context->buildViolation($constraint->message)
                ->atPath('text')
                ->addViolation();
        }
    }
}