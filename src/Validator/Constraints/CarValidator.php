<?php
namespace App\Validator\Constraints;

use App\Form\Model\CarTypeModel;
use App\Repository\NomenclatureRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CarValidator extends ConstraintValidator
{
    private $nomenclature;

    public function __construct(NomenclatureRepository $nomenclature)
    {
        $this->nomenclature = $nomenclature;
    }

    public function validate($carModel, Constraint $constraint)
    {
        /** @var CarTypeModel $carModel */
        if (
            empty($carModel->getBrand()) ||
            empty($carModel->getModel()) ||
            empty($carModel->getFuel()) ||
            empty($carModel->getTransmission())
        ) {
            return;
        }

        if (!in_array($carModel->getFuel(), self::getFuelTypes())) {
            $this->addError('fuel', 'Invalid selection.');
        }

        if (!in_array($carModel->getTransmission(), self::getTransmissionTypes())) {
            $this->addError('transmission', 'Invalid selection.');
        }

        if (!$this->nomenclature->carExists($carModel->getBrand(), $carModel->getModel())) {
            $this->addError(
                'brand',
                sprintf($constraint->message, $carModel->getBrand(), $carModel->getModel() )
            );
        }
    }

    private function addError($path, $error)
    {
        $this->context->buildViolation($error)
            ->atPath($path)
            ->addViolation();
    }

    public static function getFuelTypes()
    {
        return ['petrol', 'lpg', 'diesel', 'electric', 'hybrid'];
    }

    public static function getTransmissionTypes()
    {
        return ['manual', 'semiautomatic', 'automatic', 'cvt', 'tiptronic', 'dsg'];
    }
}