<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class Car extends Constraint
{
    public $message = '%s %s does not appear to be a valid make and model!';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}