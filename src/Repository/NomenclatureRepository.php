<?php
namespace App\Repository;

use Doctrine\DBAL\Connection;

class NomenclatureRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function carExists(string $brand, string $model)
    {
        $count = $this->connection->createQueryBuilder()
            ->select('count(id) as count')
            ->from('vehicle')
            ->where('type = :type AND brand = :brand AND model = :model')
            ->setParameters([
                'type' => 'car',
                'brand' => $brand,
                'model' => $model
            ])
            ->execute()->fetchColumn();

        return (bool)$count;
    }

    public function searchLocalities(string $searchTerm)
    {
        return $this->connection->createQueryBuilder()
            ->select('name')
            ->from('locality')
            ->where('locality LIKE :term')
            ->setParameter('term', $searchTerm.'%')
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function searchCarBrands(string $searchTerm)
    {
        return $this->connection->createQueryBuilder()
            ->select('DISTINCT brand')
            ->from('vehicle')
            ->where('type = :type')
            ->setParameter('type', 'car')
            ->andWhere('brand LIKE :term')
            ->setParameter('term', $searchTerm.'%')
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function searchCarModels(string $brand, string $searchTerm)
    {
        return $this->connection->createQueryBuilder()
            ->select('model')
            ->from('vehicle')
            ->where('type = :type')
            ->setParameter('type', 'car')
            ->andWhere('brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('model LIKE :term')
            ->setParameter('term', '%'.$searchTerm.'%')
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }
}