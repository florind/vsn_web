<?php
namespace App\Security;

use App\Exception\Auth\InvalidIdentityTokenException;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;

class IdentityTokenValidator
{
    private $converter;
    private $serializer;

    public function __construct()
    {
        $this->converter = new StandardConverter();
        $this->serializer = new CompactSerializer($this->converter);
    }

    public function validate(string $jwtToken)
    {
        try {
            $jws = $this->serializer->unserialize($jwtToken);
            $payload = $this->converter->decode($jws->getPayload());

            if (empty($payload) || !array_key_exists('exp', $payload) || $payload['exp'] <= time()) {
                throw new InvalidIdentityTokenException();
            }
        } catch (\Exception $ex) {
            throw new InvalidIdentityTokenException();
        }
    }
}