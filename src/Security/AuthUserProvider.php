<?php
namespace App\Security;

use App\Exception\Auth\InvalidIdentityTokenException;
use App\Api\AuthEndpoint;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthUserProvider implements UserProviderInterface
{
    /** @var  AuthEndpoint */
    private $authEndpoint;
    private $tokenValidator;

    /**
     * AuthUserProvider constructor.
     * @param AuthEndpoint $authEndpoint
     */
    public function __construct(AuthEndpoint $authEndpoint, IdentityTokenValidator $tokenValidator)
    {
        $this->authEndpoint = $authEndpoint;
        $this->tokenValidator = $tokenValidator;
    }

    public function loadUserByUsername($username)
    {
        // TODO: Implement loadUserByUsername() method.
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof AuthUser) {
            throw new UnsupportedUserException();
        }

        try {
            $this->tokenValidator->validate($user->getToken());
            return $user;
        } catch (InvalidIdentityTokenException $ex) {
            return $user->setToken(
                $this->authEndpoint->refreshIdentityToken($user->getUsername(), $user->getRefreshToken())
            );
        } catch (\Exception $ex) {
            throw new AuthenticationException('Invalid Credentials');
        }
    }

    public function supportsClass($class)
    {
        return $class === AuthUser::class;
    }

}