<?php
namespace App\Security;

use App\Exception\Auth\CommunicationException;
use App\Exception\Auth\ServerException;
use App\Api\AuthEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;

class AuthRemoteAuthenticator implements SimpleFormAuthenticatorInterface
{
    /** @var  AuthEndpoint */
    private $authEndpoint;

    /**
     * FormAuthenticator constructor.
     * @param AuthEndpoint $authEndpoint
     */
    public function __construct(AuthEndpoint $authEndpoint)
    {
        $this->authEndpoint = $authEndpoint;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        try {
            $user = $this->authEndpoint->login($token->getUsername(), $token->getCredentials());
        } catch (ServerException $ex) {
            throw new CommunicationException();
        }

        return new AuthToken(
            $user,
            $user->getToken(),
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof AuthToken && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
       return new AuthToken($username, $password, $providerKey);
    }

}