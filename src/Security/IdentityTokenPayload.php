<?php
namespace App\Security;

use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;

class IdentityTokenPayload
{
    private $payload;

    public function __construct(string $jwtToken)
    {
        $converter = new StandardConverter();
        $serializer = new CompactSerializer($converter);
        $jws = $serializer->unserialize($jwtToken);

        $this->payload = $converter->decode($jws->getPayload());
    }

    public function getRoles()
    {
        return array_key_exists('roles', $this->payload) ? $this->payload['roles'] : [];
    }

    public function getProfileId()
    {
        return array_key_exists('pid', $this->payload) ? $this->payload['pid'] : '';
    }
}