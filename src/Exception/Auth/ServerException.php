<?php
namespace App\Exception\Auth;

class ServerException extends \Exception
{
    protected $message = 'Server error occurred';
}