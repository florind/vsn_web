<?php
namespace App\Exception\Auth;

class UserExistsException extends \Exception
{
    protected $message = 'A user with this email already exists';
}