<?php
namespace App\Exception\Auth;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccessDeniedException extends AccountStatusException
{
    public function getMessageKey()
    {
        return 'Invalid Credentials';
    }

}