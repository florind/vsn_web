<?php
namespace App\Exception\Auth;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountLockedException extends AccountStatusException
{
    public function getMessageKey()
    {
        return 'Account Locked';
    }

}