<?php
namespace App\Exception\Auth;

class PasswordResetException extends \Exception
{
    protected $message = "This url might be expired or the account you are trying to reset is locked.";
}