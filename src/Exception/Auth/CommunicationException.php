<?php
namespace App\Exception\Auth;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CommunicationException extends AuthenticationException
{
    public function getMessageKey()
    {
        return 'A communication error occurred!';
    }

}