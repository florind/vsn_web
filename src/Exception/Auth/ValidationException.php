<?php
namespace App\Exception\Auth;

class ValidationException extends \Exception
{
    protected $message = 'validation errors';
    protected $errors = [];

    public function __construct(array $errors)
    {
        $this->errors = $errors;
        parent::__construct();
    }

    public function getErrors()
    {
        return $this->errors;
    }
}