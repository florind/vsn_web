<?php
namespace App\Exception\Auth;

class InvalidStructureException extends \Exception
{
    protected $message = 'invalid structure';
}