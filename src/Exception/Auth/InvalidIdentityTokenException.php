<?php
namespace App\Exception\Auth;

class InvalidIdentityTokenException extends \Exception
{
    protected $message = 'Identity token is invalid';
}