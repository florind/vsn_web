<?php
namespace App\Exception\Auth;

class InitiatePasswordResetException extends \Exception
{
    protected $message = "Cannot initiate password reset process beacause no eligible profile was found.";
}