<?php
namespace App\Model;

class Profile implements \JsonSerializable
{
    private $hash;
    private $firstName;
    private $lastName;
    private $birthday;
    private $gender;
    private $avatar;
    private $locality;
    private $cars;
    private $followers;
    private $follows;

    const FIELD_HASH = 'hash';
    const FIELD_FIRST_NAME = 'first_name';
    const FIELD_LAST_NAME = 'last_name';
    const FIELD_BIRTHDAY = 'birthday';
    const FIELD_GENDER = 'gender';
    const FIELD_AVATAR = 'avatar';
    const FIELD_LOCALITY = 'locality';
    const FIELD_CAR_COUNT = 'cars';
    const FIELD_FOLLOWERS_COUNT = 'followers';
    const FIELD_FOLLOWS_COUNT = 'follows';
    const FIELD_PROFILE = 'profile';

    public static function from(array $data): self
    {
        $profile = new self();

        if (!array_key_exists(self::FIELD_PROFILE, $data)) {
            throw new \InvalidArgumentException('bad data format!');
        }

        if (array_key_exists(self::FIELD_AVATAR, $data[self::FIELD_PROFILE])) {
            $profile->setAvatar($data[self::FIELD_PROFILE][self::FIELD_AVATAR]);
        }

        if (array_key_exists(self::FIELD_FIRST_NAME, $data[self::FIELD_PROFILE])) {
            $profile->setFirstName($data[self::FIELD_PROFILE][self::FIELD_FIRST_NAME]);
        }

        if (array_key_exists(self::FIELD_LAST_NAME, $data[self::FIELD_PROFILE])) {
            $profile->setLastName($data[self::FIELD_PROFILE][self::FIELD_LAST_NAME]);
        }

        if (array_key_exists(self::FIELD_GENDER, $data[self::FIELD_PROFILE])) {
            $profile->setGender($data[self::FIELD_PROFILE][self::FIELD_GENDER]);
        }

        if (array_key_exists(self::FIELD_BIRTHDAY, $data[self::FIELD_PROFILE])) {
            $profile->setBirthday($data[self::FIELD_PROFILE][self::FIELD_BIRTHDAY]);
        }

        if (array_key_exists(self::FIELD_AVATAR, $data[self::FIELD_PROFILE])) {
            $profile->setAvatar($data[self::FIELD_PROFILE][self::FIELD_AVATAR]);
        }

        if (array_key_exists(self::FIELD_LOCALITY, $data[self::FIELD_PROFILE])) {
            $profile->setLocality($data[self::FIELD_PROFILE][self::FIELD_LOCALITY]);
        }

        return $profile;
    }

    function jsonSerialize()
    {
        $data = [];

        if (!empty($this->firstName)) {
            $data[self::FIELD_FIRST_NAME] = $this->firstName;
        }

        if (!empty($this->lastName)) {
            $data[self::FIELD_LAST_NAME] = $this->lastName;
        }

        if (!empty($this->birthday)) {
            $data[self::FIELD_BIRTHDAY] = $this->birthday;
        }

        if (!empty($this->gender)) {
            $data[self::FIELD_GENDER] = $this->gender;
        }

        if (!empty($this->avatar)) {
            $data[self::FIELD_AVATAR] = $this->avatar;
        }

        if (!empty($this->locality)) {
            $data[self::FIELD_LOCALITY] = $this->locality;
        }

        return $data;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(string $birthday): self
    {
        $this->birthday = $birthday;
        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar = null): self
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getCars()
    {
        return $this->cars;
    }

    public function setCars($cars)
    {
        $this->cars = $cars;
        return $this;
    }

    public function getFollowers()
    {
        return $this->followers;
    }

    public function setFollowers($followers)
    {
        $this->followers = $followers;
        return $this;
    }

    public function getFollows()
    {
        return $this->follows;
    }

    public function setFollows($follows)
    {
        $this->follows = $follows;
        return $this;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    public function setLocality($locality)
    {
        $this->locality = $locality;
        return $this;
    }
}