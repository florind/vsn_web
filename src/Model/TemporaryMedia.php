<?php
namespace App\Model;

class TemporaryMedia implements \JsonSerializable
{
    private $filename;
    private $hash;

    public function __construct(string $hash, string $filename)
    {
        $this->hash = $hash;
        $this->filename = $filename;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    function jsonSerialize()
    {
        return [
            'hash' => $this->hash,
            'filename' => $this->filename];
    }
}