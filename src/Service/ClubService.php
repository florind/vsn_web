<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\ClubEndpoint;
use App\Api\ViewModel\Asn\ClubInfoViewModel;
use App\Api\ViewModel\Asn\ClubMemberViewModel;
use App\Api\ViewModel\Asn\MemberListViewModel;
use App\Form\Model\ClubModel;
use GuzzleHttp\Exception\ClientException;

class ClubService
{
    private $endpoint;

    public function __construct(ClubEndpoint $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function get(string $userToken, string $club)
    {
        try {
            $response = $this->endpoint->get($userToken, $club);
            return new ClubInfoViewModel($response);
        } catch (ClientException $e) {
            return false;
        }
    }

    public function members(string $userToken, string $club, $page = null, $limit = null)
    {
        try {
            $response = $this->endpoint->members($userToken, $club, $page, $limit);
            return new MemberListViewModel($response);
        } catch (ClientException $e) {
            return false;
        }
    }

    public function create(string $userToken, ClubModel $model)
    {
        try {
            $this->endpoint->create($userToken, $model);
            return true;
        } catch (ClientException $exception) {
            return false;
        }
    }

    public function join(string $userToken, string $club)
    {
        try {
            $this->endpoint->join($userToken, $club);
            return true;
        } catch (ClientException $ex) {
            return false;
        }
    }

    public function leave(string $userToken, string $club)
    {
        try {
            $this->endpoint->leave($userToken, $club);
            return true;
        } catch (ClientException $ex) {
            return false;
        }
    }

    public function ban(string $userToken, string $club, string $member)
    {
        try {
            $this->endpoint->ban($userToken, $club, $member);
            return true;
        } catch (ClientException $ex) {
            return false;
        }
    }
}