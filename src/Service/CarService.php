<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Form\Model\CarTypeModel;

class CarService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;

    public function __construct(AsnEndpoint $asnEndpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
    }

    public function save(string $jwt, CarTypeModel $model)
    {
        if ($model->getHash() === "new") {
            return $this->asnEndpoint->createCar($jwt, $model);
        } else {
            return $this->asnEndpoint->updateCar($jwt, $model);
        }
    }

    public function get(string $jwt, string $hash)
    {
        return $this->asnEndpoint->getCar($jwt, $hash);
    }

    public function delete(string $jwt, string $hash)
    {
        return $this->asnEndpoint->deleteCar($jwt, $hash);
    }

    public function deleteMedia(string $jwt, string $car, string $photo)
    {
        return $this->asnEndpoint->deleteCarMedia($jwt, $car, $photo);
    }
}