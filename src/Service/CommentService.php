<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\CommentEndpoint;
use App\Api\ViewModel\Asn\CommentListViewModel;
use App\Api\ViewModel\Asn\CommentViewModel;
use App\Api\ViewModel\Asn\TopicViewModel;
use App\Form\Model\CommentTypeModel;
use App\Form\Model\TopicTypeModel;
use GuzzleHttp\Exception\ClientException;

class CommentService
{
    private $commentEndpoint;

    public function __construct(CommentEndpoint $commentEndpoint)
    {
        $this->commentEndpoint = $commentEndpoint;
    }

    public function create(string $jwt, string $club, string $topic, CommentTypeModel $model)
    {
        try {
            $this->commentEndpoint->create($jwt, $club, $topic, $model);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function update(string $jwt, string $club, string $topic, CommentTypeModel $model)
    {
        try {
            $this->commentEndpoint->update($jwt, $club, $topic, $model);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function get(string $jwt, string $club, string $topic, string $comment)
    {
        try {
            $response = $this->commentEndpoint->get($jwt, $club, $topic, $comment);
            return new CommentViewModel($response);
        } catch (ClientException $e) {
            return null;
        }
    }

    public function delete(string $jwt, string $club, string $topic, string $comment)
    {
        try {
            $this->commentEndpoint->delete($jwt, $club, $topic, $comment);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function list(string $userToken, string $club, string $topic, $page = null, $limit = null)
    {
        try {
            $response = $this->commentEndpoint->list($userToken, $club, $topic, $page, $limit);

            return new CommentListViewModel($response);
        } catch (ClientException $e) {
            return [];
        }
    }

    public function follow(string $jwt, string $club, string $topic, string $comment)
    {
        try {
            return $this->commentEndpoint->follow($jwt, $club, $topic, $comment);
        } catch (ClientException $e) {
            return null;
        }
    }

    public function unfollow(string $jwt, string $club, string $topic, string $comment)
    {
        try {
            return $this->commentEndpoint->unfollow($jwt, $club, $topic, $comment);
        } catch (ClientException $e) {
            return null;
        }
    }
}