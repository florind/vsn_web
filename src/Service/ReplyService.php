<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\ReplyEndpoint;
use App\Api\ViewModel\Asn\CommentViewModel;
use App\Api\ViewModel\Asn\RepliesListViewModel;
use App\Api\ViewModel\Asn\TopicViewModel;
use App\Form\Model\CommentTypeModel;
use App\Form\Model\TopicTypeModel;
use GuzzleHttp\Exception\ClientException;

class ReplyService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;
    private $endpoint;

    public function __construct(AsnEndpoint $asnEndpoint, ReplyEndpoint $endpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
        $this->endpoint = $endpoint;
    }

    public function create(string $jwt, string $club, string $topic, string $comment, CommentTypeModel $model)
    {
        try {
            $this->endpoint->create($jwt, $club, $topic, $comment, $model);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function update(string $jwt, string $club, string $topic, string $comment, CommentTypeModel $model)
    {
        try {
            $this->endpoint->update($jwt, $club, $topic, $comment, $model);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function get(string $jwt, string $club, string $topic, string $comment, string $reply)
    {
        try {
            $response = $this->endpoint->get($jwt, $club, $topic, $comment, $reply);
            return new CommentViewModel($response);
        } catch (ClientException $e) {
            return false;
        }
    }

    public function delete(string $jwt, string $club, string $topic, string $comment, string $reply)
    {
        try {
            $this->endpoint->delete($jwt, $club, $topic, $comment, $reply);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function list(string $userToken, string $club, string $topic, string $comment, $page = null, $limit = null, $refDate = null)
    {
        try {
            $response = $this->endpoint->list($userToken, $club, $topic, $comment, $page, $limit, $refDate);

            return new RepliesListViewModel($response);
        } catch (ClientException $e) {
            return [];
        }
    }
}