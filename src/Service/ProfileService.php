<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\Request\Asn\ListProfileFollowsRequest;
use App\Api\Request\Asn\ViewProfileRequest;
use App\Api\ViewModel\Asn\CommentViewModel;
use App\Api\ViewModel\Asn\FullProfileViewModel;
use App\Form\Model\DataUrlPhotoUploadTypeModel;
use App\Form\Model\EditProfileTypeModel;
use App\Model\Profile;
use GuzzleHttp\Exception\ClientException;

class ProfileService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;

    public function __construct(AsnEndpoint $asnEndpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
    }

    public function view(string $jwt, string $profileHash): ?FullProfileViewModel
    {
        return $this->asnEndpoint->viewProfile(new ViewProfileRequest($jwt, $profileHash));
    }

    public function update(string $jwt, Profile $profile)
    {
        $this->asnEndpoint->updateProfile($jwt, $profile);
    }

    public function getFollowsList(string $jwt, string $profileHash): ?array
    {
        return $this->asnEndpoint->listFollows(new ListProfileFollowsRequest($jwt, $profileHash));
    }

    public function getFollowersList(string $jwt, string $profileHash): ?array
    {
        return $this->asnEndpoint->listFollowers(new ListProfileFollowsRequest($jwt, $profileHash));
    }

    public function getMemberships(string $jwt, string $profileHash): ?array
    {
        return $this->asnEndpoint->listClubMemberships($jwt, $profileHash);
    }

    public function follow(string $jwt, string $profileHash): bool
    {
        return $this->asnEndpoint->follow($jwt, $profileHash);
    }

    public function unfollow(string $jwt, string $profileHash): bool
    {
        return $this->asnEndpoint->unfollow($jwt, $profileHash);
    }

    public function like(string $jwt, string $nodeHash)
    {
        return $this->asnEndpoint->like($jwt, $nodeHash);
    }

    public function unlike(string $jwt, string $nodeHash)
    {
        return $this->asnEndpoint->unlike($jwt, $nodeHash);
    }

    public function block(string $jwt, string $nodeHash)
    {
        $this->asnEndpoint->block($jwt, $nodeHash);
    }

    public function unblock(string $jwt, string $nodeHash)
    {
        $this->asnEndpoint->unblock($jwt, $nodeHash);
    }

    public function listBlocked(string $jwt)
    {
        return $this->asnEndpoint->listBlocked($jwt);
    }

    public function listLikes(string $jwt, string $nodeHash)
    {
        return $this->asnEndpoint->listLikes($jwt, $nodeHash);
    }

    public function listComments(string $jwt, string $nodeHash)
    {
        $comments = $this->asnEndpoint->listComments($jwt, $nodeHash);

        $replyMap = $comments['replies'];
        $liked = $comments['liked'];
        unset($comments['replies'], $comments['liked']);

        if (empty($replyMap)) {
            return $comments;
        }

        $sortedComments = [];

        foreach ($comments as $commentHash => $comment) {
            if (in_array($commentHash, $liked)) {
                $comment->setLiked();
            }
            if (in_array($commentHash, array_keys($replyMap))) {
                $sortedComments[$commentHash] = $comment;
                foreach ($replyMap[$commentHash] as $replyHash) {
                    $sortedComments[$commentHash]->addReply($comments[$replyHash]);
                    unset($comments[$replyHash]);
                }
                unset($comments[$commentHash]);
            }
        }

        $sortedComments = array_merge($sortedComments, $comments);

        usort($sortedComments, function (CommentViewModel $a, CommentViewModel $b) {
            return $b->getIndex() - $a->getIndex();
        });

        return $sortedComments;
    }

    public function addComment(string $jwt, string $nodeHash, string $comment, string $replyTo = null)
    {
        $this->asnEndpoint->addComment($jwt, $nodeHash, $comment, $replyTo);
    }

    public function listCars(string $jwt, string $profileHash):?array
    {
        return $this->asnEndpoint->listCars($jwt, $profileHash);
    }

    public function vehicleMedia(string $jwt, string $vehicleHash):?array
    {
        return $this->asnEndpoint->vehicleMedia($jwt, $vehicleHash);
    }

    public function deletePhoto(string $jwt, string $nodeHash, string $mediaHash)
    {
        try {
            $this->asnEndpoint->deletePhoto($jwt, $nodeHash, $mediaHash);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function uploadAvatar(DataUrlPhotoUploadTypeModel $model, string $profileId, string $jwtToken)
    {
        // @todo: refactor this to remove hard coding
        $projectPath = '/home/florin/projects/vehicle/web';
        $uploadPath = $projectPath . '/public/images/upload/';
        $fileName = md5(uniqid($profileId, true)) . '.jpg';
        file_put_contents($uploadPath . $fileName, $model->getUrl());

        $this->asnEndpoint->changeAvatar($jwtToken, $fileName);
    }
}