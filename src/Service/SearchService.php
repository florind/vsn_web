<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\Request\Asn\ListProfileFollowsRequest;
use App\Api\Request\Asn\ViewProfileRequest;
use App\Api\ViewModel\Asn\CommentViewModel;
use App\Api\ViewModel\Asn\FullProfileViewModel;
use App\Form\Model\DataUrlPhotoUploadTypeModel;
use App\Form\Model\EditProfileTypeModel;
use App\Model\Profile;

class SearchService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;

    public function __construct(AsnEndpoint $asnEndpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
    }

    public function search(string $jwt, string $keyword, string $type = null)
    {
        return $this->asnEndpoint->search($jwt, $keyword, $type);
    }
}