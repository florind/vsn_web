<?php
namespace App\Service;

use App\Exception\InvalidCaptchaException;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CaptchaValidationService
{
    private $url;
    private $key;

    /**
     * CaptchaValidationService constructor.
     */
    public function __construct($url, $key)
    {
        $this->key = $key;
        $this->url = $url;
    }

    public function validate($token)
    {
        $client = new Client();
        $response = $client->request(Request::METHOD_POST, $this->url, [
            'form_params' => [
                'secret' => $this->key,
                'response' => $token
            ]
        ]);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new InvalidCaptchaException();
        }

        $body = $response->getBody()->getContents();
        $decoded = json_decode($body);

        if (!$decoded->success) {
            throw new InvalidCaptchaException();
        }
    }
}