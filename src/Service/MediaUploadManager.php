<?php
namespace App\Service;

use App\Model\TemporaryMedia;
use Symfony\Component\Asset\Packages;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

class MediaUploadManager
{
    const MAX_FILE_SIZE = 10485760;
    const MAX_RESOLUTION_SIZE = 2048;
    const MEDIUM_RESOLUTION_SIZE = 960;
    const MIN_RESOLUTION_SIZE = 720;
    const COMPRESSION_QUALITY = 70;
    const MEDIA_KEY = 'temporary-media-upload';

    private $static;
    private $tmpDirName;
    private $tmpPath;
    private $session;
    private $asset;

    private $allowedMimeTypes = [
        'image/jpeg',
        'image/png',
        'image/bmp'
    ];

    public function __construct(
        array $config,
        Session $session,
        Packages $asset
    ) {
        if (
            !array_key_exists('root', $config) ||
            !array_key_exists('tmp', $config)
        ) {
            throw new \InvalidArgumentException('Static config structure is invalid. Must contain keys [root, tmp]');
        }

        $this->static = $config['root'];
        $this->tmpDirName = $config['tmp'];
        $this->tmpPath = sprintf('%s/%s', $this->static, $this->tmpDirName);
        $this->session = $session;
        $this->asset = $asset;
    }

    private function getSlotData(string $slotKey)
    {
        if (!$this->session->has(self::MEDIA_KEY)) {
            $this->session->set(self::MEDIA_KEY, [
                $slotKey => []
            ]);
        }

        $mediaSlot = $this->session->get(self::MEDIA_KEY);

        if (!array_key_exists($slotKey, $mediaSlot)) {
            $mediaSlot[$slotKey] = [];
            $this->session->set(self::MEDIA_KEY, $mediaSlot);
        }

        return $mediaSlot[$slotKey];
    }

    private function setSlotData(string $slotKey, array $data)
    {
        $mediaSlot = $this->session->get(self::MEDIA_KEY);

        if (null === $mediaSlot) {
            throw new \RuntimeException('Media slot is not defined');
        }

        if (!array_key_exists($slotKey, $mediaSlot)) {
            throw new \InvalidArgumentException('Slot key not defined');
        }

        $mediaSlot[$slotKey] = $data;
        $this->session->set(self::MEDIA_KEY, $mediaSlot);
    }

    public function putInSlot(UploadedFile $file, string $slotKey)
    {
        $this->validate($file);

        $filename = $this->generateFilename($file);
        $image = $this->optimize($file);

        if (!is_dir($this->tmpPath)) {
            mkdir($this->tmpPath);
        }

        $image->writeImage(sprintf('%s/%s', $this->tmpPath, $filename));
        $image->destroy();

        $id = uniqid($filename, true);

        $data = $this->getSlotData($slotKey);
        $data[$id] = $filename;
        $this->setSlotData($slotKey, $data);

        return new TemporaryMedia(
            $id,
            sprintf('%s/%s', $this->tmpDirName, $filename)
        );
    }

    public function slotIsEmpty(string $slotKey)
    {
        return empty($this->getSlotData($slotKey));
    }

    public function persistFromSlot(string $slotKey)
    {
        $data = $this->getSlotData($slotKey);
        $paths = [];

        foreach ($data as $id => $filename) {
            $path = sprintf('%s/%s', $this->tmpPath, $filename);
            $file = new File($path);
            $this->persist($file, $filename);
            $paths[] = $filename;
        }

        return $paths;
    }

    private function persist(File $file, $filename)
    {
        $topDir = substr($filename, 0, 2);
        $subDir = substr($filename, 2, 2);
        $realFilename = substr($filename, 4);

        if (
        is_file(
            sprintf(
                '%s/$s/%s/%s',
                $this->static,
                $topDir,
                $subDir,
                $realFilename
            )
        )
        ) {
            return;
        }

        $finalDir = sprintf('%s/%s/%s', $this->static, $topDir, $subDir);

        if (!is_dir($finalDir)) {
            mkdir($finalDir, 0777, true);
        }

        $file->move($finalDir, $realFilename);
    }

    public function uploadSingle(UploadedFile $file)
    {
        $this->validate($file);

        $filename = $this->generateFilename($file);
        $image = $this->optimize($file);
        $image->writeImage();
        $image->destroy();

        $file = new File($file->getRealPath());
        $this->persist($file, $filename);

        return $filename;
    }

    public function dropFromSlot(string $slotKey, string $id)
    {
        $data = $this->getSlotData($slotKey);

        if (empty($data)) {
            return;
        }

        if (array_key_exists($id, $data)) {
            unset($data[$id]);
            $this->setSlotData($slotKey, $data);
        }
    }

    public function dropSlot(string $slotKey)
    {
        $mediaSlot = $this->session->get(self::MEDIA_KEY);

        if (null === $mediaSlot) {
            return;
        }

        if (array_key_exists($slotKey, $mediaSlot)) {
            unset($mediaSlot[$slotKey]);
            $this->session->set(self::MEDIA_KEY, $mediaSlot);
        }
    }

    /**
     * @param UploadedFile $file
     */
    private function validate(UploadedFile $file)
    {
        if ($file->getSize() > self::MAX_FILE_SIZE) {
            throw new \InvalidArgumentException();
        }

        if (!in_array($file->getMimeType(), $this->allowedMimeTypes)) {
            throw new \InvalidArgumentException();
        }
    }

    private function generateFilename (File $file)
    {
        $imageHash = sha1_file($file->getRealPath());
        return sprintf('%s.%s', $imageHash, $file->guessExtension());
    }

    private function optimize(UploadedFile $file): \Imagick
    {
        $photo = new \Imagick($file->getRealPath());
        $width = $photo->getImageWidth();

        if (
            $width >= self::MIN_RESOLUTION_SIZE &&
            $width <= self::MEDIUM_RESOLUTION_SIZE
        ) {
            $width = self::MIN_RESOLUTION_SIZE;
        } elseif (
            $width >= self::MEDIUM_RESOLUTION_SIZE &&
            $width <= self::MAX_RESOLUTION_SIZE
        ) {
            $width = self::MEDIUM_RESOLUTION_SIZE;
        } elseif ($width > self::MAX_RESOLUTION_SIZE) {
            $width = self::MAX_RESOLUTION_SIZE;
        }

        $photo->resizeImage($width, 0, \Imagick::FILTER_CATROM, 1);
        $photo->setImageCompression(\Imagick::COMPRESSION_JPEG);
        $photo->setImageCompressionQuality(self::COMPRESSION_QUALITY);
        $photo->setImageColorspace(\Imagick::COLORSPACE_SRGB);
        $photo->stripImage();

        return $photo;
    }
}