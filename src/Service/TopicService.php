<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Api\TopicEndpoint;
use App\Api\ViewModel\Asn\TopicListViewModel;
use App\Api\ViewModel\Asn\TopicViewModel;
use App\Form\Model\TopicTypeModel;
use GuzzleHttp\Exception\ClientException;

class TopicService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;
    private $topicsEndpoint;

    public function __construct(AsnEndpoint $asnEndpoint, TopicEndpoint $topicsEndpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
        $this->topicsEndpoint = $topicsEndpoint;
    }

    public function create(string $jwt, string $clubHash, TopicTypeModel $model)
    {
        try {
            $response = $this->topicsEndpoint->create($jwt, $clubHash, $model);
            return $response['hash'];
        } catch (ClientException $e) {
            return false;
        }
    }

    public function update(string $jwt, string $club, TopicTypeModel $model)
    {
        try {
            $this->topicsEndpoint->update($jwt, $club, $model);
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function get(string $jwt, string $club, string $topic)
    {
        try {
            $response = $this->topicsEndpoint->get($jwt, $club, $topic);
            return new TopicViewModel($response);
        } catch (ClientException $e) {
            return false;
        }
    }

    public function list(string $userToken, string $clubHash, $page = null, $limit = null)
    {
        try {
            $response = $this->topicsEndpoint->list($userToken, $clubHash, $page, $limit);
            return new TopicListViewModel($response);
        } catch (ClientException $e) {
            return [];
        }
    }

    public function lock(string $jwt, string $club, string $topic)
    {
        try {
            return $this->topicsEndpoint->lock($jwt, $club, $topic);
        } catch (ClientException $e) {
            return null;
        }
    }

    public function unlock(string $jwt, string $club, string $topic)
    {
        try {
            return $this->topicsEndpoint->unlock($jwt, $club, $topic);
        } catch (ClientException $e) {
            return null;
        }
    }

    public function follow(string $jwt, string $club, string $topic)
    {
        try {
            return $this->topicsEndpoint->follow($jwt, $club, $topic);
        } catch (ClientException $e) {
            return null;
        }
    }

    public function unfollow(string $jwt, string $club, string $topic)
    {
        try {
            return $this->topicsEndpoint->unfollow($jwt, $club, $topic);
        } catch (ClientException $e) {
            return null;
        }
    }
}