<?php
namespace App\Service;

use App\Api\AsnEndpoint;
use App\Form\Model\PostTypeModel;

class PostService
{
    /** @var  AsnEndpoint */
    private $asnEndpoint;

    public function __construct(AsnEndpoint $asnEndpoint)
    {
        $this->asnEndpoint = $asnEndpoint;
    }

    public function create(string $jwt, PostTypeModel $model)
    {
        return $this->asnEndpoint->createPost($jwt, $model);
    }

    public function update(string $jwt, PostTypeModel $model)
    {
        return $this->asnEndpoint->updatePost($jwt, $model);
    }

    public function delete(string $jwt, string $hash)
    {
        return $this->asnEndpoint->deletePost($jwt, $hash);
    }

    public function listRecent(string $jwt, string $profileId)
    {
        return $this->asnEndpoint->listPosts($jwt, $profileId);
    }

    public function get(string $jwt, string $postId)
    {
        return $this->asnEndpoint->getPost($jwt, $postId);
    }
}