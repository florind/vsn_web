<?php
namespace App\Form;

use App\Form\Model\ActivationCodeModel;
use App\Form\Model\ForgotPasswordTypeModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ActivationCodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'signup.email.placeholder'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'activation_code.submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ActivationCodeModel::class,
            'csrf_token_id' => 'activation_code_type',
            'translation_domain' => 'forms'
        ]);
    }
}