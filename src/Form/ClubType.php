<?php
namespace App\Form;

use App\Form\Model\ClubModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClubType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hash', HiddenType::class)
            ->add(
                'name',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'club_type.name.label'
                ]
            )->add(
                'description',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'club_type.description.label',
                    'attr' => ['rows' => 5, 'placeholder' => 'club_type.description.placeholder']
                ]
            )->add(
                'logo',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'club_type.avatar.label',
                    'multiple' => false,
                    'attr' => [
                        'accept' => '.jpg,.jpeg,image/jpeg'
                    ]
                ]
            )->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'club_type.submit'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClubModel::class,
            'csrf_token_id' => 'club_type',
            'translation_domain' => 'forms'
        ]);
    }

}