<?php
namespace App\Form;

use App\Form\Model\DataUrlPhotoUploadTypeModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataUrlPhotoUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', TextType::class, []);

        $builder->get('url')
            ->addModelTransformer(new CallbackTransformer(
                function ($dataUrl) { return $dataUrl; },
                function ($dataUrl) {
                    $encodedData = str_replace('data:image/jpeg;base64,', '', $dataUrl);
                    $encodedData = str_replace(' ','+',$encodedData);
                    return base64_decode($encodedData);
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DataUrlPhotoUploadTypeModel::class,
            'csrf_token_id' => 'data_url_photo_upload_type',
            'translation_domain' => 'forms'
        ]);
    }
}