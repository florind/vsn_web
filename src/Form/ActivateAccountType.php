<?php
namespace App\Form;

use App\Form\Model\ActivateAccountModel;
use App\Form\Model\ForgotPasswordTypeModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ActivateAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'signup.email.placeholder'
                ]
            ])
            ->add('token', TextType::class, [
                'attr' => [
                    'placeholder' => 'activate_account.token.placeholder'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'activate_account.buttons.activate'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ActivateAccountModel::class,
            'csrf_token_id' => 'activate_account_type',
            'translation_domain' => 'forms'
        ]);
    }
}