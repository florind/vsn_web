<?php
namespace App\Form;

use App\Form\Model\EditProfileTypeModel;
use App\Form\Model\RegisterModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $monthChoices = [
            'date.january' => '01',
            'date.february' => '02',
            'date.march' => '03',
            'date.april' => '04',
            'date.may' => '05',
            'date.june' => '06',
            'date.july' => '07',
            'date.august' => '08',
            'date.september' => '09',
            'date.october' => '10',
            'date.november' => '11',
            'date.december' => '12',
        ];

        $yearChoices = [];
        for ($year = (new \DateTime())->format('Y'); $year >= 1900; $year--) {
            $yearChoices[$year] = $year;
        }

        $dayChoices = [];
        for ($day = 31; $day >= 0; $day--) {
            $dayChoices[$day] = $day;
        }

        $builder->add('firstName', TextType::class, [
                'label' => 'edit_profile.firstname',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'edit_profile.lastname',
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'signup.gender.label',
                'choices' => [
                    'signup.gender.male' => RegisterModel::GENDER_MALE,
                    'signup.gender.female' => RegisterModel::GENDER_FEMALE,
                ],
                'expanded' => true
            ])
            ->add('year', ChoiceType::class, [
                'label' => 'signup.birthday.label',
                'choices' => $yearChoices
            ])
            ->add('month', ChoiceType::class, [
                'choices' => $monthChoices,
                'translation_domain' => 'messages'
            ])
            ->add('day', ChoiceType::class, [
                'choices' => $dayChoices
            ])
            ->add('locality', TextType::class, [
                'label' => 'edit_profile.locality'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditProfileTypeModel::class,
            'csrf_token_id' => 'edit_profile_type',
            'translation_domain' => 'forms'
        ]);
    }
}