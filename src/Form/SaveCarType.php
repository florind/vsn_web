<?php
namespace App\Form;

use App\Form\Model\CarTypeModel;
use App\Validator\Constraints\CarValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SaveCarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $yearFirstCarsMade = 1885;
        $currentYear = (new \DateTime())->format('Y');
        $range = range($currentYear, $yearFirstCarsMade);
        $years = array_combine($range, $range);

        $fuelOptions = $transmissionOptions = [];

        foreach (CarValidator::getFuelTypes() as $fuel) {
            $fuelOptions[sprintf('car_type.fuel_type.%s', $fuel)] = $fuel;
        }

        foreach (CarValidator::getTransmissionTypes() as $type) {
            $transmissionOptions[sprintf('car_type.transmission_type.%s', $type)] = $type;
        }

        $builder->add(
            'hash',
            HiddenType::class
        )->add(
            'brand',
            TextType::class,
            ['label' => 'car_type.brand']
        )->add(
            'model',
            TextType::class,
            ['label' => 'car_type.model']
        )->add(
            'body',
            TextType::class,
            [
                'label' => 'car_type.body',
                'required' => false
            ]
        )->add(
            'year',
            ChoiceType::class,
            [
                'label' => 'car_type.year',
                'choices' => $years
            ]
        )->add(
            'fuel',
            ChoiceType::class,
            [
                'label' => 'car_type.fuel',
                'choices' => $fuelOptions
            ]
        )->add(
            'engineCapacity',
            IntegerType::class,
            ['label' => 'car_type.cc']
        )->add(
            'horsepower',
            IntegerType::class,
            ['label' => 'car_type.hp']
        )->add(
            'mileage',
            IntegerType::class,
            [
                'label' => 'car_type.mileage',
                'required' => false
            ]
        )->add(
            'transmission',
            ChoiceType::class,
            [
                'label' => 'car_type.transmission',
                'choices' => $transmissionOptions
            ]
        )->add(
            'vin',
            TextType::class,
            [
                'label' => 'car_type.vin',
                'required' => false,
                'attr' => [
                    'size' => 17,
                    'maxlength' => 17,
                    'style' => 'text-transform: uppercase'
                ]
            ]
        )->add(
            'media',
            FileType::class,
            [
                'label' => 'car_type.photos',
                'multiple' => true,
                'attr' => [
                    'accept' => '.jpg,.jpeg,image/jpeg'
                ]
            ]
        )->add(
            'uploadedMedia',
            HiddenType::class
        )->add('submit', SubmitType::class, [
            'label' => 'Save'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CarTypeModel::class,
            'csrf_token_id' => 'save_car_type',
            'translation_domain' => 'forms',
            'error_bubbling' => true
        ]);
    }
}