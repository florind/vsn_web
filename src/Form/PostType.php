<?php
namespace App\Form;

use App\Form\Model\PostTypeModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hash', HiddenType::class)
            ->add(
                'text',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => ['rows' => 1, 'placeholder' => 'post_type.text_placeholder']
                ]
            )->add(
                'media',
                FileType::class,
                [
                    'label' => 'post_type.photos',
                    'multiple' => true,
                    'attr' => [
                        'accept' => '.jpg,.jpeg,image/jpeg'
                    ]
                ]
            )->add(
                'uploadedMedia',
                HiddenType::class
            )->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostTypeModel::class,
            'csrf_token_id' => 'post_type',
            'translation_domain' => 'forms'
        ]);
    }

}