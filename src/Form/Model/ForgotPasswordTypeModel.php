<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ForgotPasswordTypeModel
{
    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Email(
     *     message = "error.email_format",
     *     checkMX = true
     * )
     */
    private $email;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
}