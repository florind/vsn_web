<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\AgeAbove;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class RegisterTypeModel
 * @package App\Form\Model
 * @AgeAbove(age = 16)
 */
class RegisterModel
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z ]+$/",
     *     message = "error.name_format"
     * )
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "error.name_length",
     *     maxMessage = "error.name_length",
     * )
     */
    private $first_name;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Regex(
     *     pattern = "/^[a-zA-Z ]+$/",
     *     message = "error.name_format"
     * )
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "error.name_length",
     *     maxMessage = "error.name_length",
     * )
     */
    private $last_name;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Email(
     *     message = "error.email_format",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Regex(
     *     pattern = "/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/",
     *     htmlPattern = false,
     *     message = "error.password_strength"
     * )
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     */
    private $repeatPassword;

    /**
     * @var string
     * @Assert\Date()
     */
    private $birthday;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Choice(
     *     choices = {"male", "female"},
     *     message = "error.gender"
     * )
     */
    private $gender;

    public function __construct($firstName, $lastName, $email, $password, $repeatPassword, $birthday, $gender)
    {
        $this->first_name = $firstName;
        $this->last_name = $lastName;
        $this->email = $email;
        $this->password = $password;
        $this->repeatPassword = $repeatPassword;
        $this->birthday = $birthday;
        $this->gender = $gender;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->password !== $this->repeatPassword) {
            $context->buildViolation('error.password_match')
                ->atPath('repeat_password')
                ->addViolation();
        }
    }
}