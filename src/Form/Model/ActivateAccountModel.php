<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ActivateAccountModel
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "error.email_format",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @Assert\NotBlank()
     */
    private $token;

    public function __construct($email, $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken(string $token)
    {
        $this->token = $token;
        return $this;
    }

}