<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ClubModel
{
    /**
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Length(min=2, max=100)
     */
    private $name;

    /**
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Length(min=2, max=255)
     */
    private $description;

    /**
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     */
    private $logo;

    public function __construct($name, $description, $logo)
    {
        $this->name = $name;
        $this->description = $description;
        $this->logo = $logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    public function getArrayCopy()
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'logo' => $this->logo
        ];
    }

}