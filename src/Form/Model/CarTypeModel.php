<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\Car;

/**
 * @Car()
 */
class CarTypeModel
{
    /** @var  string */
    private $hash = "new";

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $model;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $brand;

    /** @var  string */
    private $body;

    /**
     * @Assert\NotBlank()
     */
    private $year;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $fuel;

    /**
     * @Assert\NotBlank()
     */
    private $engineCapacity;

    /**
     * @Assert\NotBlank()
     */
    private $horsepower;

    private $mileage;

    /**
     * @Assert\NotBlank()
     */
    private $transmission;

    private $vin;

    private $media;

    private $uploadedMedia;

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     * @return CarTypeModel
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     * @return CarTypeModel
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return CarTypeModel
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return CarTypeModel
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param mixed $fuel
     * @return CarTypeModel
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEngineCapacity()
    {
        return $this->engineCapacity;
    }

    /**
     * @param mixed $engineCapacity
     * @return CarTypeModel
     */
    public function setEngineCapacity($engineCapacity)
    {
        $this->engineCapacity = $engineCapacity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorsepower()
    {
        return $this->horsepower;
    }

    /**
     * @param mixed $horsepower
     * @return CarTypeModel
     */
    public function setHorsepower($horsepower)
    {
        $this->horsepower = $horsepower;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * @param mixed $mileage
     * @return CarTypeModel
     */
    public function setMileage($mileage)
    {
        $this->mileage = $mileage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * @param mixed $transmission
     * @return CarTypeModel
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param mixed $vin
     * @return CarTypeModel
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return CarTypeModel
     */
    public function setHash(string $hash = null)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUploadedMedia()
    {
        return $this->uploadedMedia;
    }

    /**
     * @param mixed $uploadedMedia
     * @return CarTypeModel
     */
    public function setUploadedMedia($uploadedMedia)
    {
        $this->uploadedMedia = $uploadedMedia;
        return $this;
    }

    public function getArrayCopy()
    {
        return [
            'type' => 'car',
            'brand' => $this->brand,
            'model' => $this->model,
            'body' => $this->body,
            'year' => $this->year,
            'fuel' => $this->fuel,
            'cc' => $this->engineCapacity,
            'hp' => $this->horsepower,
            'mileage' => $this->mileage,
            'transmission' => $this->transmission,
            'vin' => $this->vin,
            'media' => $this->media
        ];
    }
}