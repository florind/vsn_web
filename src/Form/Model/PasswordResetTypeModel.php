<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PasswordResetTypeModel
{
    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Email(
     *     message = "error.email_format",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     * @Assert\Regex(
     *     pattern = "/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/",
     *     htmlPattern = false,
     *     message = "error.password_strength"
     * )
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     */
    private $repeat_password;

    /**
     * @var int
     * @Assert\NotBlank(
     *      message = "error.required"
     * )
     */
    private $token;

    public function __construct($email, $password, $repeatPassword, $authorizationCode)
    {
        $this->email = $email;
        $this->password = $password;
        $this->repeat_password = $repeatPassword;
        $this->token = $authorizationCode;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getToken()
    {
        return $this->token;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->password !== $this->repeat_password) {
            $context->buildViolation('error.password_match')
                ->atPath('repeat_password')
                ->addViolation();
        }
    }
}