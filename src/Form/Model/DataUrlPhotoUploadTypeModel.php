<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class DataUrlPhotoUploadTypeModel
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $url;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}