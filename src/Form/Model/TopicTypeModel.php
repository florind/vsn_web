<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class TopicTypeModel implements \JsonSerializable
{
    /**
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @Assert\NotBlank()
     */
    private $text;
    private $hash;
    private $media;

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    public function getArrayCopy()
    {
        return [
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'media' => $this->getMedia()
        ];
    }

    function jsonSerialize()
    {
        return $this->getArrayCopy();
    }
}