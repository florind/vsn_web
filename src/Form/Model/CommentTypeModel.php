<?php
namespace App\Form\Model;

class CommentTypeModel implements \JsonSerializable
{
    private $hash;
    private $text;
    private $media;

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    public function getArrayCopy()
    {
        return [
            'text' => $this->getText(),
            'media' => $this->getMedia()
        ];
    }

    function jsonSerialize()
    {
        return $this->getArrayCopy();
    }
}