<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ActivationCodeModel
{
    /**
     * @var string
     * @assert\NotBlank()
     * @Assert\Email(
     *     message = "invalid_email",
     *     checkMX = true
     * )
     */
    private $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

}