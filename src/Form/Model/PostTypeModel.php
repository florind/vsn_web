<?php
namespace App\Form\Model;

use App\Validator\Constraints\Post;

/**
 * Class PostTypeModel
 * @package App\Form\Model
 * @Post()
 */
class PostTypeModel
{
    private $hash;
    private $text;
    private $media;
    private $uploadedMedia;

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return PostTypeModel
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return PostTypeModel
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     * @return PostTypeModel
     */
    public function setMedia($media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUploadedMedia()
    {
        return $this->uploadedMedia;
    }

    /**
     * @param mixed $uploadedMedia
     * @return PostTypeModel
     */
    public function setUploadedMedia($uploadedMedia)
    {
        $this->uploadedMedia = $uploadedMedia;
        return $this;
    }

    public function getArrayCopy()
    {
        return [
            'text' => $this->getText(),
            'media' => $this->getMedia()
        ];
    }
}