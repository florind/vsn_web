<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class EditProfileTypeModel
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "firstname.min_length",
     *     maxMessage = "firstname.max_length",
     * )
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = "lastname.min_length",
     *     maxMessage = "lastname.max_length",
     * )
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "blank_gender"
     * )
     * @Assert\Choice(
     *     choices = {RegisterTypeModel::GENDER_MALE, RegisterTypeModel::GENDER_FEMALE},
     *     message = "invalid_gender"
     * )
     */
    private $gender;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $year;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $month;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $day;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $locality;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName = null)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName = null)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender = null)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear(string $year = null)
    {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param string $month
     */
    public function setMonth(string $month = null)
    {
        $this->month = $month;
    }

    /**
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param string $day
     */
    public function setDay(string $day = null)
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * @param mixed $locality
     */
    public function setLocality(string $locality = null)
    {
        $this->locality = $locality;
    }

    public function getBirthday()
    {
        return sprintf('%s-%s-%s', $this->year, $this->month, $this->day);
    }
}