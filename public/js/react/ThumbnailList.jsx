var ThumbnailList = React.createClass({
    render: function () {
        return(
            <div className="thumbnail-list">
                <ul>
                    {
                       this.props.media.map((thumbnail, index) => {
                            return(
                                <li key={index}>
                                    <Thumbnail thumbnail={thumbnail}/>
                                </li>
                            );
                        })
                    }
                </ul>
            </div>
        );
    }
});

var Thumbnail = React.createClass({
    render: function () {
        let thumbnail = this.props.thumbnail;

        if (thumbnail._thumbnailProps.loading) {
            return(
                <i className="placeholder-loading"></i>
            );
        } else if (thumbnail._thumbnailProps.unloading) {
            return(
                <i className="placeholder-unloading"></i>
            );
        } else {
            return(
                <i onClick={thumbnail._thumbnailProps.click.bind(null, thumbnail)} style={{backgroundImage: 'url(' + Utils.imgUrl(thumbnail.filename) + ')'}}></i>
            );
        }
    },
});
