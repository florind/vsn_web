var ActivateAccount = React.createClass({
    getInitialState: function () {
        return {violations: {}};
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function () {
        return (
            <article className="posting no-border">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('activation_title')}</span>
                    </section>
                    <label htmlFor="submit-form" >{Translator.trans('save')}</label>
                </header>
                <section className="body text-center">
                    <form onSubmit={this.submit} noValidate>
                        <section>
                            <p><strong>{Translator.trans('activation_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('activation_description')}</p>
                            <div className="form-group">
                                <input type="text" className="form-control" name="email" placeholder={Translator.trans('email')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('email') &&
                                this.renderErrors(this.state.violations.email)
                            }
                            <div className="form-group">
                                <input type="text" className="form-control" name="token" placeholder={Translator.trans('token')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('token') &&
                                this.renderErrors(this.state.violations.token)
                            }

                            <input type="submit" id="submit-form" className="hide"/>
                        </section>
                    </form>
                </section>
            </article>
        );
    },

    renderErrors: function (errors) {
        return (
            <div className="alert alert-danger">
                {errors.map((error, index) => {return (<span key={index}>{Translator.trans(error)} </span>)})}
            </div>
        );
    },

    exit: function (e) {
        e.preventDefault();

        let eventId = Utils.confirmbox('confirm.unsaved_changes.title', 'confirm.unsaved_changes.message');
        document.addEventListener(eventId, this.cancelRegistration);
    },

    cancelRegistration: function(event) {
        if (event.detail.confirmed) {
            this.closeModal();
        }

        document.removeEventListener(event.type, this.cancelRegistration);
    },

    closeModal: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    submit: function (e) {
        e.preventDefault();

        XHR.post(Routing.generate('activate_account'), new FormData(e.target), function(xhr){
            switch (xhr.status) {
                case 200:
                    this.closeModal();
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    this.state.violations = errors;
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message')
            }
        }.bind(this));
    },
});