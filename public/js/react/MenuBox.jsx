var MenuBox = React.createClass({
    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function() {
        return(
            <div className="menu-box">
                <div className="backdrop" onClick={this.dismiss}></div>
                <div className="panel section-panel card-panel">
                    <div className="panel-body">
                        <ul>
                            {
                                this.props.items.map((item, index) => {
                                    return(<MenuBoxItem key={index} item={item} dismiss={this.dismiss}/>);
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    },

    dismiss: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    }
});

var MenuBoxItem = React.createClass({
    render: function(){
        return(
            <li className="menu-item" onClick={this.handleClick}>
                <i className={this.props.item.icon}></i>
                <span>{this.props.item.name}</span>
            </li>
        );
    },

    handleClick: function(e) {
        this.props.dismiss();
        this.props.item.event(e);
    }
});