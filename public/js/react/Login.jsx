var Login = React.createClass({
    render: function () {
        return (
            <article className="posting no-border">
                <section className="body text-center">
                    <h4>{Translator.trans('authenticate_title')}</h4>
                    {
                        this.props.error &&
                        <div className="alert alert-danger" role="alert">{Translator.trans('invalid_credentials')}</div>
                    }
                    <form action={Routing.generate('login')} method="post">
                        <div className="form-group">
                            <input type="email" className="form-control" placeholder={Translator.trans('email')} name="_username" defaultValue={this.props.username}/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" placeholder={Translator.trans('password')} name="_password"/>
                        </div>
                        <div className="form-group">
                            {/*<button data-sitekey="{{ google_recaptcha_site_key }}" data-callback='onSubmit' type="submit" className="btn btn-success col-xs-12 g-recaptcha">{{ "login.submit"|trans({}, "forms") }}</button>*/}
                            <button type="submit" className="btn btn-success btn-block">{Translator.trans('login')}</button>
                        </div>
                        <div className="form-group">
                            <a href="#" onClick={this.resetPassword}>{Translator.trans('forgot_password')}</a>
                        </div>
                        <div className="form-group">
                            <a href="#" onClick={this.register}>{Translator.trans('signup')}</a>
                        </div>
                        <div className="form-group">
                            <a href="#" onClick={this.activateAccount}>{Translator.trans('activate_account')}</a>
                        </div>
                    </form>
                </section>
            </article>
        );
    },

    resetPassword: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(PasswordReset, {}, null));
    },

    register: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(Register, {}, null));
    },

    activateAccount: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(ActivateAccount, {}, null));
    }
});