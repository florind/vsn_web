var Loader = React.createClass({
    render: function () {
        return(
            <article className="posting loader no-border">
                <section className="body">
                    <i className="fa fa-fw fa-spin fa-2x fa-circle-o-notch"></i>
                </section>
            </article>
        );
    }
});