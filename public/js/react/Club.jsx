var Club = React.createClass({
    render: function() {
        let club = this.props.club;
        return(
            <article className="posting" onClick={this.clubUrl}>
                <header>
                    <section className="profile">
                        <section className="avatar">
                            <img src={Utils.imgUrl(club.logo)} alt=""/>
                        </section>
                        <section className="details">
                            <strong>{club.name}</strong>
                            <span>{club.memberCount} {Translator.trans('members')}</span>
                        </section>
                    </section>
                </header>
            </article>
        );
    },

    clubUrl: function(){
        window.location.href = Routing.generate('club/view', {'club': this.props.club.hash});
    }
});