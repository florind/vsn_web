var Topic = React.createClass({
    getInitialState: function () {
        return {
            page: 1,
            perPage: 15,
            total: 15,
            topic: this.props.topic,
            comments: [],
            isLoading: false,
            isFetching: false,
            noComments: false,
            contextMenuFrozen: false,
        };
    },

    componentDidMount: function() {
        document.addEventListener(Utils.event.TOPIC_UPDATED, this.refresh);
        document.addEventListener(Utils.event.COMMENT_UPDATED, this.handleCommentUpdated);
        this.fetchComments(this.state.page, this.state.perPage);
    },

    componentWillUnmount: function () {
        document.removeEventListener(Utils.event.TOPIC_UPDATED, this.refresh);
        document.removeEventListener(Utils.event.COMMENT_UPDATED, this.handleCommentUpdated);
    },

    render: function () {
        return(
            <section>
                <article className="posting" key={this.state.topic.hash}>
                    <header>
                        <section className="profile" onClick={this.profile}>
                            <section className="avatar">
                                <img src={Utils.imgUrl(this.state.topic.author.avatar)} alt=""/>
                            </section>
                            <section className="details">
                                <a href="#">{this.state.topic.author.first_name+" "+this.props.topic.author.last_name}</a>
                                <span>{moment(this.state.topic.timestamp).fromNow()}</span>
                            </section>
                        </section>
                        {
                            !this.state.contextMenuFrozen &&
                            <section className="options" onClick={this.contextMenu}>
                                <a href="#"><i className="fa fa-fw fa-ellipsis-v"></i></a>
                            </section>
                        }
                        {
                            this.state.contextMenuFrozen &&
                            <section className="options">
                                <i className="fa fa-fw fa-spinner fa-pulse"></i>
                            </section>
                        }
                    </header>
                    <section className="body">
                        <section>
                            {
                                this.state.topic.locked &&
                                <div className="alert alert-danger" role="alert">
                                    <i className="fa fa-fw fa-lock"></i> {Translator.trans('topic_locked')}
                                </div>
                            }
                        </section>
                        <h4>{this.state.topic.title}</h4>
                        {
                            Utils.splitIntoParagraphs(this.state.topic.text).map((paragraph, index) => {
                                return(<p key={index}>{paragraph}</p>)
                            })
                        }
                    </section>
                    {
                        this.state.topic.media.length > 0 &&
                        <ThumbnailGallery media={this.state.topic.media} />
                    }
                    <footer>
                        <section className="stats">
                            <div className="text-muted">
                                {
                                    this.state.topic.likes > 0 &&
                                    <a href="#" onClick={this.listLikes}>{Translator.transChoice('likes', this.state.topic.likes, {value: this.state.topic.likes})}</a>
                                }
                                {
                                    this.state.topic.likes > 0 && this.state.topic.comments > 0 &&
                                    <br/>
                                }
                                {
                                    this.state.topic.comments > 0 &&
                                    <span>{this.state.topic.comments} {Translator.trans('comments')}</span>
                                }
                            </div>
                            <div className="actions">
                                {
                                    this.state.topic.permissions.indexOf('LIKE') !== -1 &&
                                    <a href="#" onClick={this.like}><i className="fa fa-fw fa-heart-o"></i> {Translator.trans('like')}</a>
                                }
                                {
                                    this.state.topic.permissions.indexOf('UNLIKE') !== -1 &&
                                    <a href="#" onClick={this.unlike}><i className="fa fa-fw fa-heart"></i> {Translator.trans('like')}</a>
                                }
                                <a href="#" onClick={this.addComment}>
                                    <i className="fa fa-fw fa-comments-o"></i> {Translator.trans('comment')}
                                </a>
                            </div>
                        </section>
                    </footer>
                </article>
                {
                    this.state.total > this.state.perPage &&
                    this.state.comments.length > 0 &&
                    <Pagination isLoading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
                {
                    this.state.isFetching &&
                    <Loader/>
                }
                {
                    !this.state.isFetching &&
                    this.state.comments.map((comment, index) => {
                        return(<Comment club={this.props.club} topic={this.state.topic.hash} locked={this.state.topic.locked} delete={this.deleteComment} comment={comment} key={index} csrf={this.props.csrf}/>);
                    })
                }
                {
                    this.state.noComments &&
                    <div className="noresults">{Translator.trans('no_comments')}</div>
                }
                {
                    this.state.total > this.state.perPage &&
                    this.state.comments.length > 0 &&
                    <Pagination isLoading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
            </section>
        );
    },

    addComment: function (e) {
        e.preventDefault();

        if (this.state.topic.locked) {
            Utils.infobox('info', 'alert.comments_locked.title', 'alert.comments_locked.message');
            return;
        }

        Utils.modal(React.createElement(
            CommentForm,
            {club: this.props.club, topic: this.state.topic.hash, csrf: this.props.csrf.comment},
            null
        ));
    },

    refresh: function () {
        XHR.get(Routing.generate('topic/reload', {
            club: this.props.club,
            topic: this.state.topic.hash
        }), function(xhr) {
            if (xhr.status === 200) {
                this.state.topic = JSON.parse(xhr.responseText);
                this.setState(this.state);
            }

            this.state.isLoading = false;
        }.bind(this));
    },

    like: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/like', {hash: this.state.topic.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    unlike: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/unlike', {hash: this.state.topic.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    listLikes: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(LikesList, {node: this.state.topic.hash}));
    },

    profile: function() {
        window.location.href = Routing.generate('profile', {id: this.state.topic.author.hash});
    },

    contextMenu: function (e) {
        e.preventDefault();

        let menuItems = [];

        if (this.state.topic.permissions.indexOf('UNLOCK') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('unlock_topic'), 'fa fa-fw fa-unlock', this.confirmUnlock));
        }

        if (this.state.topic.permissions.indexOf('UNFOLLOW') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('unfollow_topic'), 'fa fa-fw fa-eye-slash', this.unfollow));
        }

        if (this.state.topic.permissions.indexOf('FOLLOW') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('follow_topic'), 'fa fa-fw fa-eye', this.follow));
        }

        if (this.state.topic.permissions.indexOf('EDIT') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('edit'), 'fa fa-fw fa-pencil', this.edit));
        }

        if (this.state.topic.permissions.indexOf('DELETE') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('delete'), 'fa fa-fw fa-trash-o', this.confirmDelete));
        }

        if (this.state.topic.permissions.indexOf('LOCK') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('lock_topic'), 'fa fa-fw fa-lock', this.confirmLock));
        }

        Utils.menubox(menuItems);
    },

    edit: function () {
        Utils.modal(React.createElement(
            TopicForm,
            {club: this.props.club, topic: this.state.topic, csrf: this.props.csrf.topic},
            null
        ));
    },

    confirmDelete: function () {
        console.log('implement topic delete');
    },

    follow: function () {
        let url = Routing.generate('topic/follow', {club: this.props.club, topic: this.state.topic.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.state.topic.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    unfollow: function () {
        let url = Routing.generate('topic/unfollow', {club: this.props.club, topic: this.state.topic.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.state.topic.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    confirmLock: function () {
        let eventId = Utils.confirmbox(
            Translator.trans('confirm.topic_lock.title'),
            Translator.trans('confirm.topic_lock.message')
        );

        document.addEventListener(eventId, this.lock);
    },

    confirmUnlock: function () {
        let eventId = Utils.confirmbox(
            Translator.trans('confirm.topic_unlock.title'),
            Translator.trans('confirm.topic_unlock.message')
        );
        document.addEventListener(eventId, this.unlock);
    },

    lock: function () {
        document.removeEventListener(event.type, this.lock);

        if (this.state.topic.locked) {
            return;
        }

        this.freezeContextMenu();

        XHR.post(Routing.generate('topic/lock', {club: this.props.club, topic: this.state.topic.hash}), null, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.state.topic.locked = true;
                    this.state.topic.permissions = JSON.parse(xhr.responseText);
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }
            this.unfreezeContextMenu();
        }.bind(this));
    },

    unlock: function () {
        document.removeEventListener(event.type, this.unlock);

        if (!this.state.topic.locked) {
            return;
        }

        this.freezeContextMenu();

        XHR.post(Routing.generate('topic/unlock', {club: this.props.club, topic: this.state.topic.hash}), null, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.state.topic.locked = false;
                    this.state.topic.permissions = JSON.parse(xhr.responseText);
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    freezeContextMenu: function () {
        this.state.contextMenuFrozen = true;
        this.setState(this.state);
    },

    unfreezeContextMenu: function () {
        this.state.contextMenuFrozen = false;
        this.setState(this.state);
    },

    handleCommentUpdated: function (event) {
        let id = event.detail.comment;

        for (let i = 0; i < this.state.comments.length ; i++) {
            if (this.state.comments[i].hash === id) {
                this.refreshComment(id, i);
                return;
            }
        }
    },

    refreshComment: function (id, index) {
        XHR.get(Routing.generate('comment/reload', {
            club: this.props.club,
            topic: this.props.topic.hash,
            comment: id
        }), function(xhr) {
            if (xhr.status === 200) {
                this.state.comments[index] = JSON.parse(xhr.responseText);
                this.setState(this.state);
            }
        }.bind(this));
    },

    loadPage: function(page) {
        this.state.page = page;
        this.fetchComments(this.state.page, this.state.perPage);
    },

    fetchComments: function(page, perPage) {
        let url = Routing.generate('comments', {
            club: this.props.club,
            topic: this.props.topic.hash,
            page: page,
            limit: perPage
        });

        this.setFetching(true);

        XHR.get(url, function (xhr) {
            if (xhr.status === 200) {
                let response = JSON.parse(xhr.responseText);

                if (response.comments.length > 0) {
                    this.state.total = response.count;
                    this.state.comments = response.comments;
                }

                this.state.noComments = response.comments.length === 0;
            }

            this.setFetching(false);
        }.bind(this));
    },

    setFetching: function (status) {
        this.setState({
            isFetching: status,
            comments: this.state.comments
        });
    },

    deleteComment: function (comment) {
        if (this.state.comments.indexOf(comment) >= 0) {
            let url = Routing.generate('comments/delete', {club: this.props.club, topic: this.props.topic.hash, comment: comment.hash});

            XHR.delete(url, function (commentIndex, xhr) {
                switch (xhr.status) {
                    case 200:
                        this.state.comments.splice(commentIndex,1);
                        this.setState(this.state);
                        break;
                    default:
                        Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this, this.state.comments.indexOf(comment)));
        }
    }

});

var Comment = React.createClass({
    getInitialState: function () {
        return {
            isLoading: false,
            contextMenuFrozen: false
        };
    },

    render: function () {
        let paragraphs = Utils.splitIntoParagraphs(this.props.comment.text);
        return(
            <article className="posting">
                <header>
                    <section className="profile" onClick={this.viewProfile}>
                        <section className="avatar">
                            <img src={Utils.imgUrl(this.props.comment.author.avatar)} alt=""/>
                        </section>
                        <section className="details">
                            <a href="#">{this.props.comment.author.first_name+" "+this.props.comment.author.last_name}</a>
                            <span>{moment(this.props.comment.timestamp).fromNow()}</span>
                        </section>
                    </section>
                    {
                        !this.state.contextMenuFrozen &&
                        <section className="options" onClick={this.contextMenu}>
                            <a href="#"><i className="fa fa-fw fa-ellipsis-v"></i></a>
                        </section>
                    }
                    {
                        this.state.contextMenuFrozen &&
                        <section className="options">
                            <i className="fa fa-fw fa-spinner fa-pulse"></i>
                        </section>
                    }
                </header>
                <section className="body">
                    {
                        paragraphs.map((paragraph, index) => {
                            return(<p key={index}>{paragraph}</p>)
                        })
                    }
                </section>
                {
                    this.props.comment.media.length > 0 &&
                    <ThumbnailGallery media={this.props.comment.media} />
                }
                <footer>
                    <section className="stats">
                        <div className="text-muted">
                            {
                                this.props.comment.likes > 0 &&
                                <a href="#" onClick={this.listLikes}>{Translator.transChoice('likes', this.props.comment.likes, {value: this.props.comment.likes})}</a>
                            }
                            {
                                this.props.comment.likes > 0 && this.props.comment.replies > 0 &&
                                <br/>
                            }
                            {
                                this.props.comment.replies > 0 &&
                                <a href="#" onClick={this.viewReplies}>{this.props.comment.replies} {Translator.trans('comments')}</a>
                            }
                        </div>
                        <div className="actions">
                            {
                                this.props.comment.permissions.indexOf('LIKE') !== -1 &&
                                <a href="#" onClick={this.like}><i className="fa fa-fw fa-heart-o"></i> {Translator.trans('like')}</a>
                            }
                            {
                                this.props.comment.permissions.indexOf('UNLIKE') !== -1 &&
                                <a href="#" onClick={this.unlike}><i className="fa fa-fw fa-heart"></i> {Translator.trans('like')}</a>
                            }

                            <a href="#" onClick={this.reply}><i className="fa fa-fw fa-comments-o"></i> {Translator.trans('comment')}</a>
                        </div>
                    </section>
                </footer>
            </article>
        );
    },

    refresh: function () {
        Utils.event.dispatch(Utils.event.COMMENT_UPDATED, {comment: this.props.comment.hash});
        this.state.isLoading = false;
    },

    like: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/like', {hash: this.props.comment.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    unlike: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/unlike', {hash: this.props.comment.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    listLikes: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(LikesList, {node: this.props.comment.hash}));
    },

    contextMenu: function (e) {
        e.preventDefault();
        let menu = [];

        if (this.props.locked) {
            return;
        }

        if (this.props.comment.permissions.indexOf('UNFOLLOW') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('unfollow_comment'), 'fa fa-fw fa-eye-slash', this.unfollow));
        }

        if (this.props.comment.permissions.indexOf('FOLLOW') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('follow_comment'), 'fa fa-fw fa-eye', this.follow));
        }

        if (this.props.comment.permissions.indexOf('EDIT') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('edit'), 'fa fa-fw fa-pencil', this.edit));
        }

        if (this.props.comment.permissions.indexOf('DELETE') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('delete'), 'fa fa-fw fa-trash-o', this.confirmDelete));
        }

        Utils.menubox(menu);
    },

    follow: function () {
        let url = Routing.generate('comments/follow', {club: this.props.club, topic: this.props.topic, comment: this.props.comment.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.props.comment.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    unfollow: function () {
        let url = Routing.generate('comments/unfollow', {club: this.props.club, topic: this.props.topic, comment: this.props.comment.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.props.comment.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    viewReplies: function (e) {
        e.preventDefault();

        Utils.modal(React.createElement(
            ReplyList,
            {
                club: this.props.club,
                topic: this.props.topic,
                locked: this.props.locked,
                comment: this.props.comment,
                csrf: this.props.csrf.reply
            },
            null
        ));
    },

    reply: function (e) {
        e.preventDefault();

        if (this.props.locked) {
            Utils.infobox('info', 'Topic Locked', 'You cannot add replies to a locked topic');
            return;
        }

        Utils.modal(React.createElement(
            ReplyForm,
            {club: this.props.club, topic: this.props.topic, comment: this.props.comment, csrf: this.props.csrf.reply},
            null
        ));
    },

    edit: function () {
        Utils.modal(React.createElement(
            CommentForm,
            {
                club: this.props.club,
                topic: this.props.topic,
                comment: this.props.comment,
                csrf: this.props.csrf.comment
            },
            null
        ));
    },

    confirmDelete: function () {
        let eventId = Utils.confirmbox('confirm.delete_comment.title', 'confirm.delete_comment.message');
        document.addEventListener(eventId, this.delete);
    },

    delete: function (event) {
        if (event.detail.confirmed) {
            this.props.delete(this.props.comment);
        }
        document.removeEventListener(event.type, this.delete);
    },

    viewProfile: function() {
        window.location.href = Routing.generate('profile', {id: this.props.topic.author.hash});
    },

    freezeContextMenu: function () {
        this.state.contextMenuFrozen = true;
        this.setState(this.state);
    },

    unfreezeContextMenu: function () {
        this.state.contextMenuFrozen = false;
        this.setState(this.state);
    },
});

var CommentQuote = React.createClass({
    render: function () {
        let name = this.props.comment.author.first_name+" "+this.props.comment.author.last_name;
        let paragraphs = Utils.splitIntoParagraphs(this.props.comment.text);

        return(
            <details className="comment-summary">
                <summary>{name} wrote {moment(this.props.comment.timestamp).fromNow()}</summary>
                <blockquote>
                    {
                        paragraphs.map((paragraph, index) => {
                            return(<p key={index}>{paragraph}</p>)
                        })
                    }
                    {
                        this.props.comment.media.length > 0 &&
                        <details>
                            <summary>{this.props.comment.media.length} attachments</summary>
                            <ThumbnailGallery media={this.props.comment.media} />
                        </details>
                    }
                </blockquote>
            </details>
        );
    }
});

var ReplyList = React.createClass({
    getInitialState: function () {
        return {
            page: 1,
            perPage: 15,
            total: 15,
            isFetching: false,
            replies: []
        };
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
        document.addEventListener(Utils.event.REPLY_UPDATED, this.handleReplyUpdates);
        this.fetchReplies(this.state.page, this.state.perPage);
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
        document.removeEventListener(Utils.event.REPLY_UPDATED, this.handleReplyUpdates);
    },

    render: function () {
        return(
            <article className="posting">
                <header>
                    <section>
                        <a href="#" onClick={this.dismiss}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('reply_form_title')}</span>
                    </section>
                    <a href="#" onClick={this.reply}>{Translator.trans('comment')}</a>
                </header>
                <section>
                    {
                        this.state.total > this.state.perPage &&
                        this.state.replies.length > 0 &&
                        <Pagination isloading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                    }
                    {
                        this.state.isFetching &&
                        <Loader/>
                    }
                    {
                        !this.state.isFetching &&
                        this.state.replies.map((reply, index) => {
                            return(<Reply club={this.props.club} topic={this.props.topic} locked={this.props.locked} delete={this.delete} comment={this.props.comment} reply={reply} key={index} csrf={this.props.csrf} />)
                        })
                    }
                    {
                        this.state.total > this.state.perPage &&
                        this.state.replies.length > 0 &&
                        <Pagination isloading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                    }
                </section>
            </article>
        );
    },

    reply: function (e) {
        e.preventDefault();

        if (this.props.locked) {
            Utils.infobox('info', 'Topic Locked', 'You cannot add replies to a locked topic');
            return;
        }

        Utils.modal(React.createElement(
            ReplyForm,
            {club: this.props.club, topic: this.props.topic, comment: this.props.comment, csrf: this.props.csrf},
            null
        ));
    },

    dismiss: function (e) {
        e.preventDefault();
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    handleReplyUpdates: function (event) {
        let id = event.detail.reply;
        for (let i = 0; i < this.state.replies.length; i++) {
            if (this.state.replies[i].hash === id) {
                this.refreshReply(id, i);
                return;
            }
        }
    },

    refreshReply: function (replyId, index) {
        XHR.get(Routing.generate('replies/get', {
            club: this.props.club,
            topic: this.props.topic,
            comment: this.props.comment.hash,
            reply: replyId
        }), function(xhr) {
            if (xhr.status === 200) {
                this.state.replies[index] = JSON.parse(xhr.responseText);
                this.setState(this.state);
            }
        }.bind(this));
    },

    loadPage: function(page) {
        this.state.page = page;
        this.fetchReplies(this.state.page, this.state.perPage);
    },

    fetchReplies: function(page, perPage) {
        let url = Routing.generate('replies', {
            club: this.props.club,
            topic: this.props.topic,
            comment: this.props.comment.hash,
            page: page,
            limit: perPage
        });

        this.state.isFetching = true;
        this.setState(this.state);

        XHR.get(url, function (xhr) {
            if (xhr.status === 200) {
                let response = JSON.parse(xhr.responseText);

                if (response.replies.length) {
                    this.state.replies = response.replies;
                    this.state.isFetching = false;
                    this.state.total = response.count;
                    this.setState(this.state);
                }
            }
        }.bind(this));
    },

    delete: function (reply) {
        let index = this.state.replies.indexOf(reply);
        this.state.replies.splice(index, 1);
        this.setState(this.state);
    },
});

var Reply = React.createClass({
    getInitialState: function () {
        return {isLoading: false};
    },

    render: function () {
        let paragraphs = Utils.splitIntoParagraphs(this.props.reply.text);
        return(
            <article className="posting">
                <header>
                    <section className="profile" onClick={this.viewProfile}>
                        <section className="avatar">
                            <img src={Utils.imgUrl(this.props.reply.author.avatar)} alt=""/>
                        </section>
                        <section className="details">
                            <a href="#">{this.props.reply.author.first_name+" "+this.props.reply.author.last_name}</a>
                            <span>{moment(this.props.reply.timestamp).fromNow()}</span>
                        </section>
                    </section>
                    <section className="options" onClick={this.contextMenu}>
                        <a href="#"><i className="fa fa-fw fa-ellipsis-v"></i></a>
                    </section>
                </header>
                <section className="body">
                    {
                        paragraphs.map((paragraph, index) => {
                            return(<p key={index}>{paragraph}</p>)
                        })
                    }
                </section>
                {
                    this.props.reply.media.length > 0 &&
                    <ThumbnailGallery media={this.props.reply.media} />
                }
                <footer>
                    <section className="stats">
                        <div className="text-muted">
                            {
                                this.props.reply.likes > 0 &&
                                <a href="#" onClick={this.listLikes}>{Translator.transChoice('likes', this.props.reply.likes, {value: this.props.reply.likes})}</a>
                            }
                        </div>
                        <div className="actions">
                            {
                                this.props.reply.permissions.indexOf('LIKE') !== -1 &&
                                <a href="#" onClick={this.like}><i className="fa fa-fw fa-heart-o"></i> {Translator.trans('like')}</a>
                            }
                            {
                                this.props.reply.permissions.indexOf('UNLIKE') !== -1 &&
                                <a href="#" onClick={this.unlike}><i className="fa fa-fw fa-heart"></i> {Translator.trans('like')}</a>
                            }
                        </div>
                    </section>
                </footer>
            </article>
        );
    },

    refresh: function () {
        Utils.event.dispatch(Utils.event.REPLY_UPDATED, {reply: this.props.reply.hash});
        this.state.isLoading = false;
    },

    like: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/like', {hash: this.props.reply.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    unlike: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/unlike', {hash: this.props.reply.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    listLikes: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(LikesList, {node: this.props.reply.hash}));
    },

    contextMenu: function (e) {
        e.preventDefault();

        if (this.props.locked) {
            return;
        }

        let menu = [];

        if (this.props.comment.permissions.indexOf('EDIT') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('edit'), 'fa fa-fw fa-pencil', this.edit));
        }

        if (this.props.comment.permissions.indexOf('DELETE') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('delete'), 'fa fa-fw fa-trash-o', this.confirmDelete));
        }

        Utils.menubox(menu);
    },

    edit: function () {
        Utils.modal(React.createElement(
            ReplyForm,
            {
                club: this.props.club,
                topic: this.props.topic,
                comment: this.props.comment,
                reply: this.props.reply,
                csrf: this.props.csrf
            },
            null
        ));
    },

    confirmDelete: function () {
        let eventId = Utils.confirmbox('confirm.delete_comment.title', 'confirm.delete_comment.message');
        document.addEventListener(eventId, this.deleteReply);
    },

    deleteReply: function (event) {
        if (event.detail.confirmed) {
            let url = Routing.generate('replies/delete', {
                club: this.props.club,
                topic: this.props.topic,
                comment: this.props.comment.hash,
                reply: this.props.reply.hash
            });

            XHR.delete(url, function(xhr){
                this.props.delete(this.props.reply.hash);
            }.bind(this));
        }

        document.removeEventListener(event.type, this.deleteReply);
    },

    viewProfile: function() {
        window.location.href = Routing.generate('profile', {id: this.props.reply.author.hash});
    },
});