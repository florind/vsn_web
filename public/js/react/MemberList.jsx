var MemberList = React.createClass({
    getInitialState: function() {
        return {
            page: 1,
            perPage: 15,
            total: 15,
            isFetching: false,
            noMembers: false,
            members: []
        };
    },

    componentDidMount: function() {
        this.fetchMembers(this.state.page, this.state.perPage);
    },

    render: function() {
        return(
            <div className="members-list">
                {
                    this.state.total > this.state.perPage &&
                    this.state.members.length > 0 &&
                    <Pagination isLoading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
                {
                    this.state.noMembers &&
                    <p className="noresults">{Translator.trans('no_members')}</p>
                }
                {
                    this.state.isFetching &&
                    <Loader/>
                }
                {
                    !this.state.isFetching &&
                    this.state.members.map(member => {
                        return <Member key={member.hash} member={member} club={this.props.clubId}/>
                    })
                }
                {
                    this.state.total > this.state.perPage &&
                    this.state.members.length > 0 &&
                    <Pagination isloading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
            </div>
        );
    },

    loadPage: function(page) {
        this.state.page = page;
        this.fetchMembers(this.state.page, this.state.perPage);
    },

    fetchMembers: function(page, perPage) {
        let component = this;
        let url = Routing.generate('clubs/members', {
                club: component.props.clubId,
                page: page,
                limit: perPage
            });

        this.setFetching(true);

        XHR.get(url, function (xhr) {
            if (xhr.status === 200) {
                let response = JSON.parse(xhr.responseText);

                if (response.members.length > 0) {
                    this.state.total = response.count;
                    this.state.members = response.members;
                }

                this.state.noMembers = response.members.length === 0;
            }
            this.setFetching(false);
        }.bind(this));
    },

    setFetching: function (status) {
        this.state.isFetching = status;
        this.setState(this.state);
    },
});

var Member = React.createClass({
    render: function () {
        let member = this.props.member;
        return(
            <article className="posting">
                <header>
                    <section className="profile">
                        <section className="avatar" onClick={this.profile.bind(this, member.hash)}>
                            <img src={Utils.imgUrl(member.avatar)} alt=""/>
                        </section>
                        <section className="details" onClick={this.profile.bind(this, member.hash)}>
                            <a href="#">{member.firstName+ " " + member.lastName}</a>
                            <span>{member.locality}</span>
                            <span>{Translator.trans('member_since') + " " + moment(member.timestamp).format('D MMMM YYYY')}</span>
                        </section>
                        <section className="options">
                            <a href="#" onClick={this.contextMenu}><i className="fa fa-fw fa-ellipsis-v"></i></a>
                        </section>
                    </section>
                </header>
            </article>
        );
    },

    profile: function(id) {
        window.location.href = Routing.generate('profile', {id: id});
    },

    contextMenu: function (e) {
        e.preventDefault();
        let menu = [];

        if (this.props.member.permissions.indexOf('UNFOLLOW') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('unfollow_member'), 'fa fa-fw fa-eye-slash', this.unfollow));
        }

        if (this.props.member.permissions.indexOf('FOLLOW') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('follow_member'), 'fa fa-fw fa-eye', this.follow));
        }

        if (this.props.member.permissions.indexOf('BLOCK') !== -1) {
            menu.push(new MenuBoxEntry(Translator.trans('block_member'), 'fa fa-fw fa-trash-o', this.banMember));
        }

        Utils.menubox(menu);
    },

    follow: function () {

    },

    unfollow: function () {

    },

    banMember: function () {
        XHR.post(Routing.generate('club/ban', {club: this.props.club , member: this.props.member.hash}), null, function (xhr) {
            if (xhr.status === 200) {
                console.log('success');
            } else {
                console.log('failed');
            }
        }.bind(this));
    }
});