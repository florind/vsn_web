var TopicForm = React.createClass({
    KEY:'TOPIC',
    getInitialState: function () {
        if (typeof this.props.topic === 'undefined') {
            return {
                media: [],
                isProcessing: false
            };
        }

        if (this.props.topic !== null) {
            for (let i = 0; i < this.props.topic.media.length; i++) {
                this.props.topic.media[i]['_thumbnailProps'] = {
                    persistent: true,
                    loading: false,
                    unloading: false,
                    click: this.contextMenu
                };
            }
        }

        return {
            media: this.props.topic.media,
            isProcessing: false
        };
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function () {
        return(
            <article className="posting">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('topic_form_title')}</span>
                    </section>
                    <label htmlFor="submit_form" >{Translator.trans('save')}</label>
                </header>
                <section className="body">
                    <form name="topic" method="post" noValidate onSubmitCapture={this.submit}>
                        <input type="hidden" id="topic_hash" name="hash" value={this.props.topic && this.props.topic.hash}/>
                        <input type="hidden" id="topic_csrf" name="csrf" value={this.props.csrf}/>
                        <div className="form-group">
                            <input type="text" id="topic_title" ref="title" name="title" defaultValue={this.props.topic && this.props.topic.title} placeholder="short but descriptive title" className="form-control" autoComplete="off"/>
                        </div>
                        <div className="form-group">
                            <textarea id="topic_text" name="text" ref="body" defaultValue={this.props.topic && this.props.topic.text} rows="10" placeholder="What's on your mind?" className="form-control" autoComplete="off"></textarea>
                        </div>
                        <div className="form-group">
                            <div className="image-uploader">
                                <i className="fa fa-fw fa-cloud-upload"></i> add photos
                                <input onChange={this.uploadMedia} type="file" id="temporary_media" name="temporary_media" required="required" accept="image/*" multiple="multiple"/>
                            </div>
                        </div>
                        <div className="form-group">
                            <ThumbnailList media={this.state.media}/>
                        </div>
                        <button type="submit" id="submit_form" name="submit" className="hide" disabled={this.state.isProcessing}>save</button>
                    </form>
                </section>
            </article>
        );
    },

    exit: function (e) {
        e.preventDefault();

        if (
            this.hasUploadedMedia() ||
            this.topicHasChanged()
        ) {
            let eventId = Utils.confirmbox('confirm.unsaved_changes.title', 'confirm.unsaved_changes.message');
            document.addEventListener(eventId, this.cancelEdit);
        } else {
            this.dismissForm();
        }
    },

    cancelEdit: function(event) {
        if (event.detail.confirmed) {
            this.clearUploads();
            this.dismissForm();
        }

        document.removeEventListener(event.type, this.cancelEdit);
    },

    dismissForm: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    hasUploadedMedia: function () {
        for (let i  = 0; i < this.state.media.length; i++) {
            if (!this.state.media[i]._thumbnailProps.persistent) {
                return true;
            }
        }

        return false;
    },

    topicHasChanged: function () {
        let originalTitle = (typeof this.props.topic !== 'undefined') ? this.props.topic.title : '';
        let originalBody = (typeof this.props.topic !== 'undefined') ? this.props.topic.text : '';

        return (this.refs.title.value.trim() !== originalTitle) || (this.refs.body.value.trim() !== originalBody);
    },

    clearUploads: function () {
        let formData = new FormData();
        formData.append('csrf_key', this.KEY);
        formData.append('csrf_value', this.props.csrf);

        XHR.post(Routing.generate('images/uploads/clear'), formData);
    },

    addThumbnailPlaceholder: function() {
        let placeholder = {
            hash: null,
            type: null,
            filename: null,
            created: null,
            _thumbnailProps: {
                persistent: false,
                loading: true,
                unloading: false,
                click: (function(){})
            }
        };

        this.state.media.push(placeholder);
        this.setState(this.state);
    },

    replaceThumbnailPlaceholder: function (image) {
        for (let i = 0; i < this.state.media.length; i++) {
            if (this.state.media[i].hash === null) {
                let placeholder = this.state.media[i];
                placeholder.hash = image.hash;
                placeholder.filename = image.filename;
                placeholder._thumbnailProps.loading = false;
                placeholder._thumbnailProps.click = this.contextMenu;
                this.setState(this.state);
                break;
            }
        }
    },

    removeThumbnailPlaceholder: function () {
        for (let i = 0; i < this.state.media.length; i++) {
            if (this.state.media[i].id === null) {
                this.state.media.splice(i, 1);
                this.setState(this.state);
                return;
            }
        }
    },

    contextMenu: function (imageId) {
        Utils.menubox([
            new MenuBoxEntry(Translator.trans('delete'), 'fa fa-fw fa-trash-o', this.deleteMedia.bind(null, imageId))
        ]);
    },

    deleteMedia: function (media) {
        if (this.state.isProcessing) {
            return;
        }

        let url = media._thumbnailProps.persistent ?
            Routing.generate('images/delete', {nodeHash: this.props.topic.hash, mediaHash: media.hash}) :
            Routing.generate('images/uploads/remove', {image: media.hash});

        media._thumbnailProps.unloading = true;
        this.setState(this.state);

        let ajaxResponseCallback = function (media, xhr) {
            switch (xhr.status) {
                case 200:
                    this.state.media.splice(this.state.media.indexOf(media), 1);
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger','alert.request_failed.title', 'alert.request_failed.message');
                    media._thumbnailProps.unloading = false;
                    this.setState(this.state);
            }

            this.enableSubmit();
        }.bind(this, media);

        let formData = null;

        if (!media._thumbnailProps.persistent) {
            formData = new FormData();
            formData.append('csrf_key', this.KEY);
            formData.append('csrf_value', this.props.csrf);

            XHR.post(url, formData, ajaxResponseCallback);
        } else {
            XHR.delete(url, ajaxResponseCallback);
        }
    },

    uploadMedia: function (e) {
        for (let i =0; i < e.currentTarget.files.length; i++) {
            let formData = new FormData();
            formData.append('image', e.currentTarget.files[i]);
            formData.append('csrf_key', this.KEY);
            formData.append('csrf_value', this.props.csrf);

            this.disableSubmit();
            this.addThumbnailPlaceholder();

            XHR.post(Routing.generate('images/upload'), formData, function(xhr){
                switch (xhr.status) {
                    case 200:
                        this.replaceThumbnailPlaceholder(JSON.parse(xhr.responseText));
                        break;
                    case 400:
                        Utils.infobox('danger', 'alert.invalid_image_upload.title','alert.invalid_image_upload.message');
                        this.removeThumbnailPlaceholder();
                        break;
                    default:
                        Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                        this.removeThumbnailPlaceholder();
                }

                this.enableSubmit();
            }.bind(this));
        }
    },

    submit: function (e) {
        e.preventDefault();
        let isNew = (typeof this.props.topic === 'undefined');

        let formData = new FormData(e.currentTarget);
        let url = isNew ?
            Routing.generate('topic/create', {club: this.props.club}) :
            Routing.generate('topic/edit', {club: this.props.club, topic: this.props.topic.hash});

        this.disableSubmit();

        XHR.post(url, formData, function(xhr){
            switch (xhr.status) {
                case 200:
                    if (!isNew) {
                        Utils.event.dispatch(Utils.event.TOPIC_UPDATED, {topic: this.props.topic.hash});
                    }
                    this.dismissForm();
                    break;
                case 400:
                    Utils.infobox('danger', 'alert.invalid_topic.title', 'alert.invalid_topic.message');
                    this.enableSubmit();
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                    this.enableSubmit();
            }
        }.bind(this));
    },

    disableSubmit: function () {
        this.state.isProcessing = true;
        this.setState(this.state);
    },

    enableSubmit: function () {
        this.state.isProcessing = false;
        this.setState(this.state);
    }
});