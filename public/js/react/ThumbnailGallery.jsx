var ThumbnailGallery = React.createClass({
    render: function () {
        let media = this.props.media.slice(0, 5);
        let extra = this.props.media.length - 5;

        return(
            <section className="media">
            {
                media.map((item, index) => {
                    return(
                        <i key={item.hash} onClick={this.mediaGallery.bind(null, item)} className="img" style={{backgroundImage: 'url(' + Utils.imgUrl(item.filename) + ')'}}>
                            {
                                media.length === index + 1 && extra > 0 &&
                                <span className="extra">{"+" + extra}</span>
                            }
                        </i>
                    );
                })
            }
            </section>
        );
    },

    mediaGallery: function (item) {
        Utils.mediaGallery(this.props.media, item.hash);
    }
});