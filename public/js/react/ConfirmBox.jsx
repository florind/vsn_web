var ConfirmBox = React.createClass({
    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function() {
        return(
            <div className="confirm-box">
                <div className="backdrop"></div>
                <div className="panel section-panel card-panel">
                    <div className="panel-body">
                        <h3>{this.props.title}</h3>
                        <p>{this.props.message}</p>
                        <div className="nav">
                            <a href="javascript:void(0)" onClick={this.doHandle.bind(null, this.props.id, false)}>
                                {Translator.trans('no')}
                            </a>
                            <a href="javascript:void(0)" onClick={this.doHandle.bind(null, this.props.id, true)}>
                                {Translator.trans('yes')}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    doHandle: function (id, status) {
        let event = new CustomEvent(id, {detail: {confirmed: status}});
        document.dispatchEvent(event);
        this.dismiss();
    },

    dismiss: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    }
});