var ClubForm = React.createClass({
    cropper: null,
    logoPreview: null,

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
        this.logoPreview = document.getElementById('club-logo-preview');

        this.cropper = new Croppie(this.logoPreview, {
            viewport: 'square',
            showZoomer: false
        });
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
        this.cropper.destroy();
    },

    getInitialState: function () {
        return {
            loading: false,
            cursor: 0,
            sections: ['name', 'description', 'logo'],
            violations: {}
        };
    },

    render: function () {
        let progress = (this.state.cursor +1) / this.state.sections.length * 100;
        return(
            <article className="posting">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('create_club_form_title')}</span>
                    </section>
                    {
                        this.isLoading() &&
                        <label><i className="fa fa fw fa-spinner fa-pulse"></i></label>
                    }
                    {
                        !this.isLoading() && !this.cursorAt('logo') &&
                        <label onClick={this.next}>{Translator.trans('next')}</label>
                    }
                    {
                        !this.isLoading() && this.cursorAt('logo') &&
                        <label htmlFor="submit-form">{Translator.trans('save')}</label>
                    }
                </header>
                <section>
                    <div className="progress">
                        <div className="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100" style={{width: progress+"%"}}>
                        </div>
                    </div>
                </section>
                <section className="body text-center">
                    <form action="#" onSubmit={this.submit}>
                        <section className={this.cursorAt('name') ? "" : "hidden"}>
                            <p><strong>{Translator.trans('club_name_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('club_name_description')}</p>
                            <div className="form-group">
                                <input type="text" className="form-control" name="name"/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('name') &&
                                this.renderErrors(this.state.violations.name)
                            }
                        </section>

                        <section className={this.cursorAt('description') ? "" : "hidden"}>
                            <p><strong>{Translator.trans('club_description_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('club_description_description')}</p>
                            <div className="form-group">
                                <textarea className="form-control" name="description"/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('description') &&
                                this.renderErrors(this.state.violations.description)
                            }
                        </section>

                        <section className={this.cursorAt('logo') ? "" : "hidden"}>
                            <p><strong>{Translator.trans('club_logo_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('club_logo_description')}</p>
                            <div className="form-group">
                                <div className="image-uploader">
                                    <i className="fa fa-fw fa-cloud-upload"></i> add photos
                                    <input onChange={this.updateLogo} type="file" id="temporary_media" name="logo" required="required" accept="image/*"/>
                                </div>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('logo') &&
                                this.renderErrors(this.state.violations.logo)
                            }
                            <section id="club-logo-preview"></section>
                        </section>
                        <input type="submit" id="submit-form" className="hidden" placeholder={Translator.trans('club_description_placeholder')}/>
                    </form>
                </section>
            </article>
        );
    },

    renderErrors: function (errors) {
        return (
            <div className="alert alert-danger">
                {errors.map((error, index) => {return (<span key={index}>{Translator.trans(error)} </span>)})}
            </div>
        );
    },

    cursorAt: function (section) {
        return this.state.cursor === this.state.sections.indexOf(section);
    },

    updateLogo: function (e) {
        let input = e.target;

        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                this.logoPreview.classList.add('ready');
                this.cropper.bind({url: e.target.result});
            }.bind(this);

            reader.readAsDataURL(input.files[0]);
        }
    },

    next: function (e) {
        e.preventDefault();

        if (this.state.cursor < this.state.sections.length) {
            this.state.cursor++;
            this.setState(this.state);
        }
    },

    exit: function (e) {
        e.preventDefault();
        this.closeModal();
    },

    closeModal: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    submit: function (e) {
        e.preventDefault();

        let formData = new FormData(e.target);

        this.cropper.result('blob').then(function(blob){
            let logo = new File([blob], 'cropped');
            formData.append('logo-crop', logo);

            this.startLoading();

            XHR.post(Routing.generate('club/create'), formData, function(xhr){
                switch (xhr.status) {
                    case 200:
                        this.closeModal();
                        break;
                    case 400:
                        let errors = JSON.parse(xhr.responseText);
                        let fields = Object.keys(errors);
                        let index = this.state.sections.length;

                        for (let i = 0; i < fields.length; i++) {
                            let sectionIndex = this.state.sections.indexOf(fields[i]);

                            if (sectionIndex < index) {
                                index = sectionIndex;
                            }
                        }

                        this.state.cursor = index;
                        this.state.violations = errors;
                        this.setState(this.state);
                        break;
                    default:
                        Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message')
                }
                this.finishLoading();
            }.bind(this));
        }.bind(this));
    },

    isLoading: function () {
        return this.state.loading;
    },

    startLoading: function () {
        this.state.loading = true;
        this.setState(this.state);
    },

    finishLoading: function () {
        this.state.loading = false;
        this.setState(this.state);
    }
});