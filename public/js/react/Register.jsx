var Register = React.createClass({
    inputs: {
        first_name: null,
        last_name: null,
        birthday: null,
        email: null,
        password: null,
        repeat_password: null,
        gender: null
    },

    getInitialState: function () {
        return {
            loading: false,
            registered: false,
            cursor: 0,
            sections: ['name', 'birthday', 'email', 'password', 'gender', 'activation'],
            violations: {},
            sectionFieldsToValidate: {
                'name': ["first_name", "last_name"],
                'birthday': [],
                'email': ["email"],
                'password': ["password", "repeat_password"],
                'gender': ["gender"],
            },
            fieldsToSectionsMap: {
                'first_name': 'name',
                'last_name': 'name',
                'birthday': 'birthday',
                'email': 'email',
                'password': 'password',
                'repeat_password': 'password',
                'gender': 'gender',
                'token': 'activation'
            }
        };
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function () {
        let progress = (this.state.cursor +1) / this.state.sections.length * 100;
        return (
            <article className="posting no-border">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('register_title')}</span>
                    </section>
                    {
                        this.isLoading() &&
                        <label><i className="fa fa fw fa-spinner fa-pulse"></i></label>
                    }
                    {
                        !this.cursorAt('gender') && !this.cursorAt('activation') &&
                        <a href="#" onClick={this.next}>{Translator.trans('next')}</a>
                    }
                    {
                        !this.isLoading() && this.cursorAt('gender') &&
                        <label htmlFor="submit-form" >{Translator.trans('next')}</label>
                    }
                    {
                        !this.isLoading() && this.cursorAt('activation') &&
                        <label htmlFor="submit-form" >{Translator.trans('save')}</label>
                    }
                </header>
                <section>
                    <div className="progress">
                        <div className="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100" style={{width: progress+"%"}}>
                        </div>
                    </div>
                </section>
                <section className="body text-center">
                    <form onSubmit={this.submit} noValidate>
                        <section className={!this.cursorAt('name') && "hidden"}>
                            <p><strong>{Translator.trans('register_name_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('register_name_description')}</p>
                            <div className="form-group">
                                <input type="text" onChange={this.handleInput} className="form-control" name="first_name" placeholder={Translator.trans('firstname')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('first_name') &&
                                this.renderErrors(this.state.violations.first_name)
                            }
                            <div className="form-group">
                                <input type="text" onChange={this.handleInput} className="form-control" name="last_name" placeholder={Translator.trans('lastname')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('last_name') &&
                                this.renderErrors(this.state.violations.last_name)
                            }
                        </section>
                        <section className={!this.cursorAt('birthday') && "hidden"}>
                            <p><strong>{Translator.trans('register_birthdate_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('register_birthdate_description')}</p>
                            <DatePicker name="birthday"/>
                            {
                                this.state.violations.hasOwnProperty('birthday') &&
                                this.renderErrors(this.state.violations.birthday)
                            }
                        </section>
                        <section className={!this.cursorAt('email') && "hidden"}>
                            <p><strong>{Translator.trans('register_email_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('register_email_description')}</p>
                            <div className="form-group">
                                <input type="email" onChange={this.handleInput} className="form-control" name="email" placeholder={Translator.trans('email')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('email') &&
                                this.renderErrors(this.state.violations.email)
                            }
                        </section>
                        <section className={!this.cursorAt('password') && "hidden"}>
                            <p><strong>{Translator.trans('register_password_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('register_password_description')}</p>
                            <div className="form-group">
                                <input type="password" onChange={this.handleInput} className="form-control" name="password" placeholder={Translator.trans('password')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('password') &&
                                this.renderErrors(this.state.violations.password)
                            }
                            <div className="form-group">
                                <input type="password" onChange={this.handleInput} className="form-control" name="repeat_password" placeholder={Translator.trans('repeat_password')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('repeat_password') &&
                                this.renderErrors(this.state.violations.repeat_password)
                            }
                        </section>
                        <section className={!this.cursorAt('gender') && "hidden"}>
                            <p><strong>{Translator.trans('register_gender_title')}</strong></p>
                            <ul className="list-group">
                                <li className="list-group-item">
                                    <label className="custom-radio">
                                        <input type="radio" onChange={this.handleInput} name="gender" defaultValue='male'/>
                                        <span className="lbl">{Translator.trans('male')}</span>
                                    </label>
                                </li>
                                <li className="list-group-item">
                                    <label className="custom-radio">
                                        <input type="radio" onChange={this.handleInput}  name="gender" defaultValue='female'/>
                                        <span className="lbl">{Translator.trans('female')}</span>
                                    </label>
                                </li>
                            </ul>
                            {
                                this.state.violations.hasOwnProperty('gender') &&
                                this.renderErrors(this.state.violations.gender)
                            }
                            <input type="submit" id="submit-form" className="hide"/>
                        </section>
                        <section className={!this.cursorAt('activation') && "hidden"}>
                            <p><strong>{Translator.trans('register_activate_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('register_activate_description')}  <a href="#" onClick={this.resend}><strong>{Translator.trans('resend_authorization_code')}</strong></a></p>
                            <div className="form-group">
                                <input type="numeric" className="form-control" name="token"  placeholder={Translator.trans('token')}/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('token') &&
                                this.renderErrors(this.state.violations.token)
                            }
                            <input type="submit" id="submit-form" className="hide"/>
                        </section>
                    </form>
                </section>
            </article>
        );
    },

    renderErrors: function (errors) {
        return (
            <div className="alert alert-danger">
                {errors.map((error, index) => {return (<span key={index}>{Translator.trans(error)} </span>)})}
            </div>
        );
    },

    handleInput: function (e) {
        let name = e.target.name;
        let value = e.target.value;

        if(this.inputs.hasOwnProperty(name)) {
            this.inputs[name] = value;
        }
    },

    cursorAt: function (section) {
        return this.state.cursor === this.state.sections.indexOf(section);
    },

    next: function (e) {
        e.preventDefault();
        let valid = this.validateCurrentSection();

        if (!valid) {
            Utils.infobox('danger', 'alert.required_fields.title', 'alert.required_fields.message')
            return;
        }

        this.stepForward();
    },

    resend: function (e) {
        e.preventDefault();
        let formData = new FormData(e.target.parentNode.parentNode.parentNode.parentNode);
        this.startLoading();

        XHR.post(Routing.generate('send_activation_code'), formData, function (xhr) {
            switch (xhr.status) {
                case 200:
                    Utils.infobox('success', 'Success', 'Code sent.');
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    this.state.violations = errors;
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.finishLoading();
        }.bind(this));
    },

    stepForward: function () {
        if (this.state.cursor < this.state.sections.length) {
            this.state.cursor++;
            this.setState(this.state);
        }
    },

    validateCurrentSection: function () {
        let fields = this.state.sectionFieldsToValidate[this.state.sections[this.state.cursor]];

        for (let i = 0; i < fields.length; i++) {
            let value = this.inputs[fields[i]];
            if (!value || value.trim().length === 0) {
                return false;
            }
        }

        return true;
    },

    exit: function (e) {
        e.preventDefault();

        let eventId = Utils.confirmbox('confirm.unsaved_changes.title', 'confirm.unsaved_changes.message');
        document.addEventListener(eventId, this.cancelRegistration);
    },

    cancelRegistration: function(event) {
        if (event.detail.confirmed) {
            this.closeModal();
        }

        document.removeEventListener(event.type, this.cancelRegistration);
    },

    closeModal: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    submit: function (e) {
        e.preventDefault();
        let formData = new FormData(e.target);

        return this.state.registered ? this.activateAccount(formData) : this.registerAccount(formData);
    },

    activateAccount: function (formData) {
        this.startLoading();

        XHR.post(Routing.generate('activate_account'), formData, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.closeModal();
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    this.state.violations = errors;
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message')
            }
            this.finishLoading();
        }.bind(this));
    },

    registerAccount: function (formData) {
        this.startLoading();

        XHR.post(Routing.generate('register'), formData, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.state.registered = true;
                    this.stepForward();
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    if (errors) {
                        this.handleErrors(errors);
                    }
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message')
            }
            this.finishLoading();
        }.bind(this));
    },

    handleErrors: function (errors) {
        Utils.infobox('danger', 'alert.form_errors.title', 'alert.form_errors.message');
        let fields = Object.keys(errors);
        let index = this.state.sections.length;

        for (let i = 0; i < fields.length; i++) {
            let sectionIndex = this.state.sections.indexOf(this.state.fieldsToSectionsMap[fields[i]]);

            if (sectionIndex < index) {
                index = sectionIndex;
            }
        }

        this.state.cursor = index;
        this.state.violations = errors;
        this.setState(this.state);
    },

    isLoading: function () {
        return this.state.loading;
    },

    startLoading: function () {
        this.state.loading = true;
        this.setState(this.state);
    },

    finishLoading: function () {
        this.state.loading = false;
        this.setState(this.state);
    }
});

var DatePicker = React.createClass({
    minYear: 1918,
    months: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],

    getInitialState: function () {
        let state = {
            year: new Date().getFullYear(),
            month: new Date().getMonth(),
            day: new Date().getDate()
        };

        try {
            let match = this.props.date.match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/gm);

            if (null === match) {
                throw new Error('Invalid date format. Use YYYY-MM-DD');
            }

            let parts = this.props.date.split("-");
            let year = parts[0];
            let month = Math.abs(parts[1]);
            let day = Math.abs(parts[2]);

            if (month > 12) {
                throw new Error('Invalid date. Use YYYY-MM-DD');
            }

            if (day > new Date(year, month+1, 0).getDate()) {
                throw new Error('Invalid date. Use YYYY-MM-DD');
            }

            state = {
                year: year,
                month: month - 1,
                day: day
            };
        } catch (error) {
            //just use today's date
        }

        return state;
    },

    render: function () {
        return(
            <div className="form-group">
                <input type="hidden" name={this.props.name} value={this.dateFormat()}/>
                <div className="btn-group btn-group-justified" role="group">
                    <a href="#" onClick={this.selectYear} className="btn btn-default">{this.state.year}</a>
                    <a href="#" onClick={this.selectMonth} className="btn btn-default">{this.monthName(this.state.month)}</a>
                    <a href="#" onClick={this.selectDay} className="btn btn-default">{this.state.day}</a>
                </div>
            </div>
        );
    },

    dateFormat: function () {
        let year = this.state.year;
        let month = this.state.month + 1 <= 9 ? "0" + (this.state.month+1) : this.state.month+1;
        let day = this.state.day <= 9 ? "0" + this.state.day : this.state.day;

        return year + "-" + month + "-" + day;
    },

    monthName: function (month) {
        if (typeof this.months[month] === 'undefined') {
            throw new Error('invalid month index');
        }

        return Translator.trans(this.months[month]);
    },

    selectYear: function (e) {
        e.preventDefault();

        let options = [];

        for (let i = new Date().getFullYear(); i >= this.minYear; i--) {
            options.push(new MenuBoxEntry(i, '', function () {
                this.changeYear(i);
            }.bind(this)))
        }

        Utils.menubox(options);
    },

    selectMonth: function (e) {
        e.preventDefault();
        
        let options = [];

        for (let i = 0; i < this.months.length; i++) {
            options.push(new MenuBoxEntry(this.months[i], '', function () {
                this.changeMonth(i);
            }.bind(this)))
        }

        Utils.menubox(options);
    },

    selectDay: function (e) {
        e.preventDefault();

        let options = [];

        for (let i = 1; i <= new Date(this.state.year, this.state.month +1, 0).getDate(); i++) {
            options.push(new MenuBoxEntry(i, '', function () {
                this.changeDay(i);
            }.bind(this)))
        }

        Utils.menubox(options);
    },

    changeYear: function (year) {
        this.state.year = year;
        this.setState(this.state);
        this.correctDay();
    },

    changeMonth: function (month) {
        this.state.month = month;
        this.setState(this.state);
        this.correctDay();
    },

    changeDay: function (day) {
        let maxDay = new Date(this.state.year, this.state.month+1, 0). getDate();
        this.state.day = day <= maxDay ? day : maxDay;
        this.setState(this.state);
    },
    
    correctDay: function () {
        let maxDay = new Date(this.state.year, this.state.month+1, 0). getDate();

        if (this.state.day > maxDay) {
            this.state.day = maxDay;
            this.setState(this.state);
        }
    }
});