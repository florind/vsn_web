var ClubView = React.createClass({
    getInitialState: function () {
        let club = JSON.parse(this.props.club);
        return {
            club: club,
            isMember: club.permissions.indexOf('UNFOLLOW') !== -1,
            navSelection: 'topics',
        }
    },

    render: function() {
        let section = '';

        switch (this.state.navSelection) {
            case 'topics':
                section = <TopicList club={this.state.club.hash} isMember={this.state.isMember} csrf={this.props.csrf}/>
                break;
            case 'members':
                section = <MemberList clubId={this.state.club.hash} />
        }

        return(
            <section>
                <ClubHeader club={this.state.club} isMember={this.state.isMember} navSel={this.state.navSelection} navigate={this.navigate} join={this.join} leave={this.confirmLeave}/>
                {section}
            </section>
        );
    },

    navigate: function(selection) {
        this.state.navSelection = selection;
        this.setState(this.state);
    },

    join: function() {
        XHR.post(Routing.generate('club/join', {club: this.state.club.hash}), null, function (xhr) {
            if (xhr.status === 200) {
                this.state.isMember = true;
                this.setState(this.state);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }
        }.bind(this));
    },

    confirmLeave: function() {
        let eventId = Utils.confirmbox('confirm.leave_club.title', 'confirm.leave_club.message');
        document.addEventListener(eventId, this.leave);
    },

    leave: function (event) {
        if (event.detail.confirmed) {
            XHR.post(Routing.generate('club/leave', {club: this.state.club.hash}), null, function (xhr) {
                if (xhr.status === 200) {
                    this.state.isMember = false;
                    this.setState(this.state);
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this));
        }
        document.removeEventListener(event.type, this.leave);
    }
});

var ClubHeader = React.createClass({
    getInitialState: function () {
        return {isJoining: false}
    },

    render: function() {
        return(
            <article className="posting club">
                <section className="header">
                    <section className="logo">
                        <img src={Utils.imgUrl(this.props.club.logo)}/>
                    </section>
                    {!this.props.isMember &&
                        <article className="posting">
                            {this.joinButton()}
                        </article>
                    }
                </section>
                <section className="body">
                    <a href="#" className="club-name" onClick={this.renderClubMenu}>{this.props.club.name} <i className="fa fa-fw fa-angle-down"></i></a>
                </section>
                <section className="footer">
                    <ClubViewNavigation join={this.props.join} leave={this.props.leave} navigate={this.props.navigate} selection={this.props.navSel} isMember={this.props.club.isMember}/>
                </section>
            </article>
        );
    },

    join: function (e) {
        e.preventDefault();
        this.state.isJoining = true;
        this.setState(this.state);
        this.props.join();
    },

    joinButton: function () {
        if (this.state.isJoining) {
            return(<button disabled className="btn btn-success btn-block"><i className="fa fa-spinner fa-pulse fa-fw"></i></button>);
        } else {
            return(<button onClick={this.join} className="btn btn-success btn-block">{Translator.trans('join_club')}</button>);
        }
    },

    renderClubMenu: function () {
        let menu = [];

        menu.push(new MenuBoxEntry(Translator.trans('club_menu.menu_box.club_details'), 'fa fa-fw fa-info', this.details));

        if (this.props.isMember) {
            menu.push(new MenuBoxEntry(Translator.trans('club_menu.menu_box.leave_club'), 'fa fa-fw fa-sign-out', this.leave))
        }

        Utils.menubox(menu);
    },

    details: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(ClubInfo, {club: this.props.club.hash}, null));
    },

    leave: function (e) {
        e.preventDefault();
        this.props.leave();
    }
});

var ClubViewNavigation = React.createClass({
    render: function() {
        let classes = 'btn btn-default';

        return(<div className="btn-group btn-group-justified">
                <div className="btn-group" role="group">
                    <button type="button" data-nav='topics' onClick={this.navigate} className={this.props.selection === "topics" ? classes+' active' : classes}>
                        {Translator.trans('club_menu.topics')}
                    </button>
                </div>
                <div className="btn-group" role="group">
                    <button type="button" data-nav='members' onClick={this.navigate} className={this.props.selection === "members" ? classes+' active' : classes}>
                        {Translator.trans('club_menu.members')}
                    </button>
                </div>
            </div>
        );
    },

    navigate: function(e) {
        e.preventDefault();
        let nav = e.currentTarget.getAttribute('data-nav');

        if (nav) {
            this.props.navigate(nav);
        }

        return false;
    },
});
