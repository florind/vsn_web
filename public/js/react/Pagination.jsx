var Pagination = React.createClass({
    render: function () {
        return(
            <article className="posting pagination">
                <section className="header">
                    <nav>
                        <a href="#" onClick={this.previousPage} className={this.props.page === 1 ? "disabled" : ""}>
                            <i className="fa fa-fw fa-angle-double-left"></i>
                            {Translator.trans('prev')}
                        </a>
                        <a href="#" onClick={this.pageSelector} className="page">
                            {Translator.trans('page_of', {page: this.props.page, total: Math.ceil(this.props.total / this.props.results)})}
                        </a>
                        <a href="#" onClick={this.nextPage} className={this.props.page === Math.ceil(this.props.total / this.props.results) ? "disabled" : ""}>
                            {Translator.trans('next')}
                            <i className="fa fa-fw fa-angle-double-right"></i>
                        </a>
                    </nav>
                </section>
            </article>
        );
    },

    previousPage: function (e) {
        e.preventDefault();
        if (this.props.page === 1 || this.props.isLoading) {
            return;
        }

        this.loadPage(this.props.page - 1);
    },

    nextPage: function (e) {
        e.preventDefault();
        if (this.props.page === Math.ceil(this.props.total / this.props.results) || this.props.isLoading) {
            return;
        }

        this.loadPage(this.props.page + 1);
    },

    pageSelector: function (e) {
        e.preventDefault();
        if (this.props.isLoading) {
            return;
        }

        let menuItems = [];

        for (let i = 0; i < Math.ceil(this.props.total / this.props.results); i++) {
            menuItems.push(new MenuBoxEntry(Translator.trans('page', {page: i+1}), '', this.loadPage.bind(this, i+1)));
        }

        Utils.menubox(menuItems);
    },

    loadPage: function (page) {
        if (this.props.isLoading) {
            return;
        }

        if (page === this.props.page) {
            return;
        }
        this.props.loadPage(page);
    }
});
