var TopicList = React.createClass({
    getInitialState: function () {
        return {
            page: 1,
            perPage: 15,
            total: 15,
            isFetching: false,
            noTopics: false,
            topics: []
        };
    },

    componentDidMount: function() {
        this.fetchTopics(this.state.page, this.state.perPage);
    },

    render: function() {
        return(
            <div className="topic-list">
                {
                    this.props.isMember &&
                    <article className="posting">
                            <button type="text" onClick={this.createTopic} className="btn btn-success btn-block">
                                + {Translator.trans('create_new_topic')}
                            </button>
                    </article>
                }
                {
                    this.state.total > this.state.perPage &&
                    this.state.topics.length > 0 &&
                    <Pagination isLoading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
                {
                    this.state.noTopics &&
                    <p className="noresults">{Translator.trans('no_topics')}</p>
                }
                {
                    this.state.isFetching &&
                    <Loader/>
                }
                {
                    !this.state.isFetching &&
                    this.state.topics.map((topic, index) => {
                        return <TopicSummary club={this.props.club} key={index} topic={topic} topicCsrf={this.props.csrf.topic} commentCsrf={this.props.csrf.comment}/>
                    })
                }
                {
                    this.state.total > this.state.perPage &&
                    this.state.topics.length > 0 &&
                    <Pagination isloading={this.state.isFetching} loadPage={this.loadPage} page={this.state.page} results={this.state.perPage} total={this.state.total} />
                }
            </div>
        );
    },

    createTopic: function (e) {
        e.preventDefault();

        Utils.modal(React.createElement(
            TopicForm,
            {club: this.props.club, topic: this.props.topic, csrf: this.props.csrf.topic},
            null
        ));
    },

    loadPage: function(page) {
        this.state.page = page;
        this.fetchTopics(this.state.page, this.state.perPage);
    },

    fetchTopics: function(page, perPage) {
        let url = Routing.generate('topics/list', {
                hash: this.props.club,
                page: page,
                limit: perPage
            });

        this.setFetching(true);

        XHR.get(url, function (xhr) {
            if (xhr.status === 200) {
                let response = JSON.parse(xhr.responseText);

                if (response.topics.length > 0) {
                    this.state.total = response.count;
                    this.state.topics = response.topics;
                }

                this.state.noTopics = response.topics.length === 0;
            }

            this.setFetching(false);
        }.bind(this));
    },

    setFetching: function (status) {
        this.state.isFetching = status;
        this.setState(this.state);
    },
});

var TopicSummary = React.createClass({
    getInitialState: function () {
        return {
            isLoading: false,
            contextMenuFrozen: false,
            topic: this.props.topic
        }
    },

    componentDidMount: function() {
        document.addEventListener(Utils.event.TOPIC_UPDATED, this.refresh);
    },

    componentWillUnmount: function () {
        document.removeEventListener(Utils.event.TOPIC_UPDATED, this.refresh);
    },

    render: function () {
        return(
            <article className="posting" key={this.state.topic.hash}>
                <header>
                    <section className="profile" onClick={this.profile}>
                        <section className="avatar">
                            <img src={Utils.imgUrl(this.state.topic.author.avatar)} alt=""/>
                        </section>
                        <section className="details">
                            <a href="#">{this.state.topic.author.first_name+" "+this.props.topic.author.last_name}</a>
                            <span>{moment(this.state.topic.timestamp).fromNow()}</span>
                        </section>
                    </section>
                    {
                        !this.state.contextMenuFrozen &&
                        <section className="options" onClick={this.contextMenu}>
                            <a href="#"><i className="fa fa-fw fa-ellipsis-v"></i></a>
                        </section>
                    }
                    {
                        this.state.contextMenuFrozen &&
                        <section className="options">
                            <i className="fa fa-fw fa-spinner fa-pulse"></i>
                        </section>
                    }
                </header>
                <section className="body" onClick={this.view}>
                    <section>
                        {
                            this.state.topic.locked &&
                            <span className="badge">
                                <i className="fa fa-lock"></i> {Translator.trans('topic_locked')}
                            </span>
                        }
                    </section>
                    <h4>{this.state.topic.title}</h4>
                    <p>{Utils.excerpt(this.state.topic.text)}</p>
                </section>
                {
                    this.state.topic.media.length > 0 &&
                    <ThumbnailGallery media={this.state.topic.media} />
                }
                <footer>
                    <section className="stats">
                        <div className="text-muted">
                            {
                                this.state.topic.likes > 0 &&
                                <a href="#" onClick={this.listLikes}>{Translator.transChoice('likes', this.state.topic.likes, {value: this.state.topic.likes})}</a>
                            }
                            {
                                this.state.topic.likes > 0 && this.state.topic.comments > 0 &&
                                <br/>
                            }
                            {
                                this.state.topic.comments > 0 &&
                                <span>{this.state.topic.comments} {Translator.trans('comments')}</span>
                            }
                        </div>
                        <div className="actions">
                            {
                                this.state.topic.permissions.indexOf('LIKE') !== -1 &&
                                <a href="#" onClick={this.like}><i className="fa fa-fw fa-heart-o"></i> {Translator.trans('like')}</a>
                            }
                            {
                                this.state.topic.permissions.indexOf('UNLIKE') !== -1 &&
                                <a href="#" onClick={this.unlike}><i className="fa fa-fw fa-heart"></i> {Translator.trans('like')}</a>
                            }
                            <a href="#" onClick={this.view}>
                                <i className="fa fa-fw fa-comments-o"></i> {Translator.trans('comment')}
                            </a>
                        </div>
                    </section>
                </footer>
            </article>
        );
    },

    listLikes: function (e) {
        e.preventDefault();
        Utils.modal(React.createElement(LikesList, {node: this.props.topic.hash}, null));
    },

    refresh: function () {
        XHR.get(Routing.generate('topic/reload', {
            club: this.props.club,
            topic: this.state.topic.hash
        }), function(xhr) {
            if (xhr.status === 200) {
                this.state.topic = JSON.parse(xhr.responseText);
                this.setState(this.state);
            }

            this.state.isLoading = false;
        }.bind(this));
    },

    profile: function() {
        window.location.href = Routing.generate('profile', {id: this.state.topic.author.hash});
    },

    like: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/like', {hash: this.state.topic.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    unlike: function (e) {
        e.preventDefault();
        if (this.state.isLoading) {
            return;
        }

        this.state.isLoading = true;
        XHR.post(
            Routing.generate('profile/unlike', {hash: this.state.topic.hash}),
            null,
            function(xhr) {
                if (200 === xhr.status) {
                    this.refresh();
                } else {
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
                }
            }.bind(this)
        );
    },

    view: function(e) {
        e.preventDefault();

        let viewUrl =  Routing.generate('topic/view', {topicHash: this.state.topic.hash, clubHash: this.props.club});

        if (window.location.pathname === viewUrl) {
            return;
        }

        window.location.href = viewUrl;
    },

    contextMenu: function (e) {
        e.preventDefault();

        let menuItems = [];

        if (this.state.topic.permissions.indexOf('UNLOCK') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('unlock_topic'), 'fa fa-fw fa-unlock', this.confirmUnlock));
        }

        if (this.state.topic.permissions.indexOf('UNFOLLOW') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('unfollow_topic'), 'fa fa-fw fa-eye-slash', this.unfollow));
        }

        if (this.state.topic.permissions.indexOf('FOLLOW') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('follow_topic'), 'fa fa-fw fa-eye', this.follow));
        }

        if (this.state.topic.permissions.indexOf('EDIT') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('edit'), 'fa fa-fw fa-pencil', this.edit));
        }

        if (this.state.topic.permissions.indexOf('DELETE') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('delete'), 'fa fa-fw fa-trash-o', this.confirmDelete));
        }

        if (this.state.topic.permissions.indexOf('LOCK') !== -1) {
            menuItems.push(new MenuBoxEntry(Translator.trans('lock_topic'), 'fa fa-fw fa-lock', this.confirmLock));
        }

        Utils.menubox(menuItems);
    },

    edit: function () {
        Utils.modal(React.createElement(
            TopicForm,
            {club: this.props.club, topic: this.state.topic, csrf: this.props.topicCsrf},
            null
        ));
    },

    confirmDelete: function () {
        console.log('implement topic delete');
    },

    follow: function () {
        let url = Routing.generate('topic/follow', {club: this.props.club, topic: this.state.topic.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.state.topic.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    unfollow: function () {
        let url = Routing.generate('topic/unfollow', {club: this.props.club, topic: this.state.topic.hash});

        this.freezeContextMenu();

        XHR.post(url, null, function (xhr) {
            if (xhr.status === 200) {
                this.state.topic.permissions = JSON.parse(xhr.responseText);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    confirmLock: function () {
        let eventId = Utils.confirmbox(
            Translator.trans('confirm.topic_lock.title'),
            Translator.trans('confirm.topic_lock.message')
        );

        document.addEventListener(eventId, this.lock);
    },

    confirmUnlock: function () {
        let eventId = Utils.confirmbox(
            Translator.trans('confirm.topic_unlock.title'),
            Translator.trans('confirm.topic_unlock.message')
        );
        document.addEventListener(eventId, this.unlock);
    },

    lock: function () {
        document.removeEventListener(event.type, this.lock);

        if (this.state.topic.locked) {
            return;
        }

        this.freezeContextMenu();

        XHR.post(Routing.generate('topic/lock', {club: this.props.club, topic: this.state.topic.hash}), null, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.state.topic.locked = true;
                    this.state.topic.permissions = JSON.parse(xhr.responseText);
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }
            this.unfreezeContextMenu();
        }.bind(this));
    },

    unlock: function () {
        document.removeEventListener(event.type, this.unlock);

        if (!this.state.topic.locked) {
            return;
        }

        this.freezeContextMenu();

        XHR.post(Routing.generate('topic/unlock', {club: this.props.club, topic: this.state.topic.hash}), null, function(xhr){
            switch (xhr.status) {
                case 200:
                    this.state.topic.locked = false;
                    this.state.topic.permissions = JSON.parse(xhr.responseText);
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.unfreezeContextMenu();
        }.bind(this));
    },

    freezeContextMenu: function () {
        this.state.contextMenuFrozen = true;
        this.setState(this.state);
    },

    unfreezeContextMenu: function () {
        this.state.contextMenuFrozen = false;
        this.setState(this.state);
    }
});