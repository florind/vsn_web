let FormBox = React.createClass({
    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
        document.addEventListener('formbox-dismiss', this.dismiss);
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
        document.removeEventListener('formbox-dismiss', this.dismiss);
    },

    render: function() {
        return(
            <div className="form-box">
                <div className="panel section-panel card-panel">
                    <div className="panel-heading">
                        <label htmlFor="cancel_submit">
                            <i className="fa fa-fw fa-angle-left"></i> {Translator.trans('back')}
                        </label>
                        <label htmlFor="submit_form">{Translator.trans('save')}</label>
                    </div>
                    <div className="panel-body">
                        {this.props.form}
                    </div>
                </div>
            </div>
        );
    },

    dismiss: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    }
});