var ClubMembershipsList = React.createClass({
    getInitialState: function () {
        return {clubs: []}
    },

    componentDidMount: function() {
        XHR.get(Routing.generate('profile/memberships', {hash: this.props.profile}), function (xhr) {
            if (xhr.status === 200) {
                let clubs = JSON.parse(xhr.responseText);
                this.setState({clubs: clubs});
            }
        }.bind(this));
    },

    render: function() {
        return(
            <section>
                <article className="posting">
                    <section className="body">
                        <button className="btn btn-success btn-block" onClick={this.createClub}>{Translator.trans('create_club')}</button>
                    </section>
                </article>
                {
                    this.state.clubs.map(club => {
                        return <Club key={club.hash} club={club}/>
                    })
                }
            </section>
        );
    },

    createClub: function () {
        Utils.modal(React.createElement(ClubForm, {}, null));
    }
});
