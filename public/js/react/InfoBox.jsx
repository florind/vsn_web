var InfoBox = React.createClass({
    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function() {
        let type = '';

        switch (this.props.type) {
            case 'success':
                type = 'bg-success';
                break;
            case 'danger':
                type = 'bg-danger';
                break;
            default:
                type = 'bg-info';
        }

        let bodyClass = "panel-body " + type;

        return(
            <div className="info-box">
                <div className="backdrop" onTouchMove={this.dismiss} onClickCapture={this.dismiss}></div>
                <div className="panel section-panel card-panel" onClickCapture={this.dismiss}>
                    <div className={bodyClass}>
                        <strong>{this.props.title}</strong>
                        <p>{this.props.message}</p>
                    </div>
                </div>
            </div>
        );
    },

    dismiss: function (e) {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    }
});