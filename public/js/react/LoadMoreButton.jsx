var LoadMoreButton = React.createClass({
    render: function() {
        if (this.props.isLoading) {
            return(<button href="#" disabled className="btn btn-default btn-block"><i className="fa fa-spinner fa-pulse"></i></button>);
        } else {
            return(<button href="#" onClick={this.load} className="btn btn-default btn-block">
                {Translator.trans('load_more')}
            </button>);
        }
    },

    load: function(e) {
        e.preventDefault();
        this.props.callback();
    }
});