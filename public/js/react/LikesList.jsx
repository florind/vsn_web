var LikesList = React.createClass({
    getInitialState: function () {
        return {
            isLoading: false,
            likes: []
        };
    },

    componentDidMount: function () {
        this.getLikes();
    },

    render: function () {
        return(
            <article className="posting no-border">
                <header>
                    <section>
                        <a href="#" onClick={this.closeModal}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>
                            {Translator.trans('like_list_title')}
                            {
                                !this.isLoading() && this.state.likes.length > 0 && " (" + this.state.likes.length + ")"
                            }
                        </span>
                    </section>
                </header>
                <section className="body nopadding">
                    {
                        this.isLoading() &&
                        <Loader/>
                    }
                    {
                        this.state.likes.map((profile, index) => {
                            return(
                                <article className="posting" key={index}>
                                    <header>
                                        <section className="profile" onClick={this.visitProfile.bind(this, profile.hash)}>
                                            <section className="avatar">
                                                <img src={Utils.imgUrl(profile.avatar)} alt=""/>
                                            </section>
                                            <section className="details">
                                                <a href="#">{profile.firstName+ " " + profile.lastName}</a>
                                                <span>{profile.locality}</span>
                                            </section>
                                        </section>
                                    </header>
                                </article>
                            );
                        })
                    }
                </section>
            </article>
        );
    },

    visitProfile: function(id) {
        window.location.href = Routing.generate('profile', {id: id});
    },

    getLikes: function () {
        this.startLoading();

        XHR.get(Routing.generate('nodes/likes', {hash: this.props.node}), function (xhr) {
            if (xhr.status === 200) {
                this.state.likes = JSON.parse(xhr.responseText);
                this.setState(this.state);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }
            this.finishLoading();
        }.bind(this));
    },

    isLoading: function () {
        return this.state.loading;
    },

    startLoading: function () {
        this.state.loading = true;
        this.setState(this.state);
    },

    finishLoading: function () {
        this.state.loading = false;
        this.setState(this.state);
    },

    closeModal: function (e) {
        e.preventDefault();
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },
});