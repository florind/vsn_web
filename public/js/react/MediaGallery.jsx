var MediaGallery = React.createClass({
    lastTouchTimestamp: 0,
    isZoomed: false,

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    getInitialState: function () {
        return{
            media: this.props.media,
            index: typeof this.props.current !== 'undefined' ? this.props.current : this.props.media[0].hash
        };
    },

    render: function () {
        let item = this.state.media[0];

        if (null !== this.state.index) {
            for (let i = 0; i < this.state.media.length; i++) {
                if (this.state.media[i].hash === this.state.index) {
                    item = this.state.media[i];
                    break;
                }
            }
        }

        return (
            <div className="media-gallery">
                <div className="backdrop"></div>
                <div className="media">
                    <img onClick={this.zoom} onLoad={this.zoomOut} src={Utils.imgUrl(item.filename)}/>
                </div>
                <span onClick={this.dismiss} className="dismiss"><i className="fa fa-fw fa-times"></i></span>
                <span onClick={this.previous} className="prev"><i className="fa fa-fw fa-angle-left"></i></span>
                <span onClick={this.next} className="next"><i className="fa fa-fw fa-angle-right"></i></span>
            </div>
        );
    },

    dismiss: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    zoom: function (event) {
        let current = (new Date()).getTime();

        if (this.lastTouchTimestamp !== 0) {
             if (current - this.lastTouchTimestamp < 300) {
                 if (this.isZoomed) {
                     this.zoomOut(event)
                 } else {
                     this.zoomIn(event);
                 }
             } else {
                 this.lastTouchTimestamp = 0;
             }
        } else {
            this.lastTouchTimestamp = current;
        }
    },

    zoomOut: function (event) {
        event.currentTarget.style.width = '100%';
        this.isZoomed = false;
        this.lastTouchTimestamp = 0;
    },

    zoomIn: function (event) {
        event.currentTarget.style.width = '200%';
        this.isZoomed = true;
        this.lastTouchTimestamp = 0;
    },

    previous: function () {
        let prevIndex = this.state.media[this.state.media.length - 1].hash;
        let length = this.state.media.length;
        let media = this.state.media;
        let index = this.state.index;

        for (let i = (length-1); i >= 0; i--) {
            if (media[i].hash === index) {
                if ((i - 1) >= 0) {
                    prevIndex = media[i-1].hash;
                    break;
                }
            }
        }

        this.state.index = prevIndex;
        this.setState(this.state);
    },

    next: function () {
        let nextIndex = this.state.media[0].hash;
        let length = this.state.media.length;
        let media = this.state.media;
        let index = this.state.index;

        for (let i = 0; i < length; i++) {
            if (media[i].hash === index) {
                if (i < (length - 1)) {
                    nextIndex = media[i+1].hash;
                    break;
                }
            }
        }

        this.state.index = nextIndex;
        this.setState(this.state);
    }
});
