var PasswordReset = React.createClass({
    getInitialState: function () {
        return {
            initiated: false,
            loading: false,
            violations: {}
        };
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function () {
        return(
            <article className="posting no-border">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('reset_password_title')}</span>
                    </section>
                    {
                        this.isLoading() &&
                        <label><i className="fa fa fw fa-spinner fa-pulse"></i></label>
                    }
                    {
                        !this.isLoading() && this.state.initiated &&
                        <label htmlFor="submit-form" >{Translator.trans('save')}</label>
                    }
                    {
                        !this.isLoading() && !this.state.initiated &&
                        <label htmlFor="submit-form" >{Translator.trans('next')}</label>
                    }
                </header>
                <section className="body text-center">
                    <form onSubmit={this.submit} noValidate>
                        <section className={this.state.initiated ? "hidden" : ""}>
                            <p><strong>{Translator.trans('reset_password_email_title')}</strong></p>
                            <p className="text-muted">{Translator.trans('reset_password_email_description')}</p>
                            <div className="form-group">
                                <input type="email" className="form-control" placeholder={Translator.trans('email')} name="email"/>
                            </div>
                            {
                                this.state.violations.hasOwnProperty('email') &&
                                this.renderErrors(this.state.violations.email)
                            }
                        </section>
                        {
                            this.state.initiated &&
                            <section>
                                <p><strong>{Translator.trans('reset_password_title')}</strong></p>
                                <p className="text-muted">
                                    {Translator.trans('password_reset_instructions')} <a href="#" onClick={this.resend}><strong>{Translator.trans('resend_authorization_code')}</strong></a>
                                </p>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder={Translator.trans('authorization_code')} name="token"/>
                                </div>
                                {
                                    this.state.violations.hasOwnProperty('token') &&
                                    this.renderErrors(this.state.violations.token)
                                }
                                <div className="form-group">
                                    <input type="password" className="form-control" placeholder={Translator.trans('new_password')} name="password"/>
                                </div>
                                {
                                    this.state.violations.hasOwnProperty('password') &&
                                    this.renderErrors(this.state.violations.password)
                                }
                                <div className="form-group">
                                    <input type="password" className="form-control" placeholder={Translator.trans('repeat_password')} name="repeat_password"/>
                                </div>
                                {
                                    this.state.violations.hasOwnProperty('repeat_password') &&
                                    this.renderErrors(this.state.violations.repeat_password)
                                }
                            </section>
                        }
                        <input type="submit" id="submit-form" className="hidden"/>
                    </form>
                </section>
            </article>
        );
    },

    submit: function (e) {
        e.preventDefault();
        if (this.state.loading) { return }
        let formData = new FormData(e.target);
        this.state.initiated ? this.save(formData) : this.initiate(formData);
    },

    resend: function (e) {
        e.preventDefault();
        let formData = new FormData(e.target.parentNode.parentNode.parentNode.parentNode);
        this.initiate(formData);
    },

    initiate: function (formData) {
        this.startLoading();

        XHR.post(Routing.generate('init_password_reset'), formData, function (xhr) {
            switch (xhr.status) {
                case 200:
                    this.state.initiated = true;
                    this.setState(this.state);
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    this.state.violations = errors;
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.finishLoading();
        }.bind(this));
    },

    save: function (formData) {
        this.startLoading();

        XHR.post(Routing.generate('finish_password_reset'), formData, function (xhr) {
            switch (xhr.status) {
                case 200:
                    window.location.href = Routing.generate('login');
                    break;
                case 400:
                    let errors = JSON.parse(xhr.responseText);
                    this.state.violations = errors;
                    this.setState(this.state);
                    break;
                default:
                    Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }

            this.finishLoading();
        }.bind(this));
    },

    exit: function (e) {
        e.preventDefault();
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    renderErrors: function (errors) {
        return (
            <div className="alert alert-danger">
                {errors.map((error, index) => {return (<span key={index}>{Translator.trans(error)} </span>)})}
            </div>
        );
    },

    isLoading: function () {
        return this.state.loading;
    },

    startLoading: function () {
        this.state.loading = true;
        this.setState(this.state);
    },

    finishLoading: function () {
        this.state.loading = false;
        this.setState(this.state);
    }
});