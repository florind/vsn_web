var SecondaryPage = React.createClass({
    render: function () {
        return(
            <div className="panel section-panel card-panel secondary-page">
                <div className="panel-heading bg-primary" onClick={this.navigateBack}>
                    <i className="fa fa-fw fa-angle-left"></i> go back
                </div>
                <div className="panel-body">
                    {this.props.content}
                </div>
            </div>
        );
    },

    navigateBack: function() {
        window.history.back();
    }
});