let ContentBox = React.createClass({
    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function() {
        return(
            <article className="posting content-box">
                <header>
                    <a href="#" onClick={this.dismiss}><i className="fa fa-fw fa-angle-left"></i></a>
                    {
                        this.props.menu.map((item, index) => {
                            return(<a href="" key={index} onClick={(e) => {e.preventDefault(); this.handleMenuClick(index);}}>{item.name} <i className={item.icon}></i></a>);
                        })
                    }
                </header>
                <section className="body">
                    {this.props.content}
                </section>
            </article>
        );
    },

    dismiss: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    },

    handleMenuClick: function (index) {
        this.props.menu[index].event(options);
    },

    options: function () {
        return {
            dismiss: function () {
                this.dismiss();
            }.bind(this)
        };
    }
});