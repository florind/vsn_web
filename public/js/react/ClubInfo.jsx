var ClubInfo = React.createClass({
    getInitialState: function () {
        return {club: null};
    },

    componentDidMount: function () {
        document.body.style.overflow = 'hidden';
        XHR.get(Routing.generate('club/info', {club: this.props.club}), function(xhr){
            if (200 === xhr.status) {
                this.state.club = JSON.parse(xhr.responseText);
                this.setState(this.state);
            } else {
                Utils.infobox('danger', 'alert.request_failed.title', 'alert.request_failed.message');
            }
        }.bind(this));
    },

    componentWillUnmount: function () {
        document.body.style.overflow = 'visible';
    },

    render: function() {
        return(
            <article className="posting no-border">
                <header>
                    <section>
                        <a href="#" onClick={this.exit}><i className="fa fa-fw fa-angle-left"></i></a>
                        <span>{Translator.trans('club_info_title')}</span>
                    </section>
                </header>
                <section className="body">
                    {
                        null === this.state.club &&
                        <Loader/>
                    }
                    {
                        null !== this.state.club &&
                        <ul className="list-group">
                            <li className="list-group-item logo"><img src={Utils.imgUrl(this.state.club.logo)}/></li>
                            <li className="list-group-item flex center">{this.state.club.description}   </li>
                            <li className="list-group-item flex space-between">
                                <span>{Translator.trans('club_info.label.name')}</span>
                                <span>{this.state.club.name}</span>
                            </li>
                            <li className="list-group-item flex space-between">
                                <span>{Translator.trans('club_info.label.created')}</span>
                                <span>{moment(this.state.club.created).format('D MMMM YYYY')}</span>
                            </li>
                            <li className="list-group-item flex space-between">
                                <span>{Translator.trans('club_info.label.founder')}</span>
                                <span>
                                    <a href={this.generateProfileUrl(this.state.club.founder.hash)}>
                                        {this.state.club.founder.first_name + " " +this.state.club.founder.last_name}
                                    </a>
                                </span>
                            </li>
                            <li className="list-group-item flex space-between">
                                <span>{Translator.trans('club_info.label.members')}</span>
                                <span>{this.state.club.memberCount}</span>
                            </li>
                            <li className="list-group-item flex space-between">
                                <span>{Translator.trans('club_info.label.topics')}</span>
                                <span>{this.state.club.topicCount}</span>
                            </li>
                        </ul>
                    }
                </section>
            </article>
        );
    },

    generateProfileUrl: function(hash) {
        return Routing.generate('profile', {id: hash});
    },

    exit: function () {
        ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
    }
});