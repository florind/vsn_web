$(document).ready(function(){

    // Follow
    var Modal = new AppModal('#app-modal-wrapper');
    var followToggle = '.follow-toggle';

    $(document).on('change', followToggle, function(event){
        $this = $(this);
        var url = $this.is(':checked') ?
            $this.attr('data-follow') :
            $this.attr('data-unfollow');

            $.ajax({
                url: url,
                type: "POST",
                beforeSend: function() {
                    $this.prop('disabled', true);
                },
                complete: function () {
                    setTimeout(function(){
                        $this.prop('disabled', false);
                    }, 1000);
                }
            });
    });

    // LIKE
    var socialLike = {
        button : 'a.like',
        likeAttr: 'data-like',
        hash: 'data-hash',
        unlikeAttr: 'data-unlike',
        likedAttr: 'data-isliked',
        likedIconClass: 'fa-thumbs-up',
        unlikedIconClass: 'fa-thumbs-o-up',
        likeList: ''
    };

    $(document).on('click', socialLike.button, function(event){
        event.preventDefault();
        $this = $(this);
        $icon = $this.find('i.fa');
        var isLiked = $this.attr(socialLike.likedAttr);

        var url = isLiked === "1" ?
            $this.attr(socialLike.unlikeAttr) :
            $this.attr(socialLike.likeAttr);

        $.ajax({
            url: url,
            type: "POST",
            beforeSend: function() {
                $this.prop('disabled', true);
            },
            success: function(response) {
                LikeToggleEvent.launch($this.attr(socialLike.hash), response.likes, $this);
            },
            complete: function () {
                $this.prop('disabled', false);
            }
        });
    });

    // View likes and comments
    var socialStats = {
        modalTitleText: 'data-modal-title',
        trigger: '.social-stats-trigger',
        listCommentLikesTrigger: '.list-comment-likes-trigger',
        hash : 'data-hash',
        comment : {
            section: '.comments-list-view',
            likesListSection: '.comment-likes-section',
            likesListView: '.comment-likes-section .list',
            likesListBack: '.comment-likes-section a.back',
            input: '.comment-input',
            button: '.comment-post',
            url: 'data-comment-url',
            wrapper: '.post-comment-section'
        },
        reply: {
            button: '.comment-reply-trigger',
            replyTo: 'data-reply-to',
            section: '.comment-reply-section',
            originalComment: '.original-comment'
        }
    };

    $(document).on('click', socialStats.trigger, function(event){
        event.preventDefault();
        $this = $(this);

        Modal.startLoading($this.attr(socialStats.modalTitleText), 'fa fa-line-chart');
        listSocialStats($this.attr(socialStats.hash), function(html) {
            Modal.finishLoading(html);
        });

    });

    $(document).on('click', socialStats.listCommentLikesTrigger, function(event){
        event.preventDefault();
        $this = $(this);

        Modal.startLoading();

        listSocialStats($this.attr(socialStats.hash), function(html) {
            $(socialStats.comment.likesListView).html(html);
            $(socialStats.comment.likesListSection).removeClass('hide');
            $(socialStats.comment.section).remove();
            $(socialStats.comment.wrapper).remove();
            Modal.finishLoading();
        });
    });

    $(document).on('click', socialStats.comment.button, function(event){
        event.preventDefault();
        $this = $(this);
        var text = $(socialStats.comment.input).val();

        if (text.length > 0) {
            Modal.startLoading();

            var data = {'comment': text};

            if ($this.attr(socialStats.reply.replyTo).length > 1) {
                data['reply_to'] = $this.attr(socialStats.reply.replyTo);
            }

            $.post($this.attr(socialStats.comment.url), data, function() {
                listSocialStats($this.attr(socialStats.hash), function(html) {
                    AddCommentEvent.launch($this.attr('data-id'), $this);
                    Modal.finishLoading(html);
                });
            });
        }
    });

    $(document).on('click', socialStats.reply.button, function(event){
        event.preventDefault();
        $this = $(this);

        var id = $this.attr(socialStats.reply.replyTo);
        var comment = $('#comment-'+id).clone();

        $(socialStats.reply.originalComment).html(comment);

        Modal.startLoading($this.attr(socialStats.modalTitleText), 'fa fa-comment-o');
        $(socialStats.reply.section).removeClass('hide');
        $(socialStats.comment.section).remove();
        $(socialStats.comment.button).attr(socialStats.reply.replyTo, id);
        Modal.finishLoading();

    });

    var listSocialStats = function(url, success) {
        $.get(url, function (data) {
            success(data);
        });
    };

    $(document).on(LikeToggleEvent.type, function(event){
        //toggle like icon
        var likeIcon = event.initiator.find('i.fa');

        if (event.initiator.attr(socialLike.likedAttr) === "1") {
            likeIcon.removeClass(socialLike.likedIconClass).addClass(socialLike.unlikedIconClass);
            event.initiator.attr(socialLike.likedAttr, "0");
        } else {
            likeIcon.removeClass(socialLike.unlikedIconClass).addClass(socialLike.likedIconClass);
            event.initiator.attr(socialLike.likedAttr, "1");
        }

        //update like counters
        var elements = $('.listener_like_toggle[data-hash="'+event.hash+'"]');

        elements.each(function () {
            $(this).html(event.likeCount);
            if (event.likeCount === 0) {
                $(this).closest('.like_counter_wrapper').addClass('hide');
            } else {
                $(this).closest('.like_counter_wrapper').removeClass('hide');
            }
        });
    });

    $(document).on(AddCommentEvent.type, function(event){
        var elements = $('.listener_comment_add[data-hash="'+event.hash+'"]');

        elements.each(function () {
            var currentCount = parseInt($(this).text());
            var newCount = isNaN(currentCount) ? 1 : ++currentCount;
            $(this).text(newCount);
            $(this).closest('.comment_counter_wrapper').removeClass('hide');
        });
    });

    //photo album trigger
    $('.photo_album_trigger').SmartPhoto({
        resizeStyle: 'fit',
        nav: false
    });

    $(document).on('click', '.photo-gallery .item', function(event){
        event.preventDefault();

        var photoSrc = $(this).find('img.photo').attr('src');
        $('.photo-gallery .cover').attr('src', photoSrc);
    });

    $(document).on('click', '._action-block', function(event){
        event.preventDefault();
        var url = $(this).attr("href");

        $.ajax({
            url: url,
            type: "GET",
            success: function(response) {
                console.log(response);
            }
        });
    });

    $(document).on('click', '._action-create-post', function(event){
        event.preventDefault();
        var title = $(this).attr('data-modal-title');
        var icon = $(this).attr('data-modal-icon');
        var url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'POST',
            beforeSend: function() {
                Modal.startLoading(title, icon);
            },
            success: function(response) {
                Modal.finishLoading(response.html)
                $('textarea').autogrow();
            }
        });
    });

    $(document).on('click', '._action-delete-post', function(event){
        event.preventDefault();
        var url = $(this).attr('href');
        var id = $(this).attr('data-id');

        var uid = Utils.uniqueId();

        $(document).off(uid).on(uid, function(event){
            if (event.confirmed) {
                if (typeof event.payload.url === 'undefined') {
                    console.error('missing url');
                }

                $.ajax({
                    url: event.payload.url,
                    type: 'DELETE',
                    success: function(response) {
                        new AlertSuccess('Success', 'Post was deleted.');
                        $('.row[data-id="'+ event.payload.id +'"]').hide('fast', function(){
                            $(this).remove();
                        });
                    },
                    error: function () {
                        new AlertError('Error', 'Post could not be deleted.');
                    }
                });
            }
        });

        new Confirm({
            id: uid,
            title: 'Delete Post',
            icon: 'fa fa-trash',
            message: 'Are you sure?',
            payload: {url: url, id: id}
        });
    });

    $(document).on('submit', 'form[name=post]', function(event){
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        var url = form.attr('action');

        formData.append('post[uploadedMedia]', form.multiImageUploader('getList'));

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                Modal.startLoading();
            },
            success: function(response) {
                if (typeof response.saved.hash !== 'undefined') {
                    Modal.close();
                } else {
                    $('span.help-block').remove();

                    for (var name in response.errors) {
                        if (response.errors.hasOwnProperty(name)) {
                            form.find("[name='post["+name+"]']").parent('.form-group')
                                .addClass('has-error').append(
                                $(document.createElement('span')).addClass('help-block')
                                    .text(response.errors[name].join(' '))
                            );
                        }
                    }
                    Modal.finishLoading();
                }
            }
        });
    });
});
