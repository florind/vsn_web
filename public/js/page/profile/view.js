"use strict";
$(document).ready(function(){
    var profileNav = {
        tab : '#profile-nav a[role="tab"]',
        panes : '#profile-nav .tab-content'
    };

    var loader = $('.loader');

    $(document).on('click', profileNav.tab, function (event) {
        var id = $(this).attr('href');
        var pane = $(profileNav.panes + ' ' + id);
        var url = $globals.profileNav[id];

        if (typeof url !== 'undefined') {
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function() {
                    loader.removeClass('hide');
                },
                success: function (response) {
                    pane.html(response);
                    pane.find('.photo_album_trigger').SmartPhoto({
                        resizeStyle: 'fit',
                        nav: false
                    });
                },
                complete: function (result) {
                    // profileDetails.modal.id.modal('hide');
                    loader.addClass('hide');
                }
            });
        }
    });
});
