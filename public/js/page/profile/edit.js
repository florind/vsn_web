"use strict";
$(document).ready(function(){
    var photoForm = {
        id: $('#profile-photo-form'),
        field: {
            url: $('#data_url_photo_upload_url')
        },
        updateAvatar: function(img) {
            photoForm.updateImage(img, function(imgUrl, result) {
                avatar.img.attr('src', imgUrl);
                avatar.navbarImg.attr('src', imgUrl);
                avatar.imgCrop.attr('src', imgUrl);
                avatar.cropModal.modal('hide');
            });
        },
        updateImage: function(img, callback) {
            var canvas = img.cropper('getCroppedCanvas', {fillColor: '#fff'});
            var imgUrl = canvas.toDataURL('image/jpeg');
            photoForm.field.url.val(imgUrl);

            $.ajax({
                url: $globals.route.photoUpload,
                type: "POST",
                data: photoForm.id.serialize(),
                success: function (result) {
                    callback(imgUrl, result);
                }
            });
        }
    };

    var avatar = {
        fileInput: $('#profile-photo-input'),
        img: $(".profile-picture>.thumbnail>img"),
        navbarImg: $(".navbar-profile-picture>img"),
        imgCrop: $("#profile-photo-crop"),
        lastImgSrc: null,
        container: $('.profile-picture'),
        cropModal: $("#profile-photo-update-modal"),
        controls: {
            update: $('#change-profile-picture'),
            save: $("#save-profile-photo"),
            cancel: $("#cancel-profile-photo-changes")
        },
        cropperOptions: {
            viewMode: 3,
            aspectRatio: 1,
            dragMode: 'move',
            guides: false,
            rotatable: false,
            scalable: false
        }
    };

    var profileDetails = {
        body: $('.profile-details-panel .panel-body'),
        edit: $('.profile-details-panel a.edit'),
        editSection: $('#profile-details-modal .modal-body section a.edit'),
        modal: {
            id: $('#profile-details-modal'),
            body: $('#profile-details-modal .modal-body'),
            header: $('#profile-details-modal .modal-header .modal-title'),
            controls: {
                back: $('#profile-details-modal .modal-footer button.back'),
                save: $('#profile-details-modal .modal-footer button.save')
            }
        }
    };

    avatar.lastImgSrc = avatar.imgCrop.attr('src');

    //avatar event listeners
    avatar.controls.save.on('click', function(){
        photoForm.updateAvatar(avatar.imgCrop);
    });

    avatar.controls.cancel.on('click', function(){
        avatar.imgCrop.attr('src', avatar.lastImgSrc);
        avatar.cropModal.modal('hide');
    });

    avatar.fileInput.uploadCompress({
        callback: function(img) {
            avatar.lastImgSrc = avatar.imgCrop.attr('src');
            avatar.imgCrop.attr('src', img.prefix+img.data);
            avatar.imgCrop.cropper('destroy');
            avatar.imgCrop.cropper(avatar.cropperOptions);
        }
    });

    //profile picture modal
    avatar.container.on('mouseover', function () {
        avatar.controls.update.removeClass('hide');
    });

    avatar.container.on('mouseleave', function () {
        avatar.controls.update.addClass('hide');
    });

    avatar.controls.update.on('click', function(){
        avatar.cropModal.modal();
    });

    avatar.cropModal.on("shown.bs.modal", function(){
        avatar.imgCrop.cropper(avatar.cropperOptions);
    });

    avatar.cropModal.on("hidden.bs.modal", function(){
        avatar.imgCrop.cropper('destroy');
    });

    //profile details modal
    profileDetails.edit.on('click', function(ev){
        profileDetails.modal.id.modal();
        // profileDetails.modal.body.load($globals.profileDetailsUrl);
        ev.preventDefault();
    });

    $(document).off('click', profileDetails.editSection.selector).on('click', profileDetails.editSection.selector, function(){
        var title = 'Update ' + $(this).data('title');
        profileDetails.modal.body.load($(this).attr('href'), function(){
            profileDetails.modal.header.text(title);
            profileDetails.modal.controls.save.addClass('hide');
            profileDetails.modal.controls.back.removeClass('hide');

        });
        return false;
    });

    profileDetails.modal.controls.back.on('click', function(){
        profileDetails.modal.body.load($globals.profileDetailsUrl, function(){
            var title = 'Update Profile Details';
            profileDetails.modal.header.text(title);
            profileDetails.modal.controls.save.removeClass('hide');
            profileDetails.modal.controls.back.addClass('hide');
        });
    });

    profileDetails.modal.id.on('hidden.bs.modal', function(){
        profileDetails.modal.body.empty();
    });

    $(document).on('click', '.save-car-trigger', function(event){
        event.preventDefault();

        var $modal = $('#app-modal-wrapper');
        var url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function () {
                $modal.iziModal('startLoading');
            },
            success: function(response) {
                $modal.iziModal('setContent', response.html);
                $modal.iziModal('setTop', 70);
                $modal.iziModal('setBottom', 70);
                $modal.iziModal('setIcon', 'fa fa-car');
                $modal.iziModal('setTitle', ' ');
                $modal.iziModal('open');
            },
            fail: function () {
                $modal.iziModal('setContent', 'Error ocurred!');
            },
            complete: function () {
                $modal.iziModal('stopLoading');
            }
        });
    });

    $(document).on('click', '.delete-car-trigger', function(event){
        event.preventDefault();
        var confimed = confirm('Are you sure you want to remove this car?');

        if (confimed) {
            var url = $(this).attr('href');

            $.ajax({
                url: url,
                type: 'POST',
                success: function(response) {
                    window.location.reload();
                },
                fail: function () {

                }
            });
        }
    });

    $(document).on('submit', 'form[name="save_car"]', function(event){
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        var $modal = $('#app-modal-wrapper');

        formData.append('save_car[uploadedMedia]', form.multiImageUploader('getList'));

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $modal.iziModal('startLoading');
            },
            success: function(response) {
                if (response.saved) {
                    $modal.find('.alert-success').removeClass('hide');
                    $modal.find('.alert-danger').addClass('hide');

                    $('span.help-block').remove();
                    form.find('.form-group').removeClass('has-error');

                } else {
                    $modal.find('.alert-success').addClass('hide');
                    $modal.find('.alert-danger').removeClass('hide');

                    $('span.help-block').remove();
                    
                    for (var name in response.errors) {
                        if (response.errors.hasOwnProperty(name)) {
                            form.find("[name='save_car["+name+"]']").parent('.form-group')
                                .addClass('has-error').append(
                                    $(document.createElement('span')).addClass('help-block')
                                        .text(response.errors[name].join(' '))
                                );
                        }
                    }
                }
            },
            fail: function() {
                $modal.find('.alert-success').addClass('hide');
                $modal.find('.alert-danger').removeClass('hide');
            },
            complete: function() {
                $modal.iziModal('stopLoading');
                $modal.find('.iziModal-wrap').scrollTop(0);
            }
        });
    });

    $(document).on('click', '.delete-car-photo-trigger', function(event){
        event.preventDefault();
        var link = $(this);

        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            success: function(response) {
                link.remove();
            },
            complete: function() {
                console.log('failed deleting photo');
            }
        });
    });

    // profileDetails.modal.controls.save.on('click', function(){
        // $.ajax({
        //     url: $globals.profileDetailsUpdateUrl,
        //     type: "POST",
        //     data: profileDetails.modal.body.find('form').serialize(),
        //     success: function (result) {
        //         profileDetails.body.empty();
        //         profileDetails.body.append(result.html);
        //     },
        //     complete: function (result) {
        //         profileDetails.modal.id.modal('hide');
        //     }
        // });
    // });

    // $(document).on(JobSavedEvent.type + ' ' + JobDeletedEvent.type, function(event){
    //     profileDetails.modal.body.load($globals.jobListUrl);
    // });
    //
    // $(document).on(EducationSavedEvent.type + ' ' + EducationDeletedEvent.type, function(event){
    //     profileDetails.modal.body.load($globals.educationListUrl);
    // });
    //
    // $(document).on(LocalitySavedEvent.type + ' ' + LocalityDeletedEvent.type, function(event){
    //     profileDetails.modal.body.load($globals.localityListUrl);
    // });
});
