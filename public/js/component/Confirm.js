function Confirm (settings) {
    var _this = this;
    _this.props = settings;

    iziToast.question({
        timeout: false,
        close: false,
        overlay: true,
        toastOnce: true,
        icon: _this.props.icon,
        id: _this.props.id,
        zindex: 999,
        title: _this.props.title,
        message: _this.props.message,
        position: 'center',
        buttons: [
            ['<button><b>YES</b></button>', function (instance, toast) {
                $(document).trigger(jQuery.Event(instance.toast.id, {'confirmed': true, 'payload': _this.props.payload}));
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }, true],
            ['<button>NO</button>', function (instance, toast) {
                $(document).trigger(jQuery.Event(instance.toast.id, {'confirmed': false}));
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            // console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            // console.info('Closed | closedBy: ' + closedBy);
        }
    });
}

function AlertSuccess(title, message) {
    iziToast.success({
        title: title,
        message: message
    });
}

function AlertError(title, message) {
    iziToast.error({
        title: title,
        message: message
    });
}

