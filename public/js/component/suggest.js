(function ($) {
    $.fn.suggest = function(options) {
        var defaults = {
            allowCreate: false,
            createKeyTranslation: 'create',
            onSearch: function(keyword, obj) {console.error('implement onSearch callback'); return []},
            onSelect: function(suggestion) {console.error('implement onSelect callback');},
            onCreate: function(keyword) {console.error('implement onCreate callback');},
            onCreateCancel: function(keyword) {console.error('implement onCreateCancel callback');},
            onClear: function(keyword) {console.error('implement onClear callback');},
            hasSelection: function(keyword) {console.error('implement hasSelection callback');}
        };

        var settings = $.extend({}, defaults, options);

        return this.each(function(){
            var _this = this;
            var input = $(this);
            var selectionOccured = false;
            var isCreating = false;
            var suggestionList = new SuggestionList(input, settings.allowCreate, settings.createKeyTranslation);

            input.on('focus', function(){
                input.trigger('keyup');
            });

            input.on('change', function(){
                "use strict";

                if (selectionOccured) {
                    selectionOccured = false;
                    return;
                }

                settings.onClear();

                if (suggestionList.canCreate) {
                    settings.onCreateCancel();
                    isCreating = false;
                }
            });

            input.on('blur', function() {
                console.log('blur');
                suggestionList.hide();
                suggestionList.updateKeyword(input.val());

                if (selectionOccured) {
                    selectionOccured = false;
                }

                if (!settings.hasSelection() && !isCreating) {
                    input.val('');
                    suggestionList.updateKeyword('').clear().hide();
                }
            });

            input.on('keyup', function(){
                settings.onSearch(input.val(), _this);
            });

            input.on('suggestion:select', function(event, suggestion) {
                "use strict";
                console.log('select');
                selectionOccured = true;
                settings.onSelect(suggestion);
                input.val(suggestion.title);
                input.trigger('blur');
            });

            input.on('suggestion:create', function(event, keyword) {
                "use strict";
                selectionOccured = isCreating = true;
                settings.onClear();
                settings.onCreate(keyword);
                suggestionList.hide();
                input.trigger('blur');
            });

            this.updateList = function(list) {
                suggestionList.updateList(list);
                suggestionList.updateKeyword(input.val());
                suggestionList.draw();

                if (!suggestionList.isEmpty()) {
                    suggestionList.show();
                }
                return _this;
            };

            return _this;
        });
    };
}(jQuery));

/**
 *
 * @param selector
 * @param canCreate
 * @returns {SuggestionList}
 * @constructor
 */
var SuggestionList = function(selector, canCreate, createKeyTranslation) {
    "use strict";
    this.input = selector;
    this.list = $(document.createElement('ul')).addClass('suggestion_list');
    this.container = $(document.createElement('div')).addClass('suggestion-list-container');
    this.container.append(this.list);
    this.input.after(this.container);
    this.listSelector = this.input.next('.suggestion-list-container').find('.suggestion_list');
    this.hide();

    this.suggestions = [];
    this.canCreate = canCreate;
    this.keyword = null;
    this.createKeyTranslation = createKeyTranslation;

    return this;
};

SuggestionList.prototype.updateList = function(suggestions) {
    "use strict";
    this.suggestions = suggestions;

    return this;
};

SuggestionList.prototype.updateKeyword = function (keyword) {
    this.keyword = keyword;

    return this;
};

SuggestionList.prototype.clear = function() {
    "use strict";
    this.suggestions = [];
    this.listSelector.empty();

    return this;
};

SuggestionList.prototype.draw = function () {
    "use strict";
    var _this = this;
    _this.listSelector.empty();

    for (var i = 0, len = this.suggestions.length; i < len; i++) {
        _this.listSelector.append(new Suggestion(
            this.suggestions[i].uid,
            this.suggestions[i].title,
            this.suggestions[i].details,
            function(suggestion){
                _this.input.trigger('suggestion:select', [suggestion])
            }
        ).draw());
    }

    if (this.canCreate) {
        var keywordSuggestion = new KeywordSuggestion(
            this.keyword,
            this.createKeyTranslation,
            function(keyword){
                _this.input.trigger('suggestion:create', [keyword])
            }).draw();

        _this.listSelector.append(keywordSuggestion);
    }

    return _this;
};

SuggestionList.prototype.isEmpty = function() {
    "use strict";
    return (this.suggestions.length === 0 && !this.canCreate || this.suggestions.length === 0 && this.canCreate && this.keyword.length === 0);
};

SuggestionList.prototype.hide = function() {
    "use strict";
    this.listSelector.addClass('hide');

    return this;
};

SuggestionList.prototype.show = function() {
    "use strict";
    this.listSelector.removeClass('hide');

    return this;
};

/**
 * Suggest item
 * @param uid
 * @param title
 * @param details
 * @param callback
 * @returns {Suggestion}
 * @constructor
 */
var Suggestion = function(uid, title, details, callback) {
    var _this = this;
    this.uid = uid;
    this.title = title;
    this.details = details;

    this.html = {
        listItem: $(document.createElement('li')).addClass('entry'),
        hyperlink: $(document.createElement('a')).attr({'href': '#'}),
        item: $(document.createElement('ul')),
        title: $(document.createElement('li')).addClass('title'),
        details: $(document.createElement('li')).addClass('details'),
        separator: $(document.createElement('hr'))
    };

    this.html.hyperlink.on('mousedown', function(ev){
        event.preventDefault();
    });

    this.html.hyperlink.on('click', function(ev){
        callback(_this);
        return false;
    });


    return this;
};

Suggestion.prototype.draw = function(){
    "use strict";
    this.html.hyperlink.attr({'data-uid': this.uid});
    this.html.title.text(this.title);
    this.html.details.text(this.details);

    this.html.title.appendTo(this.html.item);
    this.html.details.appendTo(this.html.item);
    this.html.item.appendTo(this.html.hyperlink);
    this.html.hyperlink.appendTo(this.html.listItem);
    this.html.separator.appendTo(this.html.listItem)

    return this.html.listItem;
};

/**
 *
 * @param keyword
 * @param callback
 * @param createKeyTranslation
 * @returns {KeywordSuggestion}
 * @constructor
 */
var KeywordSuggestion = function(keyword, createKeyTranslation, callback) {
    "use strict";
    this.keyword = keyword.trim();
    this.createKeyTranslation = createKeyTranslation;

    this.html = {
        listItem: $(document.createElement('li')).addClass('entry'),
        hyperlink: $(document.createElement('a')).attr({'href': '#'}),
        item: $(document.createElement('ul')),
        title: $(document.createElement('li')).addClass('title')
    };

    this.html.hyperlink.on('mousedown', function(ev){
        event.preventDefault();
    });

    this.html.hyperlink.on('click', function(ev){
        callback(keyword);
        return false;
    });

    return this;
};

KeywordSuggestion.prototype.draw = function(){
    "use strict";

    if (this.keyword === "") {
        return "";
    }

    this.html.title.text(this.createKeyTranslation + ' "'+ this.keyword +'"');
    this.html.title.appendTo(this.html.item);
    this.html.item.appendTo(this.html.hyperlink);
    this.html.hyperlink.appendTo(this.html.listItem);

    return this.html.listItem;
};