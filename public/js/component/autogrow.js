//this helps textarea grow and shrink as you type/delete/paste
(function ($) {
    $.fn.autogrow = function() {
        return this.each(function(){
            var _this = this;
            var $this = $(this);
            $this.css('resize', 'none');

            $this.off('paste input', scale);
            $this.on('paste input', scale);

            function scale() {

                if ($this.outerHeight() > _this.scrollHeight){
                    $this.height(10);
                }

                var border = parseFloat($this.css("borderTopWidth")) + parseFloat($this.css("borderBottomWidth"));
                var sizeDifference = _this.scrollHeight + border - $this.outerHeight();

                if (sizeDifference > 0) {
                    $this.height($this.height() + sizeDifference);
                }
            }

            return this;
        });
    };
}(jQuery));