//@todo : to be removed

(function ($) {
    var uploadedPhotos = [];
    $.fn.multiImageUploader = function(method) {

        var settings = {
            inputSelector: 'input[type=file]',
            fileListSelector: 'ul.file-list',
            url: Routing.generate('images/upload')
        };

        if (method === 'getList') {
            return uploadedPhotos;
        }

        var $this = this;
        var input = this.find(settings.inputSelector);
        var fileListing = this.find(settings.fileListSelector);

        var imageLoader = $(document.createElement('li'))
            .append($(document.createElement('div')).addClass('image-loader').append(
                            $(document.createElement('i')).addClass('fa fa-gear fa-spin fa-4x')
                        )
                    );

        input.on('change', function(event){
            var files = event.target.files;

            for (var i =0; i < files.length; i++) {
                var currentFile = files[i];

                var formData = new FormData();
                formData.append('image', currentFile);

                if (typeof input.attr('data-slot') !== 'undefined' && input.attr('data-slot').length > 1) {
                    formData.append('slot', input.attr('data-slot'));
                }

                $.ajax({
                    url: settings.url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        fileListing.append(
                            $(document.createElement('li')).addClass('placeholder').append(
                                $(document.createElement('i')).addClass('fa fa-gear fa-spin fa-4x')
                            )
                        );
                    },
                    success: function(response) {
                        var src = response.url;
                        var slot = response.slot;
                        var image = response.image;

                        uploadedPhotos.push(response.image);
                        input.attr('data-slot', slot);

                        var placeholder = fileListing.find('li.placeholder').first();
                        placeholder.removeClass('placeholder').empty().append(
                            $(document.createElement('a'))
                                .attr('href', '#')
                                .addClass('remove-photo')
                                .attr('data-id', image)
                                .append(
                                    $(document.createElement('img'))
                                        .attr('style', "background-image: url("+src+")")
                                )
                        );
                    },
                    error: function(jqXHR, status, error) {
                        if (jqXHR.status === 400) {
                            console.log('upload failed');
                        }
                    },
                    complete: function() {
                        imageLoader.remove();
                    }
                });
            }
        });

        $this.on('click', '.remove-photo', function(event) {
            event.preventDefault();
            var link = $(this);
            var image = $(this).attr('data-id');
            var slot = input.attr('data-slot');

            $.ajax({
                url: Routing.generate('images/remove', {
                    slot: slot,
                    image: image
                }),
                type: 'DELETE',
                success: function(response) {
                    link.parent().remove();

                    var photoIndex = uploadedPhotos.indexOf(image);
                    if (photoIndex !== -1) {
                        uploadedPhotos.splice(photoIndex, 1);
                    }
                },
                error: function(jqXHR, status, error) {
                    if (jqXHR.status === 400) {
                        console.log('delete failed');
                    }
                },
                complete: function() {

                }
            });
        });

        $(document).on('closing', '#app-modal-wrapper', discardChanges);

        function discardChanges(event) {
            var slot = input.attr('data-slot');

            // if (slot) {
            //     $.ajax({
            //         url: Routing.generate('images/clear'),
            //         type: 'DELETE',
            //         complete: function() {
            //             uploadedPhotos = [];
            //         }
            //     });
            // }
            $(document).off('closing', '#app-modal-wrapper', discardChanges);
        }

        return this;
    };
}(jQuery));