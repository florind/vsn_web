(function ($) {
    $.fn.uploadCompress = function(options) {
        var defaults = {
            size: 2, // the max size in MB, defaults to 2MB
            quality: 0.72, // the quality of the image, max is 1,
            maxWidth: 960, // the max width of the output image, defaults to 1920px
            maxHeight: 640, // the max height of the output image, defaults to 1920px
            resize: true,
            callback: null
        };

        var settings = $.extend({}, defaults, options);

        if (settings.callback === null || !$.isFunction(settings.callback)) {
            console.error("Callback missing or invalid for image compress!");
            return this;
        }

        var $this = this;
        const compress = new Compress();

        $this.on('change', function(event){
            var files = [...event.target.files];
            compress.compress(files, settings).then(
                function(images) {
                    var img = images[0];
                    settings.callback(img);
                }
            );
        });

        return this;
    };
}(jQuery));