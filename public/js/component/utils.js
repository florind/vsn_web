var Utils = {
    event: {
        TOPIC_UPDATED: 'topic:updated',
        COMMENT_UPDATED: 'comment:updated',
        REPLY_UPDATED: 'reply:updated',
        dispatch: function (event, payload) {
            setTimeout(function() {
                document.dispatchEvent(new CustomEvent(event, {detail: payload}));
            }, 0);
        }
    },

    infobox: function (type, title, message) {
        ReactDOM.render(
            React.createElement(InfoBox, {type: type, title: Translator.trans(title), message: Translator.trans(message)}, null),
            document.getElementById('app-infobox')
        );
    },

    confirmbox: function (title, message) {
        let id = Utils.uniqueId();

        ReactDOM.render(
            React.createElement(ConfirmBox, {id: id, title: Translator.trans(title), message: Translator.trans(message)}, null),
            document.getElementById('app-confirmbox')
        );

        return id;
    },

    menubox: function (items) {
        //validate structure
        if(!items instanceof Array) {
            throw new Error('items must be array');
        }

        if(!items.length) {
            throw new Error('Menu cannot be empty');
        }

        if (!items[0] instanceof MenuBoxEntry) {
            throw new Error('invalid menu items.');
        }

        ReactDOM.render(
            React.createElement(MenuBox, {items: items}, null),
            document.getElementById('app-menubox')
        );
    },

    modal: function (content) {
        ReactDOM.render(content, document.getElementById('app-modal'));
    },

    mediaGallery: function (media, current) {
        ReactDOM.render(
            React.createElement(MediaGallery, {media: media, current: current}, null),
            document.getElementById('app-media-gallery')
        );
    },

    uniqueId: function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return '_' + s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    },

    imgUrl: function(filename) {
        return "https://static.web.vsn/" + filename;
    },

    excerpt: function (text) {
        let tolerance = 10;
        let length = 160;

        if (text.length <= length + tolerance) {
            return text;
        }

        let excerpt = text.substr(0, length);
        return excerpt.substr(0, Math.min(length, excerpt.lastIndexOf(" "))) + ' ...';
    },

    splitIntoParagraphs: function (text) {
        return text.split(/\r\n|\r|\n/);
    },
};

var MenuBoxEntry = function(name, icon, event) {
    this.name = name;
    this.icon = icon;
    this.event = event;
};
