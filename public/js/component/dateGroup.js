(function ($) {
    $.fn.dateGroup = function(options) {
        var defaults = {
            year: 'select[data-type="year"]',
            month: 'select[data-type="month"]',
            day: 'select[data-type="day"]',
            update: '[data-update="true"]'
        };

        var settings = $.extend({}, defaults, options);

        return this.each(function(){
            var $this = $(this);
            var yearDropdown = $this.find(settings.year);
            var monthDropdown = $this.find(settings.month);
            var dayDropdown = $this.find(settings.day);
            var updateTrigger = $this.find(settings.update);
            
            var selectedYear = yearDropdown.val().length > 0 ? yearDropdown.val() : new Date().getFullYear();
            var selectedMonth = monthDropdown.val().length > 0 ? monthDropdown.val() : ("0"+(new Date().getMonth()+1)).slice(-2);
            var selectedDay = dayDropdown.val().length > 0 ? dayDropdown.val() : ("0"+(new Date().getDate())).slice(-2);

            updateTrigger.on('change', function(ev){
                var maxDay = new Date(yearDropdown.val(), monthDropdown.val(), 0).getDate();
                dayDropdown.empty();
                for (var day = maxDay; day > 0; day--) {
                    var value = day < 10 ? '0' + day : day;
                    var option = $(document.createElement('option')).val(value).text(day);
                    dayDropdown.append(option);
                    if (day === maxDay) {
                        dayDropdown.val(value);
                    }
                }
            });

            yearDropdown.val(selectedYear);
            monthDropdown.val(selectedMonth);
            monthDropdown.trigger('change');
            // var selectedDay = dayDropdown.val().length > 0 ? dayDropdown.val() : ("0"+(new Date().getDate())).slice(-2);

            dayDropdown.val(selectedDay);

            return this;
        });
    };
}(jQuery));