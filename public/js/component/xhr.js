var XHR = {
    post: function (url, data, done) {
        this.request('POST', url, data, done);
    },

    get: function (url, done) {
        this.request('GET', url, null, done);
    },

    delete: function (url, done) {
        this.request('DELETE', url, null, done);
    },

    request: function(type, url, data, done) {
        let xhr = new XMLHttpRequest();
        xhr.open(type, url);

        xhr.onreadystatechange = function (event) {
            if (xhr.readyState === 4) {
                if (done instanceof Function) {
                    done(xhr);
                }
            }

        };

        xhr.send(data);
    }
};