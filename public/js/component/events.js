var LikeToggleEvent = {
    type: 'event:like:toggle',
    launch: function(hash, likeCount, initiator) {
        $(document).trigger(
            jQuery.Event(LikeToggleEvent.type, {hash: hash, likeCount: likeCount, initiator:initiator})
        );
    }
};

var AddCommentEvent = {
    type: 'event:comment:add',
    launch: function(hash, initiator) {
        $(document).trigger(
            jQuery.Event(AddCommentEvent.type, {hash: hash, initiator:initiator})
        );
    }
};
