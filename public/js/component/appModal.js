function AppModal(selector) {
    this.selector = $(selector);

    this.defaultSettings = {
        history: true,
        bodyOverflow: true,
        autoOpen: false
    };

    this.gallerySettings = {
        history: true,
        bodyOverflow: true,
        autoOpen: false,
        openFullscreen: true,
        background: 'rgba(0,0,0, 0.9)',
        icon: 'fa fa-photo',
        title: ' ',
        headerColor: 'rgba(0,0,0, 0)',
        closeButton: true
    };

    $(this.selector).iziModal(this.defaultSettings);
    $(this.selector).find('.iziModal-content').css('min-height', '100px');
}

AppModal.prototype.close = function () {
    this.selector.iziModal('close');
};

AppModal.prototype.openGallery = function (content) {
    this.selector.iziModal('destroy');
    this.selector.iziModal(this.gallerySettings);
    this.selector.iziModal('open');
    this.selector.iziModal('startLoading');
    this.selector.iziModal('setContent', content);
    this.selector.iziModal('stopLoading');
};

AppModal.prototype.startLoading = function (title, icon) {
    this.selector.iziModal('destroy');
    this.selector.iziModal(this.defaultSettings);

    if (typeof title !== 'undefined') {
        this.selector.iziModal('setTitle', title);
    }

    if (typeof icon !== 'undefined') {
        this.selector.iziModal('setIcon', icon);
    }

    this.selector.iziModal('open');
    this.selector.iziModal('startLoading');
};

AppModal.prototype.finishLoading = function (content, title, icon) {
    if (typeof title !== 'undefined') {
        this.selector.iziModal('setTitle', title);
    }

    if (typeof content !== 'undefined') {
        this.selector.iziModal('setContent', content);
    }

    if (typeof icon !== 'undefined') {
        this.selector.iziModal('setIcon', icon);
    }

    this.selector.iziModal('stopLoading');
};