(function ($) {
    $.fn.sectionPanel = function(options) {
        var defaults = {
            url: "",
            method: "",
            finished: function(success){
                console.log("finished not implemented");
            }
        };

        //example - delete
        // var fakeData = [
        //     ["#", "images/bmw_logo.png"],
        //     ["#", "images/audi_logo.png"],
        //     ["#", "images/audi_logo.png"],
        //     ["#", "images/audi_logo.png"],
        // ];

        var settings = $.extend({}, defaults, options);
        var $this = this;

        $this.on('click', '.panel-footer a.more', function(){
            $.ajax({
                url: settings.url,
                beforeSend: function(xhr) {
                    $this.find(".panel-footer a.more").addClass('hide');
                    $this.find(".panel-footer").append(
                        $(document.createElement("div")).html("loading...").addClass("loader")
                    );
                }
            }).done(function(data){
                // populateGallery(fakeData);
                if (data.constructor === Array) {
                    populateGallery(data);
                }
                settings.finished(true);
            }).fail(function(){
                //failed
                settings.finished(false);
            }).always(function(){
                $this.find(".panel-footer .loader").remove();
                $this.find(".panel-footer a.more").removeClass('hide');
            });
            return false;
        });

        function populateGallery(entries) {
            var gallery = $this.find(".panel-body .profile-gallery.scss");
            var i,j,tempArray,chunk = 10;

            for (i = 0,j = entries.length; i<j; i+=chunk) {
                tempArray = entries.slice(i,i+chunk);
                var row = $(document.createElement("div")).addClass("row");

                for (var x = 0; x < tempArray.length; x++) {
                    var entry = $(document.createElement("div")).addClass("col-md-6");
                    entry.append(
                        $(document.createElement("div")).addClass("thumbnail").append(
                            $(document.createElement("a")).attr("href", entries[x][0])
                        ).append($(document.createElement("img")).attr("src", entries[x][1]))
                    );
                    row.append(entry);
                }
                gallery.append(row);
            }
        }

        return this;
    };
}(jQuery));